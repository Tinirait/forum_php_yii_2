<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
                'class'=>'CaptchaExtendedAction',
                // if needed, modify settings
                'testLimit' => 1,
                //'mode'=>CaptchaExtendedAction::MODE_MATH,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$error=Yii::app()->errorHandler->error;
	
		if(Yii::app()->request->isAjaxRequest)
			echo $error['message'];
		else
			$this->render('error', $error);
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
                        if(isset($_POST['ajax'])) {
                           echo CActiveForm::validate($model);
                           Yii::app()->end();
                        }
			if($model->validate())
			{
				Yii::import('ext.phpmailer.JPhpMailer');
				$mail = new JPhpMailer;
				$mail->IsSMTP();
				$mail->Host = Yii::app()->params['emailHost'];
				$mail->Port = Yii::app()->params['emailPort']; 
				$mail->SMTPAuth = true;
				$mail->Username = Yii::app()->params['emailUsername'];
				$mail->Password = Yii::app()->params['emailPassword'];
				$mail->SetFrom($model->email, $model->name);
				$mail->Subject = $model->subject;
				$mail->AltBody = Yii::app()->params['emailAltMessage'];
				$mail->MsgHTML($model->body);
				$mail->AddAddress(Yii::app()->params['adminEmail'], Yii::app()->name);
				$mail->Send();	
	
				Yii::app()->user->setFlash('contact',Message::getTranslation(192));
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}
}