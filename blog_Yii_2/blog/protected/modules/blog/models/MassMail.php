<?php

class MassMail extends CActiveRecord
{
	public $categories;
	public $roles;
	public $users;
	public $oldstatus;
	
	const STATUS_DRAFT=1;
	const STATUS_SENT=2;
	const TYPE_MESSAGE=1;
	const TYPE_NEWS=2;

	/**
     * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     *return Строка связана имя таблицы базы данных
	 */
	public function tableName()
	{
		return '{{mass_mail}}';
	}
	
	/**
     * *return Правила проверки для атрибутов массив модели.
	 */
	public function rules()
	{
		return array(
			array('title, content, status, type', 'required'),
			array('status', 'in', 'range'=>array(1,2)),
			array('title', 'length', 'max'=>128),
			array('id, title, status, user_id, type', 'safe', 'on'=>'search'),
			array('categories, users, roles', 'default'),
			array('title, content, users','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
		);
	}


	/**
     *return Массив индивидуальные этикетки атрибутов (имя => label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Message::getTranslation(27),
			'title' => Message::getTranslation(28),
			'content' => Message::getTranslation(29),
			'status' => Message::getTranslation(30),
			'create_time' => Message::getTranslation(31),
			'update_time' => Message::getTranslation(32),
			'user_id' => Message::getTranslation(33),
			'type' => Message::getTranslation(157),
			'categories' => Message::getTranslation(152),
		);
	}

	/**
     *return Массив реляционные правила
	 */
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	
	/**
    Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('type',$this->type);
		$criteria->compare('user_id',$this->user_id);
		
		return new CActiveDataProvider('MassMail', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'status, create_time DESC',
			),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(31),
			),
		));
	}
	
	public function afterFind()
	{
		$this->oldstatus = $this->status;

        // Получение пользователи....................
		$this->users = "";
		$mmus = MassMailUser::model()->findAll(array('condition' => 'mass_mail_id = ' . $this->id));
		foreach($mmus as $mmu) {
			$this->users .= User::model()->findByPk($mmu->user_id)->username . ', ';
		}

		// Получение категории.................
		$this->categories = array();
		$mmcs = MassMailCategory::model()->findAll(array('condition' => 'mass_mail_id = ' . $this->id));
		foreach($mmcs as $mmc) {
			$this->categories[$mmc->category->language . '|' . $mmc->category_id] = $mmc->category->language . ': ' . $mmc->category->name;
		}
	
		// Получение роли...................
		$this->roles = array();
		$mmrs = MassMailRole::model()->findAll('mass_mail_id = ?', array($this->id));
		foreach ($mmrs as $mmr){
			$this->roles[$mmr->role_id] = $mmr->role_id;
		}
	}
	
	public function setCategories()
	{
		$listData = array();
		foreach($this->categories as $category)
		{
			$category_array = explode('|', $category);
			$category2 = Category::model()->findByPk(array('language' => $category_array[0], 'id' => $category_array[1]));
			$listData[$category2->language . '|' . $category2->id] = $category2->language . ': ' . $category2->name;
		}
		$this->categories = $listData;		
	}
	
	protected function beforeSave()
	{
			if($this->isNewRecord)
			{
				$this->create_time=$this->update_time=time();
				$this->user_id=Yii::app()->user->id;
			}
			else
				$this->update_time=time();
		
		return true;
	}
	
	protected function afterSave()
	{
		// Сохранение пользователи.....................
		if(!is_null($this->users))
		{
			MassMailUser::model()->deleteAll('mass_mail_id =' . $this->id);
			$usersarray = self::string2array($this->users);
			foreach($usersarray as $user)
			{
				$mmu = new MassMailUser;
				$mmu->mass_mail_id = $this->id;
				$user2 = User::model()->find('username = ?', array($user));
				if(!is_null($user))
				{
					$mmu->user_id = $user2->id;
					$mmu->save();
				}
			}
		}
		
		//Сохранение категории....................
		if(!is_null($this->categories))
		{
			MassMailCategory::model()->deleteAll('mass_mail_id =' . $this->id);
			foreach($this->categories as $category)
			{
				$category_array = explode('|', $category);

				$mmc = new MassMailCategory;
				$mmc->mass_mail_id = $this->id;			
				$mmc->language = $category_array[0];
				$mmc->category_id = $category_array[1];
				$mmc->save();
			}
		}
		
		// Saving roles..........................
		if(!is_null($this->roles))
		{
			MassMailRole::model()->deleteAll('mass_mail_id =' . $this->id);
			foreach($this->roles as $role)
			{
				$mmr = new MassMailRole;
				$mmr->mass_mail_id = $this->id;
				$mmr->role_id = $role;
				$mmr->save();
			}
		}
		
		// Отправка по электронной почте........................................................
		$emailList = "";
		$usersList = "";
		// Getting users ids
		if($this->status == MassMail::STATUS_SENT && $this->oldstatus == MassMail::STATUS_DRAFT)
		{
			if(!is_null($this->users))
			{
				$usersarray = self::string2array($this->users);
				foreach($usersarray as $user)
				{
					$user2 = User::model()->find('username = ?', array($user));
					if(!is_null($user2))
					{
						$usersList .= ',' . $user2->id;
					}
				}
			}
			
			// Получение пользователей идентификаторы из категорий
			if(!is_null($this->categories) && isset($category_array[1]))
			{
				foreach($this->categories as $category)
				{
					$category_array = explode('|', $category);		

					$category2 = Category::model()->findByPk(array('language' => $category_array[0], 'id' => $category_array[1]));

					if($category2->areParentCategoriesPublished())
					{
						$subscriptions = Subscription::model()->findAll(
							array('condition' => 'category_id IN (' . $category2->getSubcategoriesIds() . ') AND language = ?',
									'distinct' => true,
									'select' => 'user_id',
									'params' => array($category_array[0])));				
						foreach($subscriptions as $subscription)
						{
							$user = User::model()->find(array('condition' => 'id=:id', 'params' => array(':id' => $subscription->user_id)));
							if(!is_null($user))
							{
								$usersList .= ',' . $user->id;
							}
						}
					}								
				}				
			}
			
			// Получение пользователей идентификаторы из ролей
			if(!is_null($this->roles))
			{
				foreach($this->roles as $role)
				{
					$aas = AuthAssignment::model()->findAll(array('condition' => 'itemname=:itemname', 'params' => array(':itemname' => $role)));
					foreach($aas as $aa)
					{
						$usersList .= ',' . $aa->userid;
					}
				}
			}
			
			// Отправка массовой рассылки
			if($usersList !== '')
			{
				if($this->type == MassMail::TYPE_NEWS)
				{
					$users = User::model()->findAll(array('condition' => 'id IN(' . substr($usersList,1) . ') AND status=:status AND subscribe=1', 'params' => array(':status' => User::STATUS_ACTIVE)));
					foreach($users as $user)
					{
						if($user->subscribe)
							$user->sendNews($this->title, $this->content);
					}
				}
				else
				{
					$users = User::model()->findAll(array('condition' => 'id IN(' . substr($usersList,1) . ') AND status=:status', 'params' => array(':status' => User::STATUS_ACTIVE)));
					$sender = User::model()->findByPk(Yii::app()->user->id);
					foreach($users as $user)
					{
						$user->sendEmail(Parameter::getValue(28) . ': ' . $sender->username, $sender->email, $this->title, $this->content);
					}					
				}
			}
		}
	}

	public static function string2array($tags)
	{
		return preg_split('/\s*,\s*/',trim($tags),-1,PREG_SPLIT_NO_EMPTY);
	}
}

