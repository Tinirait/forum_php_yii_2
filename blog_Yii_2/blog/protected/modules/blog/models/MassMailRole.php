<?php

class MassMailRole extends CActiveRecord
{
	/**
     * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     *return Строка связана имя таблицы базы данных
	 */
	public function tableName()
	{
		return '{{mass_mail_role}}';
	}
}
	