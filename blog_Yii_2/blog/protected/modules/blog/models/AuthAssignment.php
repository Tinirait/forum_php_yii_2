<?php

class AuthAssignment extends CActiveRecord
{
	/**
	 *Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
     * /
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
    return Строка связанную с именем таблицы базы данных
	 */
	public function tableName()
	{
		return '{{auth_assignment}}';
	}
}
	