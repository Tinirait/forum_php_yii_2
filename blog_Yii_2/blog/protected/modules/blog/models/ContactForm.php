<?php

/**
/ **
  * Класс ContactForm.
  * ContactForm это структура данных для хранения
  * Данные контактную форму. Он используется в "контакт" действия "SiteController".
 */
class ContactForm extends CFormModel
{
	public $subject;
	public $body;

	/**
    / **
     * Объявляет правила валидации.
     * /
	 */
	public function rules()
	{
		return array(
			array('subject, body', 'required'),
			array('subject, body','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
		);
	}

	/**
    / **
     * Объявляет настроенные ярлыки атрибутов.
     * Если не заявил здесь, атрибут будет иметь метку, которая является
     * Так же, как его имя с первой буквы в верхнем регистре.
     * /
	 */
	public function attributeLabels()
	{
		return array(
			'subject'=>Message::getTranslation(28),
			'body'=>Message::getTranslation(29),
		);
	}
}