<?php
// название модели, это название таблицы базы данных, но в базе данных пишется ещё tbl-префикс
class Post extends CActiveRecord
{
	const STATUS_DRAFT=1;
	const STATUS_PUBLISHED=2;
	const STATUS_ARCHIVED=3;

	private $_oldTags;
	public $oldId;
	public $oldCategory;
	public $oldLanguage;
	public $postyear; // Используется в виджете архивов
	public $postmonth; // Используется в виджете архивов
	public $postspermonth; // Используется в виджете архивов
	public $maxId;
	
	/**
	 * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 *return строка связана имя таблицы базы данных
	 */
	public function tableName()
	{
		return '{{post}}';
        // это значит, что берётся название модели Post и из config
        // возвращаем название таблицы, от которой берётся эта модель.
	}

	/**
     * Правила проверки return массив для атрибутов модели.
	 */
        /**
         * Тут прописаны правила проверки для каждого поля модели
         * все поля модели можно увидеть в верху в комментарии
         * @return type
         */
	public function rules()
	{
		$p = new CHtmlPurifier();
		//$p->options = array('HTML.AllowedCommentsRegexp'=> '/.*/');
		$p->options = array('HTML.AllowedComments'=> array('pagebreak'));	
		return array(
			array('title, content, status, category_id', 'required'),
            // обязательные поля *
			array('id', 'numerical', 'min' => 1, 'max' => 9999999999),
            // id должен быть числом..
			array('status', 'in', 'range'=>array(1,2,3)),
            // status-число в интервале ...
			array('title', 'length', 'max'=>128),
			array('tags', 'match', 'pattern'=>'/^[\p{L}\s,0-9]+$/u', 'message'=>Message::getTranslation(101)),
			array('tags', 'normalizeTags'),// когда мы записываем тэги, они должны удовлетворять легулярному выражению.
            //регулярка говорит:Могут быть пробелы, символы, цифры от 0-9,)
			array('meta_keywords, meta_description', 'default', 'setOnEmpty' => true, 'value' => null),
                        array('language','default', 'setOnEmpty' => true, 'value' => 'ru'),
			array('id, title, status, category_id, language, user_id', 'safe', 'on'=>'search'),
            //safe-когда мы даём поиск то поля id, title, status, category_id, language, user_id' могут быть любыми/можем
            //что то не ввести и сделать поиск.
			array('title, tags, meta_keywords, meta_description','filter','filter'=>array(new CHtmlPurifier(),'purify')),
			array('content','filter','filter'=>array($p,'purify')),
            //CHtmlPurifier()-защита от введения какого либо кода
            // мы проверяем title, tags, meta_keywords, meta_description','filter'
	        //array('id', 'checkPost', 'on' => 'insert, update'),
		);
	}
// Правилa по которым работают формы. Любое поле при заполнении таблицы должно быть определённых правил.(только числа, толко символы...)

	/**
	 * @return Массив  правил.
	 */
        /**
         * Тут прописаны связи модели с другими моделями,
         * по сути это типа связи между таблицами
         * BELONGS_TO USER - это значит что эта модель то есть пост относится к одному пользователю
         * HAS_MANY COMMENT - это значит что эта модель может иметь много комментариев
         * @return type
         */
	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'comments' => array(self::HAS_MANY, 'Comment', array('post_id', 'category_id', 'language'), 'condition'=>'comments.status='.Comment::STATUS_APPROVED . ' AND comments.comment_id IS NULL', 'order'=>'comments.create_time DESC'),
			'category' => array(self::BELONGS_TO, 'Category', array('category_id', 'language')),
		);
	}

	/**
     *return Массив индивидуальные атрибутов (имя => label)
	 */
        /**
         * ФОрма добавления поста или его редактирования имеет поля
         * ТУт указаны названия полей, то есть лейблы к ним
         * Message::getTranslation - говорит о том что все названия берутся их таблицы tpl_messages по ID
         * 
         * @return type
         */
	public function attributeLabels()
	{
		return array(
			'id' => Message::getTranslation(27),
			'title' => Message::getTranslation(28),
			'content' => Message::getTranslation(29),
			'tags' => Message::getTranslation(4),
			'status' => Message::getTranslation(30),
			'create_time' => Message::getTranslation(31),
			'update_time' => Message::getTranslation(32),
			'user_id' => Message::getTranslation(33),
			'category_id' => Message::getTranslation(34),
			'language' => Message::getTranslation(35),
			'meta_keywords' => Message::getTranslation(36),
			'meta_description' => Message::getTranslation(37),
			'commentCount' => '#' . substr(Message::getTranslation(17),0,3),
		);
	}

        /**
         * Получаем количество комментариев на определенный пост
         * @return type
         */
	public function getCommentCount()
	{
		return Comment::model()->count('post_id = ? AND language = ? AND status = ?', array($this->id, $this->language, Comment::STATUS_APPROVED));
	}
	
	/**
	 * return строка URL, который показывает деталь сообщению
	 */
        /**
         * Получаем URL к этому посту
         * URL относительный
         */
	public function getUrl()
	{
		$url = array();
		$url['post'] = $this->id;
		$url['category'] = $this->category_id;
		$url['title'] = $this->sanitizeString($this->title);
		$url['lang'] = $this->language;
		return Yii::app()->createUrl('blog/post/view', $url);
	}

        /**
         * Получаем абсолютный URL к посту
         * @return type
         */
	public function getAbsoluteUrl()
	{
		$url = array();
		$url['post'] = $this->id;
		$url['category'] = $this->category_id;
		$url['title'] = $this->sanitizeString($this->title);
		$url['lang'] = $this->language;
		return Yii::app()->createAbsoluteUrl('blog/post/view', $url);
	}
	
        /**
         * По идее это замена на спец символы, 
         * @param type $string
         * @return type
         */
	public function sanitizeString($string)
	{
	
	    $string = trim($string);
	
	    $string = str_replace(
	        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
	        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
	        $string
	    );
	
	    $string = str_replace(
	        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
	        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
	        $string
	    );
	
	    $string = str_replace(
	        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
	        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
	        $string
	    );
	
	    $string = str_replace(
	        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
	        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
	        $string
	    );
	
	    $string = str_replace(
	        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
	        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
	        $string
	    );
	
	    $string = str_replace(
	        array('ñ', 'Ñ', 'ç', 'Ç'),
	        array('n', 'N', 'c', 'C',),
	        $string
	    );
	
	    $string = str_replace(
	        array("\\", "¨", "º", "-", "~",
	             "#", "@", "|", "!", "\"",
	             "·", "$", "%", "&", "/",
	             "(", ")", "?", "'", "¡",
	             "¿", "[", "^", "`", "]",
	             "+", "}", "{", "¨", "´",
	             ">", "< ", ";", ",", ":",
	             "."),
	        '',
	        $string
	    );
		
		$string = strtolower($string);
		
		$string = str_replace(' ', '-', $string);
	
	    return $string;
	}

	/**
	 *return массив список ссылок, которые указывают на список почты фильтруется каждого тега этого поста
	 */
        /**
         * Получение ссылок на теги
         * @return type
         */
	public function getTagLinks()
	{
		$links=array();
		foreach(Tag::string2array($this->tags) as $tag)
			$links[]=CHtml::link(CHtml::encode($tag), array('post/index', 'tag'=>preg_replace(array('|\s+|','/\W-/'), array('-','-'), $tag), 'lang' => $this->language), array('rel' => Parameter::getValue(32) ? 'nofollow' : null));
		return $links;
	}

	/**
	 * Нормализует введенные пользователем теги.
	 */
        /**
         * Формирования тегов к посту (они получены из связей между таблицами - relations) в строку через запятую
         * @param type $attribute
         * @param type $params
         */
	public function normalizeTags($attribute,$params)
	{
		$this->tags=Tag::array2string(array_unique(Tag::string2array($this->tags)));
	}

    /**
     *Добавляет новый комментарий к этому сообщению.
     * Этот метод будет установлено состояние и post_id комментария соответственно.
     *param Комментарий комментарий будет добавлен
     *return Булево ли комментарий успешно сохранен
     */
        
        /**
         * Добавлени комментариев, используются константы статусов, которые прописаны в модели Comment
         * @param type $comment
         * @return type
         */
	public function addComment($comment)
	{
		if(Yii::app()->user->checkAccess('admin'))
		{
			$comment->status=Comment::STATUS_APPROVED;
		}
		else
		{
			if(Parameter::getValue(6))
				$comment->status=Comment::STATUS_PENDING;
			else
				$comment->status=Comment::STATUS_APPROVED;
		}
		
		$comment->post_id=$this->id;
		$comment->category_id=$this->category_id;
		$comment->language=$this->language;
		$comment->user_id = Yii::app()->user->id;
		
                return $comment->save();
	}

	/**
	 * This is invoked when a record is populated with data from a find() call.
	 */
        /**
         * Вызывается после поиска модели, сохраняет ее старые данные, например применяется при изменении
         */
	protected function afterFind()
	{
		parent::afterFind();
		$this->_oldTags=$this->tags;
		$this->oldId = $this->id;
		$this->oldCategory = $this->category_id;
		$this->oldLanguage = $this->language;
	}

	/**

	 */
        /**
         * Вызывается перед сохранением поста
         * дополняет данные поста временем создания или изменения, 
         * проверяет если переданы мета данные и если их нет то записывает в поля нулл
         * 
         * @return boolean
         */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{			
			if($this->meta_keywords === "")
				$this->meta_keywords = null;
			if($this->meta_description === "")
				$this->meta_description = null;
			
			if($this->isNewRecord)
			{
				$this->create_time=$this->update_time=time();
				$this->user_id=Yii::app()->user->id;
			}
			else
				$this->update_time=time();

			// Отправить сообщение в подписанных пользователей
			if(Parameter::getValue(30) && !$this->sent && $this->status == Post::STATUS_PUBLISHED)
			{
				if($this->category->areParentCategoriesPublished())
				{
					$subscriptions = Subscription::model()->findAll(
						array('condition' => 'category_id IN (' . $this->category->getSubcategoriesIds() . ') AND language = ?',
								'distinct' => true,
								'select' => 'user_id',
								'params' => array($this->language)));				
					foreach($subscriptions as $subscription)
					{
						$user = User::model()->find(array('condition' => 'id=:id AND subscribe=1 AND status=:status', 'params' => array(':id' => $subscription->user_id, ':status' => User::STATUS_ACTIVE)));
						if(!is_null($user))
						{
							$user->sendNews($this->title, $this->content);
						}
					}
				}
				$this->sent = 1;
			}
			// КОНЕЦ Отправить сообщение в подписанных пользователей .................
			return true;
		}
		else
			return false;
	}

	/**
	 * Это вызывается после сохранения записи.
	 */
        /**
         * Вызывается после сохранения модели поста и выполняет подсчет частоты появления тегов
         * также частота сохраняется
         * все выполняется в методе updateFrequency модели Tag
         */
	protected function afterSave()
	{
		parent::afterSave();
		
		Tag::model()->updateFrequency($this->_oldTags, $this->tags, $this->language);
	}

        /**
         * Выполняется перед удалением, проверяет если  у поста нет комментариев то возвращает true
         * и пост удаляется
         * 
         * @return boolean
         */
	protected function beforeDelete()
	{
		if(count($this->getAllComments()) == 0)
		{
			return true;
		}

		return false;
	}

        /**
         * Берется контент поста и обрезается его часть чтоб сделать краткое введение к посту
         * Сколько символов брать записано в таблице tpl_parameter с ID 5
         * @return type
         */
	public function getIntroductoryText()
	{	
		$posbreak = strpos($this->content, '<!-- pagebreak -->');
		if($posbreak > 0)
			return substr($this->content,0,$posbreak) . ' [...]';
		else
		{		
			$retval = $this->content; //в случае проблемы
			$array = explode(" ", $this->content);
			/* Уже достаточно коротким,чтобы  вернуть всю вещь*/
			if (count($array) <= Parameter::getValue(5))
			{
				$retval = $this->content;
			}

			else
			{
				array_splice($array, Parameter::getValue(5));
				$retval = implode(" ", $array) . " [...]";
			}
			
			return strip_tags($retval);
		}
	}
	
        /**
         * Получаем все комментарии
         * @return type
         */
	public function getAllComments()
	{
		return Comment::model()->findAll('post_id =? AND category_id = ? AND language = ?', array($this->id, $this->category_id, $this->language));
	}
	
	/**
    Это вызывается после удаления записи.
	 */
        /**
         * Изменение частоты тегов
         * Вызывается после удаления поста
         */
	protected function afterDelete()
	{
		parent::afterDelete();
		Tag::model()->updateFrequency($this->tags, '', $this->language);
	}

	/**
	 * Получает список сообщений, основываясь на текущих условиях поиск / фильтр.
     *return CActiveDataProvider поставщика данных, который может вернуть необходимые сообщения.
	 */
        /**
         * Производит поиск  поста, переданная строка при помощи конструкции compare 
         * сравнивается с указанными полями таблицы post
         * @return \CActiveDataProvider
         */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->alias='p';
		$criteria->with = array('category'=>array('alias'=>'c'), 'user'=>array('alias'=>'u'));
		$criteria->compare('p.id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('c.name', $this->category_id,true);
		$criteria->compare('p.language',$this->language);
		$criteria->compare('p.status',$this->status);
		$criteria->compare('u.username',$this->user_id, true);
		if(Yii::app()->user->checkAccess('author'))
			$criteria->compare('p.user_id', Yii::app()->user->id);
		

		return new CActiveDataProvider('Post', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'p.id DESC, p.language ASC',
			),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(8),
			),
		));
	}
	
        /**
         * получение статуса поста
         * @return type
         */
	public function getStatusOptions()
	{
		if(Yii::app()->user->checkAccess('author') || Yii::app()->user->checkAccess('editor'))
			return CHtml::listData(Lookup::model()->findAll(array('condition' => 'code <> ' . Post::STATUS_PUBLISHED . ' AND type = "PostStatus" ','order' => 'code ASC')), 'code', 'name');
		else
			return CHtml::listData(Lookup::model()->findAll(array('condition' => 'type = "PostStatus"', 'order' => 'code ASC')), 'code', 'name');
	}
}