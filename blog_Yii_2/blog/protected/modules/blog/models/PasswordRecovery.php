<?php

/**
 * Класс ContactForm.
  * ContactForm это структура данных для хранения
  * Данные контактную форму. Он используется в "контакт" действия "SiteController".
 */
class PasswordRecovery extends CFormModel
{
	public $email;
	public $verifyCode;

	/**
     * Объявляет правила валидации.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('email', 'required'),
			array('email', 'email'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
     * Объявляет настроенные ярлыки атрибутов.
     * Если не заявил здесь, атрибут будет иметь метку, которая является
     * Так же, как его имя с первой буквы в верхнем регистре.
	 */
	public function attributeLabels()
	{
		return array(
			'email' => Message::getTranslation(11),
			'verifyCode' => Message::getTranslation(182),
		);
	}
}