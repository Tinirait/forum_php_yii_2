<?php

Yii::import('ext.EImageValidator');

class Language extends CActiveRecord
{
	public $image;
	public $oldcode;
	const IMAGE_DIRECTORY = 'images/flags/';
	
	/**
    / **
     * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
     * /
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{language}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('code, name', 'required'),
			array('code', 'unique'),
            array('code', 'length', 'min'=>2, 'max'=>2),
        	array('image', 'EImageValidator', 'types'=>'gif', 'size' => 10, 'width' => 50, 'height' => 50, 'allowEmpty'=>true),
			array('code', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Code must have letters only.'),
			array('name','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'code' => Message::getTranslation(41),
			'name' => Message::getTranslation(10),
			'image' => Message::getTranslation(42),
		);
	}

	/**
    / **
     * Получает список категорий на основе текущих условий поиска / фильтров.
     *return CActiveDataProvider поставщика данных, который может вернуть необходимые сообщения.
     * /
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('code',$this->code, true);
		$criteria->compare('name',$this->name, true);

		return new CActiveDataProvider('Language', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'LOWER(code) ASC',
			),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(11),
			),
		));
	}
	
	public static function getLanguagesOptions()
	{
		return CHtml::listData(Language::model()->findAll(array('order' => 'code ASC')), 'code', 'name');
	}
	
	protected function beforeSave()
	{
		$this->code = trim(strtolower($this->code));
		$this->image=CUploadedFile::getInstance($this,'image');
		return true;
	}

	protected function afterSave()
	{
		if(!$this->isNewRecord && $this->code !== $this->oldcode)
		{
			if(file_exists(self::IMAGE_DIRECTORY . $this->oldcode . '.gif'))
				rename(self::IMAGE_DIRECTORY . $this->oldcode . '.gif', self::IMAGE_DIRECTORY . $this->code . '.gif');
		}
		if(!is_null($this->image))
		{
			if(file_exists(self::IMAGE_DIRECTORY . $this->oldcode . '.gif'))
				unlink(self::IMAGE_DIRECTORY . $this->oldcode . '.gif');
			$this->image->saveAs(self::IMAGE_DIRECTORY . $this->code . '.gif');
		}
	}
	
	protected function beforeDelete()
	{
		if(count(Post::model()->findAll('language =?', array($this->code))) == 0)
		{
			if(count(Category::model()->findAll('language =?', array($this->code))) == 0)
			{
				if(count(Message::model()->findAll('language =?', array($this->code))) == 0)
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	protected function afterDelete()
	{
		if(file_exists(self::IMAGE_DIRECTORY . $this->code . '.gif'))
			unlink(self::IMAGE_DIRECTORY . $this->code . '.gif');
	}

	public static function checkLanguage($language)
	{
		$languages = Language::model()->findAll();	
		
		foreach($languages as $lang)
		{
			if($language === $lang->code)
				return true;	
		}
		return false;				
	}
	
	public static function getNavigatorLanguage()
	{
		$language = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
		$language = strtolower(substr(chop($language[0]),0,2));
		return $language;
	}
	
	public static function getNameFromCode($code)
	{
		$language = self::model()->find('code = ?', array($code));
		if(!is_null($language))
			return $language->name;
		else
			return null;
	}
}
	