<?php

class Subscription extends CActiveRecord
{
	/**
     * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     *return Строка связана имя таблицы базы данных
	 */
	public function tableName()
	{
		return '{{subscription}}';
	}
	
	/**
     *return Массив реляционные правила.
	 */
	public function relations()
	{
		return array(
			'category' => array(self::BELONGS_TO, 'Category', array('category_id', 'language')),
		);
	}
}
	