<?php
class Message extends CActiveRecord
{
	private $oldId;
	private $oldLanguage;
	
	/**
     * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     *return Строка связана имя таблицы базы данных
	 */
	public function tableName()
	{
		return '{{message}}';
	}

	/**
     * Правила проверки return массив для атрибутов модели.
	 */
	public function rules()
	{
		return array(
			array('language', 'required'),
                        array('language', 'default', 'setOnEmpty' => true, 'value' => 'ru'),
			array('id, language, translation', 'safe', 'on' => 'search'),
			array('id', 'numerical', 'min' => 1),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
        // ПРИМЕЧАНИЕ: Вы, возможно, потребуется настроить имя отношения и связанные с
// Имя класса для отношений, автоматически сгенерированных ниже.
		return array(
			'lang' => array(self::BELONGS_TO, 'Language', 'language'),
		);
	}

	/**
     *return Массив индивидуальные этикетки атрибутов (имя => label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Message::getTranslation(27),
			'language' => Message::getTranslation(35),
			'translation' => Message::getTranslation(46),
		);
	}
	
	public static function getTranslation($id)
	{
		$translation = '';
		$message = Message::model()->find(array('condition' => 'id = :id AND language = :lang', 'params' => array(':id' => $id, ':lang' => Yii::app()->getLanguage())));
		if(is_null($message))
		{
			$message = Message::model()->findByPk(array('id' => $id, 'language' => Parameter::getValue(29)));
			if(is_null($message))
				$translation = "Нет доступных сообщений";
			else
				$translation = $message->translation;		
		}
		else
		{
			$translation = $message->translation;
		}
		return $translation;
	}

	/**
    / **
     * Получает список категорий на основе текущих условий поиска / фильтров.
     *return CActiveDataProvider поставщика данных, который может вернуть необходимые сообщения.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('language',$this->language);
		$criteria->compare('translation',$this->translation, true);

		return new CActiveDataProvider('Message', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'language, id ASC',
			),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(16),
			),
		));
	}
	
	public function afterFind()
	{
		$this->oldId = $this->id;
		$this->oldLanguage = $this->language;
	}
}
	