<?php

Yii::import('ext.EImageValidator');

class User extends CActiveRecord
{
	public $repeatPassword;
	public $image;
	public $verifyCode;
	public $role;
	public $oldrole;
	public $oldemail;
	public $ban_forever;
	public $subscribed_categories;
	public $onDeleteError;
	const IMAGE_DIRECTORY = 'images/avatars/';
	const DEFAULT_IMAGE = 'default.jpg';
	const STATUS_UNCONFIRMED = 1;
	const STATUS_BANNED = 2;
	const STATUS_ACTIVE = 3;
	const ROLE_READER = "reader";
	const ROLE_AUTHOR = "author";
	const ROLE_EDITOR = "editor";
	const ROLE_PUBLISHER = "publisher";
	const ROLE_ADMIN = "admin";

	/**
     * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     *return Строка связана имя таблицы базы данных
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
     * Правила проверкиreturn массив для атрибутов модели.
	 */
	public function rules()
	{
		return array(
			array('username', 'CRegularExpressionValidator', 'pattern'=>'/^([0-9a-z]+)$/'),
            array('password, repeatPassword', 'required', 'on'=>'insert, register'),
            array('ban_reason', 'banValidator'),
            array('password, repeatPassword, email', 'length', 'min'=>6, 'max'=>100),
            array('username', 'length', 'min'=>5, 'max'=>100),
			array('username, email', 'unique'),
			array('username, email, role, status', 'required'),
			array('email', 'email'),
			array('password', 'compare', 'compareAttribute'=>'repeatPassword', 'on' => 'insert, update, register'),
			array('username, email, url, role, status', 'safe', 'on'=>'search'),
			array('url', 'url'),
        	array('image', 'EImageValidator', 'types'=>'jpg, jpeg', 'size' => 20, 'width' => 100, 'height' => 100, 'allowEmpty'=>true),
			array('verifyCode', 'CaptchaExtendedValidator', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on' => 'register'),
			array('subscribe, ban_forever', 'boolean'),
			array('subscribed_categories, ban_expires', 'default'),
			array('username, password, additional_info','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
		);
	}

    public function banValidator($attribute,$params)
    { 		
        if($this->status == User::STATUS_BANNED){
        	if(is_null($this->ban_reason) || $this->ban_reason === "")
             	$this->addError($attribute, "Бан не может быть пустым.");
        }
    }

	/**
     *return Массив реляционные правила
	 */
	public function relations()
	{
		return array(
			'authAssignment' => array(self::HAS_ONE, 'AuthAssignment', 'userid'),
			'commentCount' => array(self::STAT, 'Comment', 'user_id', 'condition'=>'status='.Comment::STATUS_APPROVED),
		);
	}
	
	/**
     *return Массив индивидуальные этикетки атрибутов (имя => label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Message::getTranslation(27),
			'username' => Message::getTranslation(43),
			'password' => Message::getTranslation(44),
			'email' => Message::getTranslation(11),
			'repeatPassword' => Message::getTranslation(45),
			'image' => Message::getTranslation(42),
			'url' => Message::getTranslation(12),
			'status' => Message::getTranslation(30),
			'role' => Message::getTranslation(137),
			'create_time' => Message::getTranslation(31),
			'last_visit' => Message::getTranslation(138),
			'subscribe' => Message::getTranslation(150),
			'commentCount' => '#' . substr(Message::getTranslation(17),0,3),
			'verifyCode' => Message::getTranslation(149),
			'subscribed_categories' => Message::getTranslation(152),
			'additional_info' => Message::getTranslation(153),
			'ban_reason' => Message::getTranslation(177),
			'ban_expires' => Message::getTranslation(178),
			'ban_forever' => Message::getTranslation(179),
		);
	}

	/**
     * Получает список пользователей, основываясь на текущих условиях поиск / фильтр.
     *return CActiveDataProvider поставщика данных, который может вернуть необходимые сообщения.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->alias='u';
		$criteria->with = array('authAssignment'=>array('alias'=>'a'));
		$criteria->compare('username',$this->username, true);
		$criteria->compare('email',$this->email, true);
		$criteria->compare('url',$this->url, true);
		$criteria->compare('status',$this->status);
		$criteria->compare('itemname',$this->role);
		
		return new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'LOWER(username) ASC',
			),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(12),
			),
		));
	}

	protected function beforeSave()
	{
		$this->username = trim(strtolower($this->username));
		$this->image=CUploadedFile::getInstance($this,'image');
		if($this->ban_forever)
			$this->ban_expires = null;
		else
			$this->ban_expires = $this->ban_expires / 1000;
		
		if($this->status != User::STATUS_BANNED)
		{
			$this->ban_expires = null;
			$this->ban_reason = null;
		}

		if($this->scenario === 'register' || $this->scenario === 'insert')
		{
			$this->password = CPasswordHelper::hashPassword($this->password);
			$this->repeatPassword = CPasswordHelper::hashPassword($this->repeatPassword);		
			$this->token = self::generatePassword(20);
			$this->create_time = time();
		}
		elseif($this->scenario === 'update')
		{
			if($this->password === ''){
				$model2 = User::model()->findByPk($this->id);
				$this->password = $model2->password;
				$this->repeatPassword = $model2->password;
			}
			elseif($this->repeatPassword !== null) {
				$this->password = CPasswordHelper::hashPassword($this->password);
				$this->repeatPassword = CPasswordHelper::hashPassword($this->repeatPassword);					
			}
			
			if($this->email !== $this->oldemail && !Yii::app()->user->checkAccess('admin'))
			{
				$this->status = User::STATUS_UNCONFIRMED;
				$this->token = self::generatePassword(20);
			}
		}
	
		return true;
	}

	protected function afterSave()
	{
		if(!is_null($this->subscribed_categories))
		{
			Subscription::model()->deleteAll('user_id =' . $this->id);
			foreach($this->subscribed_categories as $category)
			{
				$subscribed_category_array = explode('|', $category);
				$subscription = new Subscription;
				$subscription->user_id = $this->id;
				$subscription->language = $subscribed_category_array[0];
				$subscription->category_id = $subscribed_category_array[1];
				if(!$subscription->save())
					return false;
			}
		}
		
		if($this->status == User::STATUS_UNCONFIRMED)
			$this->sendRegistrationMail();

		if(!is_null($this->image))
		{
			if(file_exists(self::IMAGE_DIRECTORY . $this->id . '.jpg'))
				unlink(self::IMAGE_DIRECTORY . $this->id . '.jpg');
			$this->image->saveAs(self::IMAGE_DIRECTORY . $this->id . '.jpg');
		}

		Yii::app()->authManager->revoke($this->oldrole,$this->id);
		Yii::app()->authManager->assign($this->role,$this->id);
	}

	protected function beforeDelete()
	{
		if($this->authAssignment->itemname === 'admin')
		{
			$this->onDeleteError = 1;		
			return false;
		}
	
		if(count(Post::model()->findAll('user_id =?', array($this->id))) == 0)
		{
			if(count(Comment::model()->findAll('user_id =?', array($this->id))) == 0)
			{
				if(count(Subscription::model()->findAll('user_id =?', array($this->id))) == 0)
				{
					$this->onDeleteError = 0;
					return true;
				}
			}
		}
		
		$this->onDeleteError = 2;
		return false;
	}
	
	protected function afterDelete()
	{
		if(file_exists(self::IMAGE_DIRECTORY . $this->id . '.jpg'))
			unlink(self::IMAGE_DIRECTORY . $this->id . '.jpg');
	}
	
	protected function afterFind()
	{
		$this->role = AuthAssignment::model()->find('userid=?', array($this->id))->itemname;
		$this->oldrole = $this->role;
		$this->oldemail = $this->email;
	}
	
	public function getImageFile()
	{
		$imagefile = '';
		if(!is_null($this->id) && file_exists(self::IMAGE_DIRECTORY . $this->id . '.jpg'))
			$imagefile = $this->id . '.jpg';
		else
			$imagefile = self::DEFAULT_IMAGE;
		
		return $imagefile;		
	}

	public function sendEmail($senderName, $senderEmail, $subject, $body)
	{
		Yii::import('ext.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['emailHost'];
		$mail->Port = Yii::app()->params['emailPort']; 
		$mail->SMTPAuth = true;
		$mail->Username = Yii::app()->params['emailUsername'];
		$mail->Password = Yii::app()->params['emailPassword'];
		$mail->SetFrom($senderEmail, $senderName);
		$mail->Subject = $subject;
		$mail->AltBody = Yii::app()->params['emailAltMessage'];
		$mail->MsgHTML($body);
		$mail->AddAddress($this->email, $this->username);
		$mail->Send();	
	}

	public function sendRegistrationMail()
	{
		$confirmUrl = Yii::app()->createAbsoluteUrl('/blog/user/confirm', array('lang' => Yii::app()->getLanguage(), 'token' => $this->token));
		$message = Message::getTranslation(108) . '<br/><br/>' . CHtml::link($confirmUrl, $confirmUrl, array('target' => '_blank'));
		$this->sendEmail(Parameter::getValue(28), Parameter::getValue(27), Message::getTranslation(109), $message);
	}
	
	public function sendNews($title, $content)
	{		
		$unsubscribeUrl = Yii::app()->createAbsoluteUrl('/blog/user/unsubscribe', array('lang' => Yii::app()->getLanguage(), 'token' => $this->token));	
		$message = $content . '<br/><br/>' . CHtml::link(Message::getTranslation(140), $unsubscribeUrl, array('target' => '_blank'));
		$this->sendEmail(Parameter::getValue(28), Parameter::getValue(27), $title, $message);		
	}
	
	public function sendNewPassword($password)
	{
		$activatePasswordUrl = Yii::app()->createAbsoluteUrl('/blog/user/activatePassword', array('lang' => Yii::app()->getLanguage(), 'token' => $this->token));	
		$message = Message::getTranslation(146) . '<p>' . $activatePasswordUrl .  '</p><p>' . Message::getTranslation(147) . '</p><p>' . $password . '</p>';
		$this->sendEmail(Parameter::getValue(28), Parameter::getValue(27), Message::getTranslation(145), $message);		
	}
	
	public function getSubscriptionOptions()
	{
		$listData = array();
		
		if(!is_null($this->subscribed_categories))
		{
			foreach($this->subscribed_categories as $category_id)
			{
				$category_array = explode('|', $category_id);
				$category = Category::model()->findByPk(array('language' => $category_array[0], 'id' => $category_array[1]));
				$listData[$category->language . '|' . $category->id] = $category->language . ': ' . $category->name;
			}					
		}
		elseif(!is_null($this->id))
		{
			$subscriptions = Subscription::model()->findAll(array('condition' => 'user_id = ' . $this->id));
			foreach($subscriptions as $subscription) {
				$listData[$subscription->language . '|' . $subscription->category_id] = $subscription->category->language . ': ' . $subscription->category->name;
			}
		}

		return $listData;
	}
	
	public static function generatePassword($length = 8) 
	{
	    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	    $count = mb_strlen($chars);
	
	    for ($i = 0, $result = ''; $i < $length; $i++) {
	        $index = rand(0, $count - 1);
	        $result .= mb_substr($chars, $index, 1);
	    }
	
	    return $result;
	}
	
	/**
     * Предлагает список существующих имен пользователей, соответствующих указанное ключевое слово.
     *param Строка Ключевое слово быть согласованы
     *param Число максимальное количество имен пользователей должны быть возвращены
     * Списокreturn массив совпадающих имен пользователей
	 */
	public function suggestUsers($keyword,$limit)
	{
		$users=User::model()->findAll(array(
			'condition'=>'username LIKE :keyword',
			'order'=>'username ASC',
			'limit'=>$limit,
			'params'=>array(
				':keyword'=>'%'.strtr($keyword,array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')).'%',
			),
		));
		$names=array();
		foreach($users as $user)
			$names[]=$user->username;
		return $names;
	}
}
