<?php

/**
/ **
  * Класс LoginForm.
  * LoginForm это структура данных для хранения
  * Данные пользователь Войти форме. Он используется в 'Login' действия '' SiteController.
  * /
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $rememberMe;

	private $_identity;

	/**
    / **
     * Объявляет правила валидации.
     * Правила гласят, что имя пользователя и пароль не требуется,
     * И пароль должен пройти проверку подлинности.
     * /
	 */
	public function rules()
	{
		return array(
            // Имя пользователя и пароль требуются
			array('username, password', 'required'),
            // RememberMe должен быть логический
			array('rememberMe', 'boolean'),
			/// Пароль должен пройти проверку подлинности
			array('password', 'authenticate'),
		);
	}

	/**
    Объявляет атрибут label.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe' => Message::getTranslation(114),
			'username' => Message::getTranslation(112),
			'password' => Message::getTranslation(44),
		);
	}

	/**
    / **
     * Аутентификацию пароль.
     * Это "аутентификацию" валидатор, как заявлено в правилах ().
     * /
	 */
	public function authenticate($attribute,$params)
	{
		$this->_identity=new UserIdentity($this->username,$this->password);
		if(!$this->_identity->authenticate())
			$this->addError('password','Не правильный логин или пароль.');
	}

	/**
    / **
     * Журналы в пользователя, используя данную логин и пароль в модели.
     *return Булево ли успешно Войти
     * /
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
		{			
			return false;
		}
	}
}
