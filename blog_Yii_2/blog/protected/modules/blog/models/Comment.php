<?php

class Comment extends CActiveRecord
{
	public $postCount;
	public $verifyCode;
	public $maxId;
	const STATUS_PENDING=1;
	const STATUS_APPROVED=2;

	/**
	 * R **
     * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
     * /
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * / **
     *return Строка связана имя таблицы базы данных
     * /
	 */
	public function tableName()
	{
		return '{{comment}}';
	}

	/**
	 ** Правила проверкиreturn массив для атрибутов модели.
	 */
	public function rules()
	{
		$p = new CHtmlPurifier();
		$p->options = array('HTML.Nofollow'=> true);	
		return array(
			array('content, status', 'required'),
			array('author, email', 'required', 'on' => 'guestcomment, updateguest'),
			array('content', 'length', 'max'=>1000),
			array('author, email, url', 'length', 'max'=>128),
			array('email','email'),
			array('url','url'),
			array('comment_id','numerical'),
			array('id, content, post_id, status, user_id, author', 'safe', 'on'=>'search'),
			array('url, content, author','filter','filter'=>array($p,'purify')),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'on' => 'guestcomment'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			//'post' => array(self::BELONGS_TO, 'Post', array('post_id', 'category_id', 'language')),
                        'post' => array(self::BELONGS_TO, 'Post', 'post_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}
	
	/**
	 *return массив настроены этикетки атрибутов (имя => label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Message::getTranslation(27),
			'content' => Message::getTranslation(13),
			'status' => Message::getTranslation(30),
			'create_time' => Message::getTranslation(31),
			'author' => Message::getTranslation(33),
			'email' => Message::getTranslation(11),
			'url' => Message::getTranslation(12),
			'post_id' => Message::getTranslation(38),
			'comment_id' => Message::getTranslation(39),
			'user_id' => Message::getTranslation(112),
		);
	}

	public function getApprovedComments()
	{
		return Comment::model()->findAll("comment_id = ? AND post_id = ? AND category_id = ? AND language = ? AND status = ?", array($this->id, $this->post_id, $this->category_id, $this->language, Comment::STATUS_APPROVED));
	}

	public function getAllComments()
	{
		return Comment::model()->findAll("comment_id = ? AND post_id = ? AND category_id = ? AND language = ?", array($this->id, $this->post_id, $this->category_id, $this->language));
	}
	
	/**
	 * Утверждает комментарий.
	 */
	public function approve()
	{
		$this->status=Comment::STATUS_APPROVED;
		$this->update(array('status'));
	}

	/**
	 * param Сообщение пост, что этот комментарий относится. Если NULL, метод
     * Будет запрашивать должность.
     *return Строка Постоянная ссылка URL для этого комментария
     * /
	 */
	public function getUrl($post=null)
	{
		if($post===null)
			$post=$this->post;
		return $post->url.'#c'.$this->id;
	}

	/**
	 * *return Строка дисплея гиперссылка на автора текущего комментария
     * /
	 */
	public function getAuthorLink()
	{
		if(is_null($this->user_id)){
			if(!empty($this->url) && Parameter::getValue(17))
				return '<span class="jslink" style="cursor:pointer" title="' . CHtml::encode($this->url) . '">' . CHtml::encode($this->author) . '</span>';
			else
				return CHtml::encode($this->author);
		}
		else {
			if(Yii::app()->user->isGuest)
				return $this->user->username;
			else
				return CHtml::link(CHtml::encode($this->user->username),array('/blog/user/view', 'user' => $this->user_id, 'lang' => Yii::app()->getLanguage()));
		}
	}

	/**
	 * *return Целое число комментариев, ожидающих одобрения
	 */
	public function getPendingCommentCount()
	{
		return $this->count('status='.self::STATUS_PENDING);
	}

	/**
	 * / **
     *param Число максимальное количество комментариев, которые должны быть возвращены
     *return Массив наиболее недавно добавленные комментарии
     * /
	 */
	public function findRecentComments($limit=10)
	{
		$categories = substr(Category::getSubcategoriesIdsR(Category::model()->findAll(array('condition' => 'language =:language AND category_id IS NULL','order' => 'LOWER(name) ASC', 'params' => array(':language' => Yii::app()->getLanguage())))),1);
		return $this->with('post')->findAll(array(
			'condition'=>'t.status='.self::STATUS_APPROVED . ' AND t.language=\'' . Yii::app()->getLanguage() . '\' AND post.status=' . Post::STATUS_PUBLISHED . ' AND t.category_id IN(' . $categories . ')' ,
			'order'=>'t.create_time DESC',
			'limit'=>$limit,
		));
	}

	/**
     * Это вызывается перед запись сохранена.
     *return Булево должны ли сохранены запись.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$comment = Comment::find(array('select' => 'MAX(id) as maxId', 'condition' => 'post_id = ? AND category_id = ? AND language = ?', 'params' => array($this->post_id, $this->category_id, $this->language)));
				$this->id = $comment->maxId + 1;
				$this->create_time=time();
			}
			return true;
		}
		else
			return false;
	}

	/**
	 * / **
     * Получает список сообщений, основываясь на текущих условиях поиск / фильтр.
     *return CActiveDataProvider поставщика данных, который может вернуть необходимые сообщения.
     * /
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->alias='c';
		$criteria->with = array('post'=>array('alias'=>'p'), 'user'=>array('alias'=>'u'));
		$criteria->compare('p.title', $this->post_id,true);
		$criteria->compare('c.id',$this->id);
		$criteria->compare('c.content',$this->content,true);
		$criteria->compare('c.status',$this->status);
		$criteria->compare('u.username',$this->user_id,true);
		$criteria->compare('c.author',$this->author,true);
		if(Yii::app()->user->checkAccess('reader') || Yii::app()->user->checkAccess('author') || 
		Yii::app()->user->checkAccess('editor') || Yii::app()->user->checkAccess('publisher'))
			$criteria->compare('c.user_id', Yii::app()->user->id);
		
		return new CActiveDataProvider('Comment', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'c.status, c.create_time DESC',
			),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(9),
			),
		));
	}
	
	protected function beforeDelete()
	{
		if(count($this->getAllComments()) > 0)
		{
			return false;
		}
		
		return true;
	}
}