<?php

class Lookup extends CActiveRecord
{

	private static $_items=array();

	/**
    / **
     * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
     * /
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
   return  Строка связана имя таблицы базы данных
	 */
	public function tableName()
	{
		return '{{lookup}}';
	}

	/**
    / **
     * Возвращает элементы для указанного типа.
     *param Строка тип элемента (например "PostStatus ').
     * Именаreturn элемент массива индексируются Код товара. Детали заказа их значений позиции.
     * Возвращается пустой массив, если тип элемента не существует.
     * /
	 */
	public static function items($type)
	{
		if(!isset(self::$_items[$type]))
			self::loadItems($type);
		return self::$_items[$type];
	}

	/**
     * Возвращает имя элемента для указанного типа и кода.
     *param Строка тип элемента (например, «PostStatus ').
     *param Число кода товара (соответствующий значению 'CODE' колонка)
     *return Строка имя элемента для указанного кода. Ложные возвращается, если тип или код элемент не существует.
	 */
	public static function item($type,$code)
	{
		if(!isset(self::$_items[$type]))
			self::loadItems($type);
		return isset(self::$_items[$type][$code]) ? self::$_items[$type][$code] : false;
	}

	/**
     * Загружает справочные пункты для указанного типа из базы данных.
     *param Строка тип элемента
	 */
	private static function loadItems($type)
	{
		self::$_items[$type]=array();
		$models=self::model()->findAll(array(
			'condition'=>'type=:type',
			'params'=>array(':type'=>$type),
			'order'=>'position',
		));
		foreach($models as $model)
			self::$_items[$type][$model->code]=$model->name;
	}
}