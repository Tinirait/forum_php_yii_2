<?php

class Tag extends CActiveRecord
{
	/**
    / **
     * Ниже приведены доступные столбцы в таблице "tbl_tag":
     *var Число $ ID
     *var Строка $ имя
     *var Число $ частота
     * /

    / **
     * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
     * /
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
     *return Строка связана имя таблицы базы данных
	 */
	public function tableName()
	{
		return '{{tag}}';
	}

	/**
     * Правила проверкиreturn массив для атрибутов модели.
	 */
	public function rules()
	{
        // ПРИМЕЧАНИЕ: Вы должны только определить правила для тех атрибутов, которые
// Получите вводимых пользователем.
		return array(
			array('name', 'required'),
			array('frequency', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>128),
		);
	}

	/**
     *return Массив индивидуальные этикетки атрибутов (имя => label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ИД',
			'name' => 'Название',
			'frequency' => 'Частота',
		);
	}

	/**
     * Возвращает имена тегов и их соответствующие веса.
     * Только теги с верхних весов будут возвращены.
     *param Число максимальное количество тегов, которые должны быть возвращены
     * Весreturn массив, индексированный по именам тегов.
	 */
	public function findTagWeights($limit=20)
	{
		$categories = substr(Category::getSubcategoriesIdsR(Category::model()->findAll(array('condition' => 'language =:language AND category_id IS NULL','order' => 'LOWER(name) ASC', 'params' => array(':language' => Yii::app()->getLanguage())))),1);
		$tagsArray = array();
		$posts = Post::model()->findAll('language=? AND status=? AND category_id IN(' . $categories . ')', array(Yii::app()->getLanguage(), Post::STATUS_PUBLISHED));
		foreach($posts as $post)
		{
			$tagsArray2 = explode(',', $post->tags);
			foreach($tagsArray2 as $tag)
			{
				$tagsArray[] = '\'' . $tag . '\'';
			}
		}
		
		$models=$this->findAll(array(
			'condition' => 'language=\'' . Yii::app()->getLanguage() . '\' AND name IN('. implode(',', $tagsArray) . ')' ,
			'order'=>'frequency DESC',
			'limit'=>$limit,
		));

		$total=0;
		foreach($models as $model)
			$total+=$model->frequency;

		$tags=array();
		if($total>0)
		{
			foreach($models as $model)
				$tags[$model->name]=8+(int)(16*$model->frequency/($total+10));
			ksort($tags);
		}
		return $tags;
	}

	/**
     * Предлагает список существующих тегов, соответствующих указанное ключевое слово.
     *param Строка Ключевое слово быть согласованы
     *param Число максимальное количество тегов должны быть возвращены
     *return Список массив соответствующих имен тэгов
	 */
	public function suggestTags($keyword,$limit,$language)
	{
		$tags=$this->findAll(array(
			'condition'=>'name LIKE :keyword AND language=\'' . $language . '\'',
			'order'=>'frequency DESC, Name',
			'limit'=>$limit,
			'params'=>array(
				':keyword'=>'%'.strtr($keyword,array('%'=>'\%', '_'=>'\_', '\\'=>'\\\\')).'%',
			),
		));
		$names=array();
		foreach($tags as $tag)
			$names[]=$tag->name;
		return $names;
	}

	public static function string2array($tags)
	{
		return preg_split('/\s*,\s*/',trim($tags),-1,PREG_SPLIT_NO_EMPTY);
	}

	public static function array2string($tags)
	{
		return implode(',',$tags);
	}

	public function updateFrequency($oldTags, $newTags, $language)
	{
		$oldTags=self::string2array($oldTags);
		$newTags=self::string2array($newTags);
		$this->addTags(array_values(array_diff($newTags,$oldTags)), $language);
		$this->removeTags(array_values(array_diff($oldTags,$newTags)), $language);
	}

	public function addTags($tags, $language)
	{
		$criteria=new CDbCriteria;
		$criteria->addInCondition('name',$tags);
		$criteria->addCondition('language=\'' . $language . '\'');
		$this->updateCounters(array('frequency'=>1),$criteria);
		foreach($tags as $name)
		{
			if(!$this->exists('name=:name AND language=:language',array(':name'=>$name, ':language'=>$language)))
			{
				$tag=new Tag;
				$tag->name=$name;
				$tag->language=$language;
				$tag->frequency=1;
				$tag->save();
			}
		}
	}

	public function removeTags($tags, $language)
	{
		if(empty($tags))
			return;
		$criteria=new CDbCriteria;
		$criteria->addInCondition('name',$tags);
		$criteria->addCondition('language=\'' . $language . '\'');
		$this->updateCounters(array('frequency'=>-1),$criteria);
		$this->deleteAll('frequency<=0 AND language=\'' . $language . '\'');
	}
}