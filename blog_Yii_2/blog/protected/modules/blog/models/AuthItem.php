<?php

class AuthItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{auth_item}}';
	}
	
	public static function getRolesOptions()
	{
		return CHtml::listData(self::model()->findAll(array('condition' => 'type=2')), 'name', 'description');		
	}
}
	