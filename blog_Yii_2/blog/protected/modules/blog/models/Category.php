<?php

class Category extends CActiveRecord
{
	const STATUS_UNPUBLISHED=1;
	const STATUS_PUBLISHED=2;

	/**
	 * / **
     * Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
     * /
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     *return Строка связана имя таблицы базы данных
	 */
	public function tableName()
	{
		return '{{category}}';
	}

	/**
     * Правила проверкиreturn массив для атрибутов модели.
	 */
	public function rules()
	{
        // ПРИМЕЧАНИЕ: Вы должны только определить правила для тех атрибутов, которые
// Получите вводимых пользователем.
		return array(
			array('name, status', 'required'),
			array('id, category_id', 'numerical', 'min' => 1 , 'max' => 999),
                        array('language','default', 'setOnEmpty' => true, 'value' => 'ru'),
			array('id, name, status, category_id, language', 'safe', 'on'=>'search'),
			array('name','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
	        array('id', 'unique', 'criteria'=>array(
	            'condition'=>'language=:language',
	            'params'=>array(
	                ':language'=>$this->language,	                
	            )
	        ), 'on' => 'update', 'message' => Message::getTranslation(195)),
		);
	}

	/**
	 * return массив реляционных правил.
	 */
	public function relations()
	{
		return array(
			'posts' => array(self::HAS_MANY, 'Post', array('category_id', 'language')),
			'categories' => array(self::HAS_MANY, 'Category', array('category_id', 'language'),'order' => 'LOWER(name) ASC'),
			'category' => array(self::BELONGS_TO, 'Category', array('category_id', 'language')),
		);
	}

	/**
     *return Массив индивидуальные этикетки атрибутов (имя => label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Message::getTranslation(27),
			'name' => Message::getTranslation(10),
			'language' => Message::getTranslation(35),
			'category_id' => Message::getTranslation(40),
			'status' => Message::getTranslation(30),
		);
	}

	/**
	 * Получает список категорий на основе текущих условий поиска / фильтров.
     *return CActiveDataProvider поставщика данных, который может вернуть необходимые сообщения.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->alias='c';
		$criteria->with = array('category'=>array('alias'=>'p'));
		$criteria->compare('c.id',$this->id);
		$criteria->compare('c.name',$this->name,true);
		$criteria->compare('p.name', $this->category_id,true);
		$criteria->compare('c.language',$this->language);
		$criteria->compare('c.status',$this->status);

		return new CActiveDataProvider('Category', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'LOWER(c.name) ASC',
			),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(10),
			),
		));
	}
	
	public static function getCategoriesOptions($language)
	{
		$categories = Category::model()->findAll(array('condition' => 'language =\'' . $language . '\' AND category_id IS NULL','order' => 'LOWER(name) ASC'));
		return self::getListData($categories, 0);
	}

	private static function getListData($categories, $level)
	{
		$listData = array();
			
		foreach($categories as $category)
		{
			$lines = '';
			for($i = 0; $i < $level; $i++)
				$lines .= '--';
			$listData[$category->id] = $lines . $category->name;
			$listData += self::getListData($category->categories, $level + 1);
		}
		
		return $listData;
	}
	
	protected function beforeDelete()
	{
		if(count($this->posts) > 0 || count($this->categories) > 0)
			return false;
		
		return true;
	}

	public function getSubcategoriesIds()
	{
		$subcategoriesids = self::getSubcategoriesIdsR($this->categories);
		return $this->id . ($subcategoriesids === '' ? '' : $subcategoriesids);
	}
		
	public static function getSubcategoriesIdsR($categories)
	{
		$subcategories = '';
		foreach($categories as $category)
		{
			if($category->status == Category::STATUS_PUBLISHED)
			{
				$subcategories .= ',' . $category->id;
				$subcategories .= self::getSubcategoriesIdsR($category->categories);
			}
		}
		return $subcategories;
	}
	
	public function areParentCategoriesPublished()
	{
		$categories_published = true;
		$category = $this;
		while(true)
		{
			if($category->status == Category::STATUS_UNPUBLISHED)
			{
				$categories_published = false;
				break;
			}
			if(is_null($category->category_id))
				break;
			$category = $category->category;
		}
		return $categories_published;
	}		
}
	