<?php
class Parameter extends CActiveRecord
{
	/**
    Возвращает статическую модель указанного класса AR.
     *return CActiveRecord статический класс модели
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
     *return Строка связана имя таблицы базы данных
	 */
	public function tableName()
	{
		return '{{parameter}}';
	}

	/**
     * Правила проверкиreturn массив для атрибутов модели.
	 */
	public function rules()
	{
		return array(
			array('id, description, value', 'required'),
			array('description, value','filter','filter'=>array($obj=new CHtmlPurifier(),'purify')),
		);
	}
	
	/**
     *return Массив индивидуальные этикетки атрибутов (имя => label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Message::getTranslation(27),
			'description' => Message::getTranslation(47),
			'value' => Message::getTranslation(48),
		);
	}
	
	public static function getValue($key)
	{
		$parameter = Parameter::model()->findByPk($key);
		if(!is_null($parameter))
			return $parameter->value;
		return "0";
	}
	
	/**
     * Получает список категорий на основе текущих условий поиска / фильтров.
     *return CActiveDataProvider поставщика данных, который может вернуть необходимые сообщения.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider('Parameter', array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'id ASC',
			),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(13),
			),
		));
	}
}
	