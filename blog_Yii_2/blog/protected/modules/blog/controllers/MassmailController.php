<?php

class MassmailController extends BlogController
{
	public $layout = '//layouts/blog';
	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		$filters = array(
			'accessControl', // perform access control for CRUD operations
		);
		
		return array_merge($filters, parent::filters());
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'roles'=>array('admin'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$post=null;
		
		if(isset($_GET['massmail']))
		{
			if(Yii::app()->user->checkAccess('admin') || Yii::app()->user->checkAccess('publisher') || Yii::app()->user->checkAccess('editor'))
				$condition='';
			elseif(Yii::app()->user->checkAccess('author'))
				$condition='status='.Post::STATUS_PUBLISHED . ' OR user_id=' . Yii::app()->user->id;
			elseif(Yii::app()->user->checkAccess('reader') || Yii::app()->user->isGuest)
				$condition='status='.Post::STATUS_PUBLISHED;
			
			$post=Post::model()->findByPk($_GET['massmail'], $condition);
			
			if(!is_null($post) && (Yii::app()->user->checkAccess('reader') || Yii::app()->user->isGuest))
			{
				$category = $post->category;
				while(true)
				{
					if($category->status == Category::STATUS_UNPUBLISHED)
					{
						$post = null;
						break;
					}
					if(is_null($category->category_id))
						break;
					$category = $category->category;
				}
			}
		}
		if($post===null)
			throw new CHttpException(404,Message::getTranslation(94));		
	
		
		$comment=new Comment;
		if(Yii::app()->user->isGuest)
			$comment->scenario = "guestcomment";
		if(isset($_POST['ajax']) && $_POST['ajax']==='comment-form')
		{
			echo CActiveForm::validate($comment);
			Yii::app()->end();
		}
		if(isset($_POST['Comment']))
		{
			$comment->attributes=$_POST['Comment'];
			if($post->addComment($comment))
			{
				if($comment->status==Comment::STATUS_PENDING)
					Yii::app()->user->setFlash('commentSubmitted',Message::getTranslation(75));
				$this->refresh();
			}
		}

		$this->render('view',array(
			'model'=>$post,
			'comment'=>$comment,
		));
	}

	/**
	 * Creates a new model.
	 */
	public function actionCreate()
	{
		$model=new MassMail('insert');
		$model->status = MassMail::STATUS_DRAFT;
		$model->oldstatus = MassMail::STATUS_DRAFT;
		$model->type = MassMail::TYPE_MESSAGE;
		$model->categories = array();
		
		if(isset($_POST['MassMail']))
		{
			$model->attributes = $_POST['MassMail'];
			if(!isset($_POST['MassMail']['categories']))
				$model->categories = array();
			if(!isset($_POST['MassMail']['roles']))
				$model->roles = array();
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success',Message::getTranslation(72));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
			$model->setCategories();
			$model->status = MassMail::STATUS_DRAFT;
			$model->oldstatus = MassMail::STATUS_DRAFT;
		}
		
		$this->render('create',array(
			'model'=>$model,
		));
	}

 
	/**
	 * Updates a particular model.
	 */
	public function actionUpdate()
	{
		$model = null;
		if(isset($_GET['massmail']))
			$model=MassMail::model()->findbyPk($_GET['massmail']);
		else
			throw new CHttpException(404,Message::getTranslation(94));

		$model->scenario = 'update';
		
		if(isset($_POST['MassMail']))
		{
			$model->attributes = $_POST['MassMail'];
			if(!isset($_POST['MassMail']['categories']))
				$model->categories = array();
			if(!isset($_POST['MassMail']['roles']))
				$model->roles = array();
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success',Message::getTranslation(174));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
			$model->setCategories();
			$model->status = MassMail::STATUS_DRAFT;
			$model->oldstatus = MassMail::STATUS_DRAFT;
		}
		
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 */
	public function actionDelete()
	{
		$model=null;
		
		if(isset($_GET['massmail']))
		{
			if(Yii::app()->user->checkAccess('admin')  || Yii::app()->user->checkAccess('publisher') || Yii::app()->user->checkAccess('editor'))
				$condition='';
			elseif(Yii::app()->user->checkAccess('author'))
				$condition='user_id=' . Yii::app()->user->id;
			
			$model=MassMail::model()->findByPk($_GET['massmail'], $condition);
		}
		if($model === null)
			throw new CHttpException(404,Message::getTranslation(94));
		
		$model->delete();
		Yii::app()->user->setFlash('success',Message::getTranslation(175));
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{	
			$this->redirect(array('admin', 'lang' => $_GET['lang']));
		}
	}

	private function getSubcategories($categories)
	{
		$subcategories = '';
		foreach($categories as $category)
		{
			if(!Yii::app()->user->isGuest || $category->status == Category::STATUS_PUBLISHED)
			{
				$subcategories .= ',' . $category->id;
				$subcategories .= $this->getSubcategories($category->categories);
			}
		}
		return $subcategories;
	}
	
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MassMail('search');
		$model->unsetAttributes();
		if(isset($_GET['MassMail']))
			$model->attributes=$_GET['MassMail'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionCategoriesByLanguage()
	{
		$categories = Category::model()->findAll(array('condition' => 'language =\'' . $_POST['Post']['language']. '\' AND category_id IS NULL','order' => 'LOWER(name) ASC'));
		$this->getListData($categories, 0);	
	}
	
	private function getListData($categories, $level)
	{			
		foreach($categories as $category)
		{
			$lines = '';
			for($i = 0; $i < $level; $i++)
				$lines .= '--';
			echo "<option value=\"{$category->id}\">{$lines}{$category->name}</option>";
			$this->getListData($category->categories, $level + 1);
		}		
	}
}
