<?php

class SiteController extends BlogController
{

	public $layout = '//layouts/blog';
	
	/**
	 * Объявляет действия на основе классов.
	 */
	public function actions()
	{
		return array(
			'captcha'=>array(
                'class'=>'CaptchaExtendedAction',
                // если нужно при необходимости, изменить настройки
                'testLimit' => 1,

			),
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * Это действие обрабатывать внешние исключения.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	public function actionBanned()
	{
		if(isset($_GET['username']))
			$model = User::model()->find('username=?', array($_GET['username']));
		
		if(is_null($model))
			throw new CHttpException(404,Message::getTranslation(94));		
		
		$this->render('banned', array('model' => $model));
	}
	
	/**
	 * Log из текущего пользователя и перенаправить на домашнюю страницу
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('/blog/post/index','lang' => Yii::app()->getLanguage()));
	}
}
