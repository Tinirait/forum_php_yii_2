<?php

class LanguageController extends BlogController
{
	public $layout = '//layouts/blog';
	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;
	private $oldcode;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		$filters = array(
			'accessControl', // perform access control for CRUD operations
		);
		
		return array_merge($filters, parent::filters());
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin users to access all actions
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 */
	public function actionCreate()
	{
		$model=new Language;
		if(isset($_POST['Language']))
		{
			$model->attributes=$_POST['Language'];

			if($model->save())
			{
				Yii::app()->user->setFlash('success',Message::getTranslation(82));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		$model->oldcode = $model->code;
		
		if(isset($_POST['Language']))
		{
			$model->attributes=$_POST['Language'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success',Message::getTranslation(83));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 */
	public function actionDelete()
	{
		// we only allow deletion via POST request
		if($this->loadModel()->delete())
			Yii::app()->user->setFlash('success',Message::getTranslation(84));
		else
			Yii::app()->user->setFlash('error',Message::getTranslation(142));


		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{
			$this->redirect(array('admin', 'lang' => $_GET['lang']));
		}
	}
	
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Language('search');
		$model->unsetAttributes();
		if(isset($_GET['Language']))
			$model->attributes=$_GET['Language'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['language']))
			{
				$this->_model=Language::model()->findByPk($_GET['language']);
			}
			if($this->_model===null)
				throw new CHttpException(404,Message::getTranslation(94));
		}
		return $this->_model;
	}
}
