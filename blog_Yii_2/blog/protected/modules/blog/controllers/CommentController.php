<?php

class CommentController extends BlogController
{
	public $layout = '//layouts/blog';

	/**
	 * @var CActiveRecord текущий загруженная модель данных.
	 */
	private $_model;
	
	/**
	 * @return фильтры действий массива
	 */
	public function filters()
	{
		$filters = array(
			'accessControl', // осуществляется контроль доступа к CRUD операций
		);
		
		return array_merge($filters, parent::filters());
	}

	/**
	 * Определяет правила управления доступом.
     * Этот метод используется фильтром '' AccessControl.
     * return Правила контроля  доступа к массиву
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('update','admin'),
				'roles'=>array('reader','author', 'editor', 'publisher'),
			),
			array('allow', // allow admin users to access all actions
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Обновления конкретной модели.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		
		if( Yii::app()->user->checkAccess('admin') || ((Yii::app()->user->checkAccess('reader') 
		|| Yii::app()->user->checkAccess('author') || Yii::app()->user->checkAccess('editor')
		|| Yii::app()->user->checkAccess('publisher')) && $model->user_id == Yii::app()->user->id ))
		{
			if(!Yii::app()->user->checkAccess('admin'))
				$model->status = Comment::STATUS_PENDING;
			
			if(is_null($model->user_id))
				$model->scenario = 'updateguest';
			else 
				$model->scenario = 'updateuser';
			
			if(isset($_POST['ajax']) && $_POST['ajax']==='comment-form')
			{
				echo CActiveForm::validate($model);
				Yii::app()->end();
			}
			if(isset($_POST['Comment']))
			{
				$model->attributes=$_POST['Comment'];
				if($model->save())
				{
					Yii::app()->user->setFlash('success',Message::getTranslation(76));
					$this->redirect(array('admin', 'lang' => $_POST['lang']));
				}
			}
	
			$this->render('update',array(
				'model'=>$model,
			));
		}
		else {
			throw new CHttpException(404,Message::getTranslation(94));
		}
	}

	/**
	 * Удаляет конкретную модель.
	 */
	public function actionDelete()
	{
		if($this->loadModel()->delete()){
			Yii::app()->user->setFlash('success',Message::getTranslation(77));
		}
		else {
			Yii::app()->user->setFlash('error', Message::getTranslation(93));							
		}
		$this->redirect(array('admin', 'lang' => $_GET['lang']));
	}
	
	
	/**
	 * Управляет всеми моделями.
	 */
	public function actionAdmin()
	{
		$model=new Comment('search');
		$model->unsetAttributes();
		if(isset($_GET['Comment']))
			$model->attributes=$_GET['Comment'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	/**
	 * Список всех моделей.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Comment', array(
			'criteria'=>array(
				'with'=>'post',
				'order'=>'t.status, t.create_time DESC',
			),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(9),
			),
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Утверждаем особое комментарий.
	 */
	public function actionApprove()
	{
		$comment=$this->loadModel();
		$comment->approve();
		Yii::app()->user->setFlash('success',Message::getTranslation(96));
		$this->redirect(array('admin', 'lang' => $_GET['lang']));
	}

	/**
	 * Возвращает модель данных на основе первичного ключа, указанному в переменной GET.
     * Если модель данных не найден,в  HTTP возникнет исключение.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['comment']) && isset($_GET['post']) && isset($_GET['category']) && isset($_GET['language']))
				$this->_model=Comment::model()->findbyPk(array('id' => $_GET['comment'], 'post_id' => $_GET['post'], 'category_id' => $_GET['category'], 'language' => $_GET['language'], ));
			if($this->_model===null)
				throw new CHttpException(404,Message::getTranslation(94));
		}
		return $this->_model;
	}
}
