<?php

class ParameterController extends BlogController
{
	public $layout = '//layouts/blog';
	/**
	 * @var CActiveRecord текущий загруженный экземпляр модели данных.
	 */
	private $_model;
	/**
	 *return массив фильтров действий
	 */
	public function filters()
	{
		$filters = array(
			'accessControl', //осуществляет контроль доступа к CRUD операций
		);
		
		return array_merge($filters, parent::filters());
	}

	/**
	 * Определяет правила управления доступом.
     * Этот метод используется фильтром '' AccessControl.
     * Правила контроляreturn доступа к массиву
	 */
	public function accessRules()
	{
		return array(
			array('allow', // позволяют пользователи с правами администратора для доступа к все действия
				'roles'=>array('admin'),
			),
			array('deny',  // все пользователи
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Обновления конкретной модели..
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		
		if(isset($_POST['Parameter']))
		{
			$model->attributes=$_POST['Parameter'];
			if($model->save())
			{
				Yii::app()->user->setFlash('flash',Message::getTranslation(92));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Parameter('search');
		$model->unsetAttributes();
		if(isset($_GET['Parameter']))
			$model->attributes=$_GET['Parameter'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}


	/**
	 * Возвращает модель данных на основе первичного ключа, указанному в переменной GET.
     * Если модель данных не найдена, в HTTP будет сделано  исключение.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['parameter']))
			{
				$this->_model=Parameter::model()->findByPk($_GET['parameter']);
			}
			if($this->_model===null)
				throw new CHttpException(404,Message::getTranslation(94));
		}
		return $this->_model;
	}
}
