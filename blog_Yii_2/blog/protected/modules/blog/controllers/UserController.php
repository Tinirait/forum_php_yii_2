<?php

class UserController extends BlogController
{
	public $layout = '//layouts/blog';
	/**
	 * var CActiveRecord в настоящее время загружены экземпляр модели данных.
	 */
	private $_model;
	/**
	 * return массив фильтров действий
	 */
	public function filters()
	{
		$filters = array(
			'accessControl', // Выполнить контроль доступа для CRUD операций
		);
		
		return array_merge($filters, parent::filters());
	}

	/**
	 * Объявляет действия на основе классов.
	 */
	public function actions()
	{
		return array(
            // CAPTCHA на действие оказывает на изображение CAPTCHA отображается на странице контактов
			'captcha'=>array(
                'class'=>'CaptchaExtendedAction',
                // if needed, modify settings
                'testLimit' => 1,
			),
		);
	}

	/**
    Указывает правила контроля доступа.
     * Этот метод используется фильтром '' AccessControl.
     * Правила контроляreturn доступа к массиву
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('activatePassword', 'passwordRecovery', 'unsubscribe', 'register', 'confirm', 'captcha', 'categoriesByLanguage'),
				'users'=>array('*'),
			),
			array('allow',
				'actions' => array('view'),
				'users'=>array('@'),
			),
			array('allow',
				'roles'=>array('admin'),
			),
			array('allow',
				'actions' => array('update'), 
				'roles'=> array('reader','author','editor','publisher'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}

	/**
     * Отображение конкретной модели.
	 */
	public function actionView()
	{
		$model = $this->loadModel();
		$user = User::model()->findByPk(Yii::app()->user->id);
		$contact=new ContactForm;
		
		if(isset($_POST['ContactForm']))
		{
			$contact->attributes=$_POST['ContactForm'];
			if($contact->validate())
			{
				$model->sendEmail(Parameter::getValue(28) . ': ' . $user->username, $user->email, $contact->subject, $contact->body);
				Yii::app()->user->setFlash('contact',Message::getTranslation(187) . ' ' . $model->username);
				$this->refresh();
			}
		}
		
		$this->render('view', array('model' => $model, 'contact' => $contact));
	}

	
	/**
    Создает новую модель.
	 */
	public function actionCreate()
	{
		$model=new User('insert');
		$model->status = User::STATUS_UNCONFIRMED;
		$model->role = User::ROLE_READER;
		$model->subscribe = Parameter::getValue(30) ? true : false;
		
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->subscribed_categories == null)
				$model->subscribed_categories = array();
			if($model->save())
			{
				Yii::app()->user->setFlash('success',Message::getTranslation(85));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
		}

        $model->password = null;
		$model->repeatPassword = null;
		
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
    Создает новую модель.
	 */
	public function actionRegister()
	{
		$model=new User('register');
		$model->status = User::STATUS_UNCONFIRMED;
		$model->role = User::ROLE_READER;
		$model->subscribe = Parameter::getValue(30) ? true : false;
		
		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->subscribed_categories == null)
				$model->subscribed_categories = array();
			
			if($model->save())
			{
				Yii::app()->user->setFlash('success',Message::getTranslation(105));
				$this->redirect(array('/blog/post/index', 'lang' => Yii::app()->getLanguage()));
			}
		}

	
        $model->password = null;
		$model->repeatPassword = null;
		
		$this->render('register',array(
			'model'=>$model,
		));
	}
	
	public function actionConfirm()
	{
		if(isset($_GET['token']))
		{
			$user = User::model()->find(array('condition' => 'token = :token', 'params' => array(':token' => $_GET['token'])));
			if($user != null)
			{
				$user->scenario = 'confirm';
				$user->status = User::STATUS_ACTIVE;
				$user->save();
			}
			else {
				throw new CHttpException(404, Message::getTranslation(110));
			}
		}
		else
			throw new CHttpException(404, Message::getTranslation(110));
		
		Yii::app()->user->setFlash('success',Message::getTranslation(111));
		$this->redirect(array('/blog/post/index', 'lang' => isset($_GET['lang']) ? $_GET['lang'] : Parameter::getValue(29)));
	}
	
	public function actionUnsubscribe()
	{
		if(isset($_GET['token']))
		{
			$user = User::model()->find(array('condition' => 'token = :token', 'params' => array(':token' => $_GET['token'])));
			if($user != null)
			{
				$user->scenario = 'unsubscribe';
				$user->subscribe = 0;
				$user->save();
			}
			else {
				throw new CHttpException(404, Message::getTranslation(110));
			}
		}
		else
			throw new CHttpException(404, Message::getTranslation(110));
		
		Yii::app()->user->setFlash('success',Message::getTranslation(141));
		$this->redirect(array('/blog/post/index', 'lang' => isset($_GET['lang']) ? $_GET['lang'] : Parameter::getValue(29)));
	}
	
	/**
	 * Обновления конкретной модели.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		// Администраторы не могут обновлять другие админы
		if($model->authAssignment->itemname === 'admin' && Yii::app()->user->id !== $model->id)
			throw new CHttpException(404, Message::getTranslation(94));
		// Читатели, авторы, редакторы и издатель может обновлять только свои собственные профили
		if(Yii::app()->user->checkAccess('reader') || Yii::app()->user->checkAccess('author') || 
			Yii::app()->user->checkAccess('editor') || Yii::app()->user->checkAccess('publisher'))
			if(Yii::app()->user->id !== $model->id)
				throw new CHttpException(404, Message::getTranslation(94));
						
		$model->scenario='update';
		
		if(Yii::app()->user->checkAccess('admin'))
		{
			if($model->status == User::STATUS_BANNED)
			{
				if(!is_null($model->ban_expires))
				{
					$model->ban_expires = ($model->ban_expires * 1000) . '';
					$model->ban_forever = false;
				}
				else
					$model->ban_forever = true;
			}
		}
				
		if(isset($_POST['User']))
		{
			$model->attributes = $_POST['User'];
			$model->username = trim(strtolower($model->username));
			if($model->subscribed_categories == null)
				$model->subscribed_categories = array();
				
			if($model->save())
			{
				Yii::app()->user->setFlash('success',Message::getTranslation(86));
				if(Yii::app()->user->checkAccess('admin'))
					$this->redirect(array('admin', 'lang' => $_POST['lang']));
				else
					$this->redirect(array('/blog/post/index', 'lang' => $_POST['lang']));
			}
		}

        $model->password = null;
		$model->repeatPassword = null;
		
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Удаляет конкретную модель.
	 */
	public function actionDelete()
	{
		// we only allow deletion via POST request
		$model = $this->loadModel();
		$model->delete();
		
		switch($model->onDeleteError)
		{
			case 0:
				Yii::app()->user->setFlash('success',Message::getTranslation(87));
				break;
			case 1:
				Yii::app()->user->setFlash('error',Message::getTranslation(88));
				break;
			case 2:
				Yii::app()->user->setFlash('error', Message::getTranslation(143));
				break;
		}

        // Если AJAX запрос (вызвано удаление с помощью зрения администратора сетки), мы не должны перенаправить браузер
		if(!isset($_GET['ajax']))
		{
			$this->redirect(array('admin', 'lang' => $_GET['lang']));
		}
	}
	
	
	/**
	 * Управляет всеми моделями.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}


	/**
	 * Возвращает модель данных на основе первичного ключа, указанному в переменной GET.
     * Если модель данных не найден, HTTP будет возбуждено исключение.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['user']))
			{
				$this->_model=User::model()->findByPk($_GET['user']);
			}
			if($this->_model===null)
				throw new CHttpException(404,Message::getTranslation(94));
		}
		return $this->_model;
	}
	
	public function actionCategoriesByLanguage()
	{
		$categories = Category::model()->findAll(array('condition' => 'language =\'' . $_POST['languages']. '\' AND category_id IS NULL','order' => 'LOWER(name) ASC'));
		$this->getListData($categories, 0);	
	}
	
	private function getListData($categories, $level)
	{			
		foreach($categories as $category)
		{
			$lines = '';
			for($i = 0; $i < $level; $i++)
				$lines .= '--';
			echo "<option value=\"{$category->id}\">{$lines}{$category->name}</option>";
			$this->getListData($category->categories, $level + 1);
		}		
	}
	
	/**
	 * Восстановление пароля.
	 */
	public function actionPasswordRecovery()
	{
		$model=new PasswordRecovery;
		
		if(isset($_POST['PasswordRecovery']))
		{
			$model->attributes=$_POST['PasswordRecovery'];
			if($model->validate())
			{
				$user = User::model()->find('email=?', array($model->email));
				if(!is_null($user))
				{
					$password = User::generatePassword(10);
					$user->scenario = "passwordrecovery";
					$user->new_password = CPasswordHelper::hashPassword($password);
					if($user->save())
					{
						$user->sendNewPassword($password);
						Yii::app()->user->setFlash('success',Message::getTranslation(184));
						$this->refresh();
					}
					else {
						throw new CHttpException(404, Message::getTranslation(110));
					}
				}
				$model->addError('email', Message::getTranslation(183));
			}
		}
		
		$this->render('password_recovery', array('model' => $model));
	}
	
	public function actionActivatePassword()
	{
		if(isset($_GET['token']))
		{
			$user = User::model()->find(array('condition' => 'token = :token', 'params' => array(':token' => $_GET['token'])));
			if($user != null && $user->new_password != null)
			{
				$user->scenario = 'activatepassword';
				$user->password = $user->repeatPassword = $user->new_password;
				$user->new_password = null;
				$user->save();
			}
			else {
				throw new CHttpException(404, Message::getTranslation(110));
			}
		}
		else
			throw new CHttpException(404, Message::getTranslation(110));
		
		Yii::app()->user->setFlash('success',Message::getTranslation(185));
		$this->redirect(array('/blog/post/index', 'lang' => isset($_GET['lang']) ? $_GET['lang'] : Parameter::getValue(29)));
	}
	
	public function actionSuggestUsers()
	{
		if(isset($_GET['term']) && ($keyword=trim($_GET['term']))!=='')
		{
			$users=User::model()->suggestUsers($keyword, Parameter::getValue(7));
			echo CJSON::encode($users);
		    Yii::app()->end();
		}
	}
}
