<?php

class CategoryController extends BlogController
{
	public $layout = '//layouts/blog';
	/**
	 * @var CActiveRecord текущий загруженный экземпляр модели данных.
	 */
	private $_model;
	/**
	 * @return фильтры действий массив
	 */
	public function filters()
	{
		$filters = array(
			'accessControl', // осуществляет контроль доступа к CRUD операций
            //Create, read, update и delete (CRUD) — четыре основных операции,
            // при помощи которых можно управлять объектами данных
		);
		
		return array_merge($filters, parent::filters());
	}

	/**
	 * Specifies правила контроля доступа.
     * Этот метод используется фильтром '' AccessControl
	 */
	public function accessRules()
	{
		return array(
			array('allow', // позволяют пользователи с правами администратора для доступа к всем действиям
				'roles'=>array('admin'),
			),
			array('deny',  // все пользователи
				'users'=>array('*'),
			),
		);
	}
	
	/**
	 * Создает новую модель.
	 */
	public function actionCreate()
	{
		$model=new Category;
		if(isset($_POST['Category']))
		{
			$model->attributes=$_POST['Category'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success', Message::getTranslation(78));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
		}

		$languages = Language::model()->findAll(array('order' => 'code ASC'));

		$this->render('create',array(
			'language' => $languages[0]->code,
			'model'=>$model,
		));
	}

	/**
	 * Обновления конкретной модели.
	 */
	public function actionUpdate()
	{
                $model=$this->loadModel();
		$model->scenario = 'update';
		if(isset($_POST['Category']))
		{
			$model->attributes=$_POST['Category'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success',Message::getTranslation(79));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Удаляет конкретную модель.
	 */
	public function actionDelete()
	{
		//  только позволит исключить через POST запрос
		if($this->loadModel()->delete()){
			Yii::app()->user->setFlash('success',Message::getTranslation(80));			
		}
		else {
			Yii::app()->user->setFlash('error',Message::getTranslation(81));						
		}

		$this->redirect(array('admin', 'lang' => $_GET['lang']));
	}
	
	
	/**
	 * Управляет всеми моделями.
	 */
	public function actionAdmin()
	{
		$model=new Category('search');
		$model->unsetAttributes();
		if(isset($_GET['Category']))
			$model->attributes=$_GET['Category'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}


	/**
	 * Возвращает модель данных на основе первичного ключа, указанному в переменной GET.
     * Если модель данных не найдена, HTTP будет сделано исключение.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['category']) && isset($_GET['language']) )
			{
				if(Yii::app()->user->isGuest)
					$condition='status='.Category::STATUS_PUBLISHED;
				else
					$condition='';
				$this->_model=Category::model()->findByPk(array('id' => $_GET['category'], 'language' => $_GET['language']), $condition);
			}
			if($this->_model===null)
				throw new CHttpException(404,Message::getTranslation(94));
		}
		return $this->_model;
	}

	public function actionCategoriesByLanguage()
	{
		$categories = Category::model()->findAll(array('condition' => 'language =\'' . $_POST['Category']['language'] . '\' AND category_id IS NULL','order' => 'LOWER(name) ASC'));
		echo "<option value=''>" . Message::getTranslation(95) . "</option>";
		$this->printOptions($categories, 0, $_POST['Category']['id']);
	}
	
	private function printOptions($categories, $level, $id)
	{			
		foreach($categories as $category)
		{
			$lines = '';
			for($i = 0; $i < $level; $i++)
				$lines .= '--';
			echo "<option " . ($category->id == $id ? 'disabled' : '') . " value=\"{$category->id}\">{$lines}{$category->name}</option>";
			$this->printOptions($category->categories, $level + 1, $id);
		}		
	}
}
