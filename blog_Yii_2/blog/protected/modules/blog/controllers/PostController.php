<?php

class PostController extends BlogController
{
	public $layout = '//layouts/blog';

	/**
	 * Объявляем действия на основе классов.
	 */
	public function actions()
	{
		return array(
			// captcha действие оказывает на изображение CAPTCHA отображается на странице контактов
			'captcha'=>array(
                'class'=>'CaptchaExtendedAction',
                // при необходимости, изменяем  настройки
                'testLimit' => 1,
                //'mode'=>CaptchaExtendedAction::MODE_MATH,
			),
		);
	}
		//подключается капча через внешний класс работы с капчёй.
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		$filters = array(
			'accessControl', // осуществляет контроль доступа к CRUD операций
		);
		
		return array_merge($filters, parent::filters());
	}

        /**
         * Это набор правил который говорит  о том какие действия могут запускать пользователи
         * action - дейсвтия
         * users - пользователи (* - любой пользователь)
         * roles - это разделение запуска по ролям пользователей
         * дейсвия передаются без окончания Action
         * если дейсвие запрещено для определенного класса пользователей  появится исключение
         * 
         * @return type
         */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index','view','captcha','search'),
				'users'=>array('*'),
			),
            //Любой пользователь может запускать 'index','view','captcha','search'
			array('allow',
				'roles'=>array('admin'),
                //пользователь с правами администратора, может запускать всё.
			),
			array('allow',
				'actions' => array('create', 'update', 'delete', 'admin', 'categoriesByLanguage', 'suggestTags', 'getMaxId'), 
				'roles'=> array('author', 'editor', 'publisher'),//авторо редактор..может запускать те действия.
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}
    //Остальным пользователям запрещены действия.
// Описываем, что сможет запускать любой пользователь.
        /**
         * Просмотр данных определнного поста переданного в качестве параметра GET
         * Также идет проверка если пользователь имеет право это смотреть через checkAccess
         * отображение через виюшку - view
         */
	public function actionView()
	{
		$post=null;
		
		if(isset($_GET['post']) && isset($_GET['category']) && isset($_GET['lang']))
            //проверяем:если у нас есть id поста,категории, язык
		{
			if(Yii::app()->user->checkAccess('admin') || Yii::app()->user->checkAccess('publisher') || Yii::app()->user->checkAccess('editor'))
				$condition='';
                //проверка на права checkAccess :либо мы админы, либо мы издатели, редакторы,то у нас нет условия $condition='';
			elseif(Yii::app()->user->checkAccess('author'))
				$condition='status='.Post::STATUS_PUBLISHED . ' OR user_id=' . Yii::app()->user->id;
            // Если мы авторы то мы можем открывать только свои статьи.STATUS_PUBLISHED-cтатья опубликована
            //Yii::app()->user->id;-id когда мы залогировались в систему
			elseif(Yii::app()->user->checkAccess('reader') || Yii::app()->user->isGuest)
				$condition='status='.Post::STATUS_PUBLISHED;
            // если у нас читатель, то он читает любые статьи, которые опубликованы.
			
			$post=Post::model()->findByPk(array('id' => $_GET['post'], 'category_id' => $_GET['category'], 'language' => $_GET['lang']), $condition);
			
			if(!is_null($post) && (Yii::app()->user->checkAccess('reader') || Yii::app()->user->isGuest))
			{
				$category = $post->category;
				while(true)
				{
					if($category->status == Category::STATUS_UNPUBLISHED)
					{
						$post = null;
						break;
					}
					if(is_null($category->category_id))
						break;
					$category = $category->category;
				}
			}
		}
		if($post===null)
			throw new CHttpException(404,Message::getTranslation(94));		
	
		
		$comment=new Comment;
		if(Yii::app()->user->isGuest)
			$comment->scenario = "guestcomment";
		if(isset($_POST['ajax']) && $_POST['ajax']==='comment-form')
		{
			echo CActiveForm::validate($comment);
			Yii::app()->end();
		}
		if(isset($_POST['Comment']))
		{
			$comment->attributes=$_POST['Comment'];
			if($post->addComment($comment))
			{
				if($comment->status==Comment::STATUS_PENDING)
					Yii::app()->user->setFlash('commentSubmitted',Message::getTranslation(75));
				$this->refresh();
			}
		}
// Если пост не равен 0, то мы его выводим вместе с комментариями
		$comments = new CActiveDataProvider('Comment',array( //CActiveDataProvider-компонент листает через пагинатор
            //берёт только определённое кол-во
            'criteria'=>array(                   
              	'order'=>'create_time DESC',
				'condition' =>
		             'post_id = ' . $post->id . ' AND category_id = ' . $post->category_id . 
		             ' AND language = "' . $post->language . '" AND status = ' . Comment::STATUS_APPROVED . 
		             ' AND comment_id IS NULL'		
            ),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(33),
			),
		));


		$this->render('view',array(
			'model'=>$post,
			'comment'=>$comment,
			'comments' => $comments,
		));
	} //Вывод на экран

	/**
	 * Creates a new model.
	 */
        /**
         * Создание новой модели
         * По коду создается пустая модель, в нее передаются параметры из массива POST
         * происходит сохранение модели - save(). В сохранении встроена проверка, 
         * правила валидации прописаны в модели. Они там и описаны
         * если сохранилось все хорошо идет открытие виюшки admin
         */
	public function actionCreate()
	{
		$model=new Post;
		$model->meta_keywords = null;
		$model->meta_description = null;
		$model->status = Post::STATUS_DRAFT;
		$model->user_id = Yii::app()->user->id;
		
		if(isset($_POST['Post']))
		{
			$model->attributes=$_POST['Post'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success',Message::getTranslation(72));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
		}

		$languages = Language::model()->findAll(array('order' => 'code ASC'));

		$this->render('create',array(
			'language' => $languages[0]->code,
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
         * Работает схожим образом как и создание только не создается новая модель
         * а находится модель по переданному GET параметру category (это идентификатор категории)
	 */
	public function actionUpdate()
	{
		$model=null;
		
		if(isset($_GET['post']) && isset($_GET['category']) && isset($_GET['language']))
		{
			if(Yii::app()->user->checkAccess('admin') || Yii::app()->user->checkAccess('publisher') || Yii::app()->user->checkAccess('editor'))
				$condition='';
			elseif(Yii::app()->user->checkAccess('author'))
				$condition='user_id=' . Yii::app()->user->id;
			
			$model=Post::model()->findByPk(array('id' => $_GET['post'], 'category_id' => $_GET['category'], 'language' => $_GET['language']), $condition);
		}
		if($model===null)
			throw new CHttpException(404,Message::getTranslation(94));
		
		$model->scenario = 'update';
		if(isset($_POST['Post']))
		{
			$model->attributes=$_POST['Post'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success',Message::getTranslation(73));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

        /**
         * Удаляется модель согласно переданному параметру GET
         * после удаления на той же страницы выводятся всплывающие сообщения. 
         * Они вызываются через конструкцию setFlash
         */
	public function actionDelete()
	{
		$model=null;
		
		if(isset($_GET['post']))
		{
			if(Yii::app()->user->checkAccess('admin')  || Yii::app()->user->checkAccess('publisher') || Yii::app()->user->checkAccess('editor'))
				$condition='';
			elseif(Yii::app()->user->checkAccess('author'))
				$condition='user_id=' . Yii::app()->user->id;
			
			$model=Post::model()->findByPk(array('id' => $_GET['post'], 'category_id' => $_GET['category'], 'language' => $_GET['language']), $condition);
		}
		if($model === null)
			throw new CHttpException(404,Message::getTranslation(94));
		

		if($model->delete())
			Yii::app()->user->setFlash('success',Message::getTranslation(74));
		else
			Yii::app()->user->setFlash('error',Message::getTranslation(144));
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
		{	
			$this->redirect(array('admin', 'lang' => $_GET['lang']));
		}
	}

	/**

         * Выбор и отображение всех моделей постов
         * если передана GET параметр - ID категории то выводятся модели из нее.
         * если передана GET параметр - ID тега то выводятся модели из нее.
         * 
         * 
	 */
	public function actionIndex()
	{
		if(!isset($_GET['lang']))
			throw new CHttpException(404,Message::getTranslation(94));
		
		$categories = '';
		$language = $_GET['lang'];
		
		if(isset($_GET['category']))
		{
			$category = Category::model()->findByPk(array('id' => $_GET['category'], 'language' => $language), array('condition' => 'status = ' . Category::STATUS_PUBLISHED));
			if(!is_null($category))
				$categories = $category->id . Category::getSubcategoriesIdsR($category->categories);		
		}		
		else
			$categories = substr(Category::getSubcategoriesIdsR(Category::model()->findAll(array('condition' => 'language =:language AND category_id IS NULL','order' => 'LOWER(name) ASC', 'params' => array(':language' => $language)))),1);
		
		$params = array();
		$params['language'] = $language;
		if(isset($_GET['year']))
			$params['year'] = $_GET['year'];
		if(isset($_GET['month']))
			$params['month'] = $_GET['month'];	
			
		$criteria=new CDbCriteria(array(
			'condition'=>'status='.Post::STATUS_PUBLISHED . ' AND language=:language AND category_id IN('. $categories .')' . (isset($_GET['year']) ? ' AND DATE_FORMAT(FROM_UNIXTIME(create_time),"%Y") = :year': '') . (isset($_GET['month']) ? ' AND DATE_FORMAT(FROM_UNIXTIME(create_time),"%m") = :month' : ''),
			'order'=>'id DESC',
			'params'=>$params
		));			
			
		if(isset($_GET['tag']))
			$criteria->addSearchCondition('tags', str_replace('-',' ',$_GET['tag']));

		$dataProvider=new CActiveDataProvider('Post', array(
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(4),
			),
			'criteria'=>$criteria,
		));

		if(count($dataProvider->getData()) == 0 && Yii::app()->getLanguage() !== Parameter::getValue(29))
			$this->redirect(array('post/index', 'lang' => Parameter::getValue(29)));
		
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	/**
	 * Manages all models.
         * Управление всеми моделями, по коду создается пустая модель POST, 
         * обнуляются параметры если есть
         * проверяются если переданы параметры для поиска (это фильтрация админской таблицы)
         * если есть эти параметры то они сохраняются в модель
         * и выводится через виюшку admin
         * Виюшка находится в папке modules/blog/views/post
	 */
	public function actionAdmin()
	{
		$model=new Post('search');
		$model->unsetAttributes();
		if(isset($_GET['Post']))
			$model->attributes=$_GET['Post'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 *  Теги, основанных на текущем входе пользователя.
     * Это вызывается через AJAX, когда пользователь вводит данные.
	 */
	public function actionSuggestTags()
	{
		if(isset($_GET['term']) && ($keyword=trim($_GET['term']))!=='')
		{
			$tags=Tag::model()->suggestTags($keyword, Parameter::getValue(7), $_GET['lang']);
			echo CJSON::encode($tags);
		    Yii::app()->end();
		}
	}
	
        /**
         * Выбор категории по языку. Через конструкцию Category::model()->findAll находятся все модели удовлетворяющие 
         * условию выборки, которое передается в качестве параметра. И вызывается приватный метод getListData
         */
	public function actionCategoriesByLanguage()
	{
		$categories = Category::model()->findAll(array('condition' => 'language =\'' . $_POST['Post']['language']. '\' AND category_id IS NULL','order' => 'LOWER(name) ASC'));
		$this->getListData($categories, 0);	
	}

        /**
         * Создает из полученного списка категориев древовидную HTML структуру options
         * Используется для вывода категорий и подкатегорий.
         * Вызывается рекурсивно
         * @param type $categories
         * @param type $level
         */
	private function getListData($categories, $level)
	{			
		foreach($categories as $category)
		{
			$lines = '';
			for($i = 0; $i < $level; $i++)
				$lines .= '--';
			echo "<option value=\"{$category->id}\">{$lines}{$category->name}</option>";
			$this->getListData($category->categories, $level + 1);
		}		
	}
	
        /*
         *  получение максимального ID категории
         *  производится через конструкцию         Post::model()->find - 
         *  которая ищет первые элемент удовлетворяющий условию
         */
        
	public function actionGetMaxId()
	{
		if(isset($_POST['category']) && isset($_POST['language']))
		{
			$post = Post::model()->find(array('select' => 'MAX(id) as maxId', 'condition' => 'category_id = ? AND language = ?', 'params' => array($_POST['category'], $_POST['language'])));
			echo $post->maxId + 1;
		}		
	}
	/**
         * Реализует поиск постов по коду при помощи DAO находятся все категории, 
         * сначала берется строка поиска и экранируется (функция addslashes)
         * потом из них формируется массив моделей категориев
         * затем массив категорий передается в провайдер вместе с трокой поиска  и 
         * формируется провайдер результата с постраничным отображением
         * который передается в виюшку searchResults
         */
	public function actionSearch()
	{
		$match = '';
		
		if(isset($_GET['search']))
			$match = addcslashes($_GET['search'], '%_');
                $catModels = array();
                //$catModels = Category::model()->findAll(array('condition' => 'language =:language AND category_id IS NULL','order' => 'LOWER(name) ASC', 'params' => array(':language' => Yii::app()->getLanguage())));
                // берем данные подключения из конфиг файла
                $connection=Yii::app()->db;
                // запрос
                $sql="SELECT * FROM {{category}} WHERE language= :language AND category_id IS NULL ORDER BY LOWER(name) ASC";
                $command=$connection->createCommand($sql);
                // подключение параметров
                $lang = Yii::app()->getLanguage();
                $command->bindParam(":language", $lang, PDO::PARAM_STR);
                $dataReader=$command->query();
                // проходим по полученным результатам и создаем пустые модели Category
                // заполняем их свойствами и передаем дальше
                foreach($dataReader as $row) {
                    $catModel = new Category;
                    $catModel->id = $row['id'];
                    $catModel->name = $row['name'];
                    $catModel->language = $row['language'];
                    $catModel->category_id = $row['category_id'];
                    $catModel->status = $row['status'];
                    $catModels[] = $catModel;
                }
                
		$categories = substr(Category::getSubcategoriesIdsR($catModels),1);
		$results = new CActiveDataProvider('Post',array(
            'criteria'=>array(                   
              	'order'=>'id',
				'condition' => '(title LIKE ? OR content LIKE ?) AND language = ? AND status=' . Post::STATUS_PUBLISHED . ' AND category_id IN(' . $categories . ')',
		        'params' => array("%$match%","%$match%",Yii::app()->getLanguage())	
            ),
			'pagination'=>array(
				'pageSize'=>Parameter::getValue(34),
			),
		));
		

		$this->render('searchResults', array('results' => $results));	
	}
}
