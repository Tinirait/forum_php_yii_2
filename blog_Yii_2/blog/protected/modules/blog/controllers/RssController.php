<?php

/* 
 * RSS controller
 */

class RssController extends BlogController
{
    function actionIndex()
    {
        $rss = new Rss();
        $rss->init();
    }
}