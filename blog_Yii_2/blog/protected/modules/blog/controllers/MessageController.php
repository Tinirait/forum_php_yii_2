<?php

class MessageController extends BlogController
{
	public $layout = '//layouts/blog';
	/**
	 * @var CActiveRecord текущий загруженный экземпляр модели данных.
	 */
	private $_model;
	/**
	 * @return фильтры действий массив
	 */
	public function filters()
	{
		$filters = array(
			'accessControl', //осуществляется контроль доступа к CRUD операциям
		);
		
		return array_merge($filters, parent::filters());
	}

	/**
	 * Определяет правила управления доступом.
     * Этот метод используется фильтром '' AccessControl.
     * return Правила контроля доступа к массиву
	 */
	public function accessRules()
	{
		return array(
			array('allow', // позволяют пользователи с правами администратора для доступа к всем действиям
				'roles'=>array('admin'),
			),
			array('deny',  // все пользователи
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 */
	public function actionCreate()
	{
		$model=new Message;
		if(isset($_POST['Message']))
		{
			$model->attributes=$_POST['Message'];
			if($model->save())
			{
				Yii::app()->user->setFlash('flash',Message::getTranslation(89));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Обновления конкретной модели.
	 */
	public function actionUpdate()
	{
		$model=$this->loadModel();
		
		if(isset($_POST['Message']))
		{
			$model->attributes=$_POST['Message'];

			if($model->save())
			{
				Yii::app()->user->setFlash('flash',Message::getTranslation(90));
				$this->redirect(array('admin', 'lang' => $_POST['lang']));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 *Удаляет конкретную модель.
	 */
	public function actionDelete()
	{
		//  только позволит исключить через POST запрос
		$this->loadModel()->delete();

		// если AJAX запрос (вызвано удаление с помощью  администратора сетки), мы не должны перенаправить браузер
		if(!isset($_GET['ajax']))
		{
			Yii::app()->user->setFlash('flash',Message::getTranslation(91));
			$this->redirect(array('admin', 'lang' => $_GET['lang']));
		}
	}
	
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Message('search');
		$model->unsetAttributes();
		if(isset($_GET['Message']))
			$model->attributes=$_GET['Message'];
		$this->render('admin',array(
			'model'=>$model,
		));
	}


	/**
	 * Возвращает модель данных на основе первичного ключа, указанному в переменной GET.
     * Если модель данных не найдена,в HTTP будет  исключение.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['message']))
			{
				$this->_model=Message::model()->findByPk(array('id' => $_GET['message'], 'language' => $_GET['language']));
			}
			if($this->_model===null)
				throw new CHttpException(404,Message::getTranslation(94));
		}
		return $this->_model;
	}
}
