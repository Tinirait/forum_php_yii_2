
<?php

class BlogModule extends CWebModule{
	/*
	 * Добавляются параметры пользователя здесь. Сдесь можно получить доступ к их помощи Yii::app()->controller->module->myBlogParameter
	 */
	public $myParameter;

    public $defaultController ="Post"; //или название контроллера
    public function init()
    {
            // этот метод вызывается, когда модуль создается
            // Можно  разместить код здесь, чтобы настроить модуль или приложение
            // импортировать модели и компоненты уровня модуля
            $this->setImport(array(
                    'blog.models.*',
                    'blog.components.*',
            ));
    }
}
//В этом файле подключаются модели и компоненты.
//Компоненты-это виджеты, боковые рамки для информации