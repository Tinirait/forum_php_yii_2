<ul id='portletmenu'>
	<?php if(Yii::app()->user->checkAccess('admin')): ?>
		<li><?php echo CHtml::link(Message::getTranslation(18),array('post/admin', 'lang' => Yii::app()->getLanguage())); ?></li>
		<li><?php echo CHtml::link(Message::getTranslation(19),array('comment/admin', 'lang' => Yii::app()->getLanguage())); ?></li>
		<li><?php echo CHtml::link(Message::getTranslation(20),array('category/admin', 'lang' => Yii::app()->getLanguage())); ?></li>
		<!--<li><?php //echo CHtml::link(Message::getTranslation(21),array('language/admin', 'lang' => Yii::app()->getLanguage())); ?></li>-->
		<li><?php echo CHtml::link(Message::getTranslation(22),array('user/admin', 'lang' => Yii::app()->getLanguage())); ?></li>
		<!--<li><?php //echo CHtml::link(Message::getTranslation(25),array('message/admin', 'lang' => Yii::app()->getLanguage())); ?></li>-->
		<!--<li><?php //echo CHtml::link(Message::getTranslation(158),array('massmail/admin', 'lang' => Yii::app()->getLanguage())); ?></li>-->
		<!--<li><?php //echo CHtml::link(Message::getTranslation(23),array('parameter/admin', 'lang' => Yii::app()->getLanguage())); ?></li>-->
	<?php elseif(Yii::app()->user->checkAccess('publisher')): ?>
		<li><?php echo CHtml::link(Message::getTranslation(127),array('user/update', 'lang' => Yii::app()->getLanguage(), 'user' => Yii::app()->user->id)); ?></li>	
		<li><?php echo CHtml::link(Message::getTranslation(18),array('post/admin', 'lang' => Yii::app()->getLanguage())); ?></li>
		<li><?php echo CHtml::link(Message::getTranslation(19),array('comment/admin', 'lang' => Yii::app()->getLanguage())); ?></li>	
	<?php elseif(Yii::app()->user->checkAccess('editor')): ?>
		<li><?php echo CHtml::link(Message::getTranslation(127),array('user/update', 'lang' => Yii::app()->getLanguage(), 'user' => Yii::app()->user->id)); ?></li>
		<li><?php echo CHtml::link(Message::getTranslation(18),array('post/admin', 'lang' => Yii::app()->getLanguage())); ?></li>
		<li><?php echo CHtml::link(Message::getTranslation(19),array('comment/admin', 'lang' => Yii::app()->getLanguage())); ?></li>	
	<?php elseif(Yii::app()->user->checkAccess('author')): ?>
		<li><?php echo CHtml::link(Message::getTranslation(127),array('user/update', 'lang' => Yii::app()->getLanguage(), 'user' => Yii::app()->user->id)); ?></li>		
		<li><?php echo CHtml::link(Message::getTranslation(18),array('post/admin', 'lang' => Yii::app()->getLanguage())); ?></li>
		<li><?php echo CHtml::link(Message::getTranslation(19),array('comment/admin', 'lang' => Yii::app()->getLanguage())); ?></li>	
	<?php elseif(Yii::app()->user->checkAccess('reader')): ?>
		<li><?php echo CHtml::link(Message::getTranslation(127),array('user/update', 'lang' => Yii::app()->getLanguage(), 'user' => Yii::app()->user->id)); ?></li>		
		<li><?php echo CHtml::link(Message::getTranslation(19),array('comment/admin', 'lang' => Yii::app()->getLanguage())); ?></li>	
	<?php endif; ?>
	<li><?php echo CHtml::link(Message::getTranslation(24),array('site/logout', 'lang' => Yii::app()->getLanguage())); ?></li>
</ul>