<?='<?xml version="1.0" encoding="UTF-8"?>'; ?>
<rss version="2.0">    
    <channel>
        <title><?=$this->config['name']?></title>
        <link><?=$this->config['url']?></link>
        <description><?=$this->config['desc']?></description>
        <copyright>Copyright, <?php echo date("Y"); ?></copyright>';
        <?php foreach ($arr as $key => $val): ?>
        <item>
                 <title><?=addslashes($val->title)?></title> 
                 <link><?=$this->config['url'].$val->url.'/post/'?></link> 
                 <description><?=htmlspecialchars(mb_substr(strip_tags($val->content), 0, 200))."...";?></description> 
                 <pubDate><?php if (isset($val->create_time)) echo date("Y-m-d", $val->create_time);?></pubDate>
        </item>
        <?php endforeach; ?>
        
    </channel>
</rss>