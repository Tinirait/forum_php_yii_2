<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');	
?>
<div class="form" style="margin-bottom:20px;">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
		<div>
			<?php echo $form->labelEx(Yii::app()->getController()->loginForm,'username'); ?>
			<?php echo $form->textField(Yii::app()->getController()->loginForm,'username',array('placeholder' => 'логин')); ?>
			<?php echo $form->error(Yii::app()->getController()->loginForm,'username'); ?>
		</div>
		<div>
			<?php echo $form->labelEx(Yii::app()->getController()->loginForm,'password'); ?>
			<?php echo $form->passwordField(Yii::app()->getController()->loginForm,'password',array('placeholder' => 'пароль')); ?>
			<?php echo $form->error(Yii::app()->getController()->loginForm,'password'); ?>
		</div>
		<div class="row rememberMe">
			<?php echo CHtml::submitButton('Логин'); ?>
			<?php echo $form->checkBox(Yii::app()->getController()->loginForm,'rememberMe'); ?>
			<?php echo $form->label(Yii::app()->getController()->loginForm,'rememberMe'); ?>
			<?php echo $form->error(Yii::app()->getController()->loginForm,'rememberMe'); ?>
		</div>
		<div class="row buttons">
			<?php
				echo CHtml::link(Message::getTranslation(106), array('/blog/user/register', 'lang' => Yii::app()->getLanguage())) . '&nbsp;&nbsp;&nbsp;';
				//echo CHtml::link(Message::getTranslation(168), array('/blog/user/passwordrecovery', 'lang' => Yii::app()->getLanguage()));
			?>
		</div>
		
		<?php $this->endWidget(); ?>
</div><!-- form -->	