<?php
	$script = '
		$(function(){
			// Attach the dynatree widget to an existing <div id="tree"> element
			// and pass the tree options as an argument to the dynatree() function:
			$("#tree2").dynatree({
		//          autoCollapse: true,
				minExpandLevel: 1,
		//          persist: true,
				onPostInit: function(isReloading, isError) {
					this.reactivate();
				},
				onActivate: function(node) {
					// Use <a> href and target attributes to load the content:
					if( node.data.href ){
						// Open target
						window.open(node.data.href, node.data.target);
						// or open target in iframe
		//                $("[name=contentFrame]").attr("src", node.data.href);
					}
				}
			});
		});		
	';
	$baseUrl = Yii::app()->baseUrl; 
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl.'/js/dynatree/jquery-ui.custom.js',CClientScript::POS_END);
	$cs->registerScriptFile($baseUrl.'/js/dynatree/jquery.cookie.js',CClientScript::POS_END);
	$cs->registerScriptFile($baseUrl.'/js/dynatree/jquery.dynatree.js',CClientScript::POS_END);
	$cs->registerCssFile($baseUrl.'/js/dynatree/ui.dynatree.css');
	$cs->registerScript('scripttree2', $script);
	
		
	function printArchives($posts)
	{
		$printstart = true;
		
		echo "<ul>";
		foreach($posts as $key => $post)
		{
			if($printstart){
				echo "<li " . (Parameter::getValue(15) ? "class='expanded'" : "") . ">" . CHtml::link($post->postyear, array('/blog/post/index', 'year' => $post->postyear, 'lang' => Yii::app()->getLanguage()), array('target' => '_parent', 'rel' => Parameter::getValue(32) ? 'nofollow' : null));
				echo "<ul>";
				$printstart = false;
			}
			echo "<li class='expanded'>" . CHtml::link(Message::getTranslation(114 + $post->postmonth) . " ({$post->postspermonth})" , array('/blog/post/index', 'year' => $post->postyear, 'month' => $post->postmonth, 'lang' => Yii::app()->getLanguage()), array('target' => '_parent', 'rel' => Parameter::getValue(32) ? 'nofollow' : null));
			
			if(!isset($posts[$key + 1]) || $post->postyear != $posts[$key + 1]->postyear){
				echo "</ul>";
				$printstart = true;
			}
		}
		echo "</ul>";	
	}
?>

<div id="tree2">
	<?php
		printArchives($posts);
	?>
</div>