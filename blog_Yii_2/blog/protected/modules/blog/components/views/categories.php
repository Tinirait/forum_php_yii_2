<?php
	$script = '
		$(function(){
			// Attach the dynatree widget to an existing <div id="tree"> element
			// and pass the tree options as an argument to the dynatree() function:
			$("#tree").dynatree({
		//          autoCollapse: true,
				minExpandLevel: 1,
		//          persist: true,
				onPostInit: function(isReloading, isError) {
					this.reactivate();
				},
				onActivate: function(node) {
					// Use <a> href and target attributes to load the content:
					if( node.data.href ){
						// Open target
						window.open(node.data.href, node.data.target);
						// or open target in iframe
		//                $("[name=contentFrame]").attr("src", node.data.href);
					}
				}
			});
		});		
	';
	$baseUrl = Yii::app()->baseUrl; 
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl.'/js/dynatree/jquery-ui.custom.js',CClientScript::POS_END);
	$cs->registerScriptFile($baseUrl.'/js/dynatree/jquery.cookie.js',CClientScript::POS_END);
	$cs->registerScriptFile($baseUrl.'/js/dynatree/jquery.dynatree.js',CClientScript::POS_END);
	$cs->registerCssFile($baseUrl.'/js/dynatree/ui.dynatree.css');
	$cs->registerScript('scripttree', $script);
	
		
	function printCategories($categories)
	{
		echo "<ul>";
		foreach($categories as $category)
		{
			if($category->status == Category::STATUS_PUBLISHED)
			{
				echo "<li " . (Parameter::getValue(14) ? "class='expanded'" : "") . ">" . CHtml::link($category->name, array('/blog/post/index', 'category' => $category->id, 'lang' => Yii::app()->getLanguage()), array('target' => '_parent', 'rel' => Parameter::getValue(32) ? 'nofollow' : null));
				if(count($category->categories) > 0)
				{
					printCategories($category->categories);
				}
			}
		}
		echo "</ul>";	
	}
?>

<div id="tree">
	<?php
		printCategories($categories);
	?>
</div>