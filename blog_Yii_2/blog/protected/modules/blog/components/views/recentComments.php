<?php
	$jslink ="	
			$('.jslink').click(function(){
				window.open($(this).attr('title'), '_blank');
			});";
	$cs = Yii::app()->getClientScript();
	$cs->registerScript('jslink', $jslink);
?>

<ul>
	<?php foreach($this->getRecentComments() as $comment): ?>
	<li><?php echo $comment->authorLink; ?> -
		<?php echo CHtml::link(CHtml::encode($comment->post->title), $comment->getUrl(), array('rel' => Parameter::getValue(32) ? 'nofollow' : null)); ?>
	</li>
	<?php endforeach; ?>
</ul>