<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'search-form',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
		<?= CHtml::textField('search','', array('style'=>'width:120px;')); ?>
		<?= CHtml::submitButton(Message::getTranslation(189), array('style'=>'width:100px')); ?>

<?php $this->endWidget(); ?>
