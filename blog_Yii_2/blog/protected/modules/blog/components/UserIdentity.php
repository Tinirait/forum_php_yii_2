<?php

/**
 * UserIdentity представляет данные, необходимые для идентификации пользователя.
  * Она содержит метод аутентификации, который проверяет, если при условии
  * Данные могут личность пользователя.
 */
class UserIdentity extends CUserIdentity
{
	const ERROR_USER_BANNED=3;
	const ERROR_USER_UNCONFIRMED=4;
	private $_id;

	/**
	 * Аутентифицирует пользователя.
     *return Булево удастся ли аутентификация.
	 */
	public function authenticate()
	{
		$user=User::model()->find('LOWER(username)=?',array(strtolower($this->username)));
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!CPasswordHelper::verifyPassword($this->password, $user->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$user->scenario = "login";
			if($user->status == User::STATUS_BANNED)
			{
				if(!is_null($user->ban_expires) && $user->ban_expires < time())
				{		
					$user->status = User::STATUS_ACTIVE;
					$user->ban_expires = null;
					$user->ban_reason = null;
					$user->last_visit = time();
					$user->save();
					$this->_id=$user->id;
					$this->username=$user->username;
					$this->errorCode=self::ERROR_NONE;				
				}
				else {
					$this->errorCode=self::ERROR_USER_BANNED;
				}
			}
			elseif($user->status == User::STATUS_UNCONFIRMED)
			{
				$this->errorCode=self::ERROR_USER_UNCONFIRMED;
			}
			else 
			{
				$user->last_visit = time();
				$user->save();
				$this->_id=$user->id;
				$this->username=$user->username;
				$this->errorCode=self::ERROR_NONE;
			}

		}
		return $this->errorCode==self::ERROR_NONE;
	}

	/**
	 * @return integer Идентификатор записи пользователя
	 */
	public function getId()
	{
		return $this->_id;
	}
}