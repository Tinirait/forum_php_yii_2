<?php

Yii::import('zii.widgets.CPortlet');

class Search extends CPortlet
{
	protected function renderContent()
	{		
		$this->render('search');
	}
}