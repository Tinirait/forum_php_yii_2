<?php

Yii::import('zii.widgets.CPortlet');

class Login extends CPortlet
{		
	protected function renderContent()
	{	
		$this->render('login');
	}
}