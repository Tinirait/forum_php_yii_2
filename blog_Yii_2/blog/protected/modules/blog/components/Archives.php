<?php

Yii::import('zii.widgets.CPortlet');

class Archives extends CPortlet
{
	public $title='Archives';

	protected function renderContent()
	{
		$categories = substr(Category::getSubcategoriesIdsR(Category::model()->findAll(array('condition' => 'language =:language AND category_id IS NULL','order' => 'LOWER(name) ASC', 'params' => array(':language' => Yii::app()->getLanguage())))),1);
		$posts = Post::model()->findAll(array(
			'select' => "DATE_FORMAT(FROM_UNIXTIME(create_time), '%Y') as postyear, DATE_FORMAT(FROM_UNIXTIME(create_time), '%m') as postmonth, count(*) as postspermonth",
			'group' => "postyear, postmonth", 
			'condition' => "language ='" . Yii::app()->getLanguage() . "' AND status =" . Post::STATUS_PUBLISHED . " AND category_id IN(" . $categories . ")",
			'order' => "postyear DESC, postmonth DESC"));
		$this->render('archives', array('posts' => $posts));
	}
}