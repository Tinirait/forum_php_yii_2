<?php

class BlogController extends Controller
{		
	public function init()
	{
		parent::init();
	}
	
	public function beforeAction($action)
	{
		parent::beforeAction($action);
		
		// Флаги будут отображаться только если блог имеет сообщения...............
		if(isset($_GET['category']) && isset($_GET['post']))
		{
			$this->posts = Post::model()->with('category')->findAll(array(
			'condition' => 't.category_id = ? AND t.id = ? AND t.status =' . Post::STATUS_PUBLISHED . ' AND category.status ='. Category::STATUS_PUBLISHED,
			'order' => 't.language ASC',
			'params' => array($_GET['category'], $_GET['post'])));
		}
        //Получение  постов
		elseif(Yii::app()->controller->id === 'post' && $action->id == 'index')
		{
			$this->postCount = Post::model()->with('category')->findAll(array(
			'select' => 't.language, count(*) as postCount',
			'group' => 't.language',
			'condition' => 't.status =' . Post::STATUS_PUBLISHED . ' AND category.status ='. Category::STATUS_PUBLISHED,
			'having' => 'postCount > 0',
			'order' => 't.language ASC'));
		}
		//Получение кол-во постов
		// Блог: Проверка, будет ли пользователю будет запрещено или не...
		if(!Yii::app()->user->isGuest && User::model()->find('id=?', array(Yii::app()->user->id))->status == User::STATUS_BANNED)
		{
			$username = Yii::app()->user->name;
			Yii::app()->user->logout();
			$this->redirect(array('/blog/site/banned', 'lang' => Yii::app()->getLanguage(), 'username' => $username));
		}
//Определяет, если пользователь не гость, его имя берёт.
		$this->loginForm=new LoginForm;
		if(isset($_POST['LoginForm']))
		{
			$this->loginForm->attributes=$_POST['LoginForm'];
			if($this->loginForm->validate() && $this->loginForm->login())
			{
				$this->redirect(array('/blog/post/index', 'lang' => Yii::app()->getLanguage()));
			}
			else
			{
				$user = User::model()->find('username=?', array($_POST['LoginForm']['username']));
				if(!is_null($user) && CPasswordHelper::verifyPassword($this->loginForm->password, $user->password))
				{
					if($user->status == User::STATUS_BANNED)
						$this->redirect(array('/blog/site/banned', 'lang' => Yii::app()->getLanguage(), 'username' => $user->username));
				}
			}
		}
        //Если была овтаризация он берёт пользователя или делает редирект на главную страницу /blog/post/index'
        // А если пользователь в бане делает redirect на страницу бана.
		
		if(isset($_POST['search']))
			$this->redirect(array('/blog/post/search', 'search' => $_POST['search'], 'lang' => Yii::app()->getLanguage()));	
		
		return true;
        //поиск
	}
}
	//базовый контроллер, от которого наследуются другие контроллеры/мы описываем операцию выбора данных
// для того , чтобы не описывать в других контроллерах это постоянно,мы наследуем его в др.контроллерах.