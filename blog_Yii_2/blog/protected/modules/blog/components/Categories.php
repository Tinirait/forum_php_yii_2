<?php

Yii::import('zii.widgets.CPortlet');

class Categories extends CPortlet
{
	public $title='Categories';

	protected function renderContent()
	{
		$categories = Category::model()->findAll(array('condition' => 'language = \'' . Yii::app()->getLanguage() . '\' AND category_id IS NULL AND status=' . Category::STATUS_PUBLISHED, 'order' => 'LOWER(name) ASC'));
		$this->render('categories', array('categories' => $categories));
	}
}