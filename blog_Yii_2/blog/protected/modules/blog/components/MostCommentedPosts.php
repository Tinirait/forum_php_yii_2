<?php

Yii::import('zii.widgets.CPortlet');

class MostCommentedPosts extends CPortlet
{
	public $mostCommentedPostsCount=10;

	protected function renderContent()
	{
		$categories = substr(Category::getSubcategoriesIdsR(Category::model()->findAll(array('condition' => 'language =:language AND category_id IS NULL','order' => 'LOWER(name) ASC', 'params' => array(':language' => Yii::app()->getLanguage())))),1);
		$comments = Comment::model()->with('post')->findAll(array(
			'select' => 'post_id, count(*) as postCount',
			'group' => 'post_id', 
			'condition' => 't.language =\'' . Yii::app()->getLanguage() . '\' AND post.status=' . Post::STATUS_PUBLISHED . ' AND post.category_id IN(' . $categories . ')',
			'limit' => $this->mostCommentedPostsCount,
			'order' => 'postCount DESC'));
		$this->render('mostCommentedPosts', array('comments' => $comments));
	}
}