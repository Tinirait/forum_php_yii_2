<?php


    Class Rss extends CWidget {
        
        var $config = array();
        
        function init()
        {
            if (empty($this->config))
            $this->config = array(
                    'url'  => 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']."/",
                    'name' => 'Мой блог',
                    'desc' => '');
            $this->render('rss_index',
                array(
                    'arr' => Post::model()->findAll('status = 2 ORDER BY create_time DESC')
                ));
        }
    }