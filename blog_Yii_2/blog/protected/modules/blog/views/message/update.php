<?php
$this->pageTitle = Message::getTranslation(56);
$this->breadcrumbs=array(
	Message::getTranslation(56),
);
?>

<h1><?= Message::getTranslation(56) ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>