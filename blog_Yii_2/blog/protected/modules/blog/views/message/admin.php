<?php
$this->pageTitle = Message::getTranslation(68);
$this->breadcrumbs=array(
	Message::getTranslation(68),
);
$style ='	@media 
	all and (max-width: 950px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
		td:nth-of-type(1):before { content: "' . Message::getTranslation(27) . '"; }
		td:nth-of-type(2):before { content: "' . Message::getTranslation(35) . '"; }
		td:nth-of-type(3):before { content: "' . Message::getTranslation(46) . '"; }
		td:nth-of-type(4):before { content: "' . Message::getTranslation(139) . '"; }
	}';
Yii::app()->getClientScript()->registerCss('style', $style);
?>
<?php if(Yii::app()->user->hasFlash('flash')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('flash'); ?>
</div>
<?php endif; ?>
<h1><?= Message::getTranslation(68) ?></h1>
<div style="text-align:center;margin-bottom:20px;">
<?php echo CHtml::link(Message::getTranslation(50),array('message/create', 'lang' => Yii::app()->getLanguage()), array('class' => 'btncreate')); ?>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/gridview.css'),
	'cssFile' => Yii::app()->baseUrl . '/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'htmlOptions' => array('class' => 'grid-view rounded'),
	'ajaxUpdate'=>false,
	'columns'=>array(
		array(
			'name'=>'id',
			'type'=>'raw',
			'value'=>'$data->id',
    		'htmlOptions'=>array('style'=>'width:40px'),
		),
		/*array(
			'name'=>'language',
			'value'=>'$data->lang->name',
			'filter'=>Language::getLanguagesOptions(),
    		'htmlOptions'=>array('style'=>'width:100px'),			
		),*/
		array(
			'name'=>'translation',
			'type'=>'raw',
			'value'=>'$data->translation',
		),
        array(
            'header' => Message::getTranslation(49),
            'class' => 'CButtonColumn',
            'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}',
            'viewButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-view.png',
            'updateButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-update.png',
            'deleteButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-delete.png',
	        'buttons'=>array
			(
		       'update' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/message/update", array("message"=>$data->id, "language"=>$data->language, "lang" => Yii::app()->getLanguage()))'
		        ),
		       'delete' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/message/delete", array("message"=>$data->id, "language"=>$data->language, "lang" => Yii::app()->getLanguage()))'
		        ),
			),
         ),
	),
)); ?>
