<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>

	<p class="note"><?= Message::getTranslation(8) ?></p>

	<?php echo CHtml::errorSummary($model); ?>

	<!--<div class="row">
		<?php //echo $form->labelEx($model,'id'); ?>
		<?php //echo $form->textField($model,'id',array('style'=>'width:100%')); ?>
		<?php //echo $form->error($model,'id'); ?>
	</div>-->
	
	<!--<div class="row">
		<?php //echo $form->labelEx($model,'language'); ?>
		<?php //echo $form->dropDownList($model,'language',Language::getLanguagesOptions()); ?>
		<?php //echo $form->error($model,'language'); ?>
	</div>-->
	
	<div class="row">
		<?php echo $form->labelEx($model,'translation'); ?>
		<?php echo $form->textField($model,'translation',array('style'=>'width:100%')); ?>
		<?php echo $form->error($model,'translation'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Message::getTranslation(50) : Message::getTranslation(70), array('class' => 'bigbutton')); ?>
	</div>
	<?= CHtml::hiddenField('lang', Yii::app()->getLanguage()) ?>

<?php $this->endWidget(); ?>

</div><!-- form -->