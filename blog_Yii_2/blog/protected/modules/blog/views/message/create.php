<?php
$this->pageTitle = Message::getTranslation(62);
$this->breadcrumbs=array(
	Message::getTranslation(62),
);
?>
<h1><?= Message::getTranslation(62) ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>