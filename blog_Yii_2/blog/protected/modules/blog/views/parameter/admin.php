<?php
$this->pageTitle = Message::getTranslation(69);
$this->breadcrumbs=array(
	Message::getTranslation(69),
);
$style ='	@media 
	all and (max-width: 950px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
		td:nth-of-type(1):before { content: "' . Message::getTranslation(27) . '"; }
		td:nth-of-type(2):before { content: "' . Message::getTranslation(47) . '"; }
		td:nth-of-type(3):before { content: "' . Message::getTranslation(48) . '"; }
		td:nth-of-type(4):before { content: "' . Message::getTranslation(139) . '"; }
	}';
Yii::app()->getClientScript()->registerCss('style', $style);
?>
<?php if(Yii::app()->user->hasFlash('flash')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('flash'); ?>
</div>
<?php endif; ?>
<h1><?= Message::getTranslation(69) ?></h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/gridview.css'),
	'cssFile' => Yii::app()->baseUrl . '/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'htmlOptions' => array('class' => 'grid-view rounded'),
	'filterPosition'=>'',
	'columns'=>array(
		array(
			'name'=>'id',
			'type'=>'raw',
			'value'=>'$data->id',
    		'htmlOptions'=>array('style'=>'width:40px'),			
		),
		array(
			'name'=>'description',
			'type'=>'raw',
			'value'=>'$data->description'
		),
		array(
			'name'=>'value',
			'type'=>'raw',
			'value'=>'$data->value',
		),
        array(
            'header' => Message::getTranslation(49),
            'class' => 'CButtonColumn',
            'template' => '{update}',
            'viewButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-view.png',
            'updateButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-update.png',
            'deleteButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-delete.png',
            'buttons'=>array
    		(
		       'update' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/parameter/update", array("parameter"=>$data->id, "lang" => Yii::app()->getLanguage()))'
		        ),
 			),
         ),
	),
)); ?>
