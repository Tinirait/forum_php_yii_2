<?php
$this->pageTitle = Message::getTranslation(57);
$this->breadcrumbs=array(
	Message::getTranslation(57),
);
?>

<h1><?= Message::getTranslation(57) ?> <i><?php echo CHtml::encode($model->description); ?></i></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>