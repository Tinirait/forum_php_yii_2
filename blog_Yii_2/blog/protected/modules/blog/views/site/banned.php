<?php
	$this->pageTitle = Message::getTranslation(113);
?>
<center>
<h1><?= Message::getTranslation(113) ?></h1>
<h2><?= Message::getTranslation(177) ?>: <?= $model->ban_reason; ?></h2>
<h2><?= Message::getTranslation(178) ?>: <?php echo is_null($model->ban_expires) ? Message::getTranslation(180)  : date('j ',$model->ban_expires) . Message::getTranslation(114 + date('n',$model->ban_expires)) . date(', Y',$model->ban_expires); ?></h2>
<?= CHtml::image(Yii::app()->baseUrl . '/images/banned.jpg', 'banned', array('style' => 'max-width:100%')) ?>
</center>