<?php
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<h1>Error <?php echo $code; ?></h1>

<div class="error">
<h2><?php echo CHtml::encode($message); ?></h2>
</div>