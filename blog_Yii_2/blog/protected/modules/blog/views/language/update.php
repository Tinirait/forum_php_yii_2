<?php
$this->pageTitle = Message::getTranslation(54);
$this->breadcrumbs=array(
	Message::getTranslation(54),
);
?>

<h1><?= Message::getTranslation(54) ?> <i><?php echo CHtml::encode($model->name); ?></i></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>