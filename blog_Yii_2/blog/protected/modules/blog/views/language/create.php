<?php
$this->pageTitle = Message::getTranslation(60);
$this->breadcrumbs=array(
	Message::getTranslation(60),
);
?>
<h1><?= Message::getTranslation(60) ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>