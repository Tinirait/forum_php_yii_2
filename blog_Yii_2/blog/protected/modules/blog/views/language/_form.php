<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
?>	
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
		'htmlOptions' => array('enctype' => 'multipart/form-data')
	)); ?>

	<p class="note"><?= Message::getTranslation(8) ?></p>

	<?php echo CHtml::errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'code'); ?>
		<?php echo $form->textField($model,'code',array('style'=>'width:100%','maxlength'=>2)); ?>
		<?php echo $form->error($model,'code'); ?>
		<?php echo CHtml::link(Message::getTranslation(128), 'http://msdn.microsoft.com/en-us/library/ms533052%28v=vs.85%29.aspx', array('target' => '_blank')); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('style'=>'width:100%','maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo $form->fileField($model,'image'); ?>
		<?php echo $form->error($model,'image'); ?>
		<?php 
			if(!$model->isNewRecord)
				echo CHtml::image(Yii::app()->baseUrl . '/' . Language::IMAGE_DIRECTORY . $model->code . '.gif');
		 ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Message::getTranslation(50) : Message::getTranslation(70), array('class' => 'bigbutton')); ?>
	</div>
	<?= CHtml::hiddenField('lang', Yii::app()->getLanguage()) ?>

<?php $this->endWidget(); ?>

</div><!-- form -->