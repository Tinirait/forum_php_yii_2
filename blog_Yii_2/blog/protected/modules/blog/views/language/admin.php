<?php
$this->pageTitle = Message::getTranslation(66);
$this->breadcrumbs=array(
	Message::getTranslation(66),
);
$style ='	@media 
	all and (max-width: 950px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
		td:nth-of-type(1):before { content: "' . Message::getTranslation(41) . '"; }
		td:nth-of-type(2):before { content: "' . Message::getTranslation(10) . '"; }
		td:nth-of-type(3):before { content: "' . Message::getTranslation(42) . '"; }
		td:nth-of-type(4):before { content: "' . Message::getTranslation(139) . '"; }
	}';
Yii::app()->getClientScript()->registerCss('style', $style);
?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php elseif(Yii::app()->user->hasFlash('error')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>
<h1><?= Message::getTranslation(66) ?></h1>
<div style="text-align:center;margin-bottom:20px;">
<?php echo CHtml::link(Message::getTranslation(50),array('language/create', 'lang' => Yii::app()->getLanguage()), array('class' => 'btncreate')); ?>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/gridview.css'),
	'cssFile' => Yii::app()->baseUrl . '/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'htmlOptions' => array('class' => 'grid-view rounded'),
	'filterPosition'=>'',
	'ajaxUpdate'=>false,
	'columns'=>array(
		array(
			'name'=>'code',
			'type'=>'raw',
			'value'=>'$data->code',
		),
		array(
			'name'=>'name',
			'type'=>'raw',
			'value'=>'$data->name',
		),
		array(
			'name'=>'image',
			'type'=>'raw',
			'value'=>'CHtml::image(Yii::app()->baseUrl . "/" . Language::IMAGE_DIRECTORY . $data->code . ".gif")',
		),
        array(
            'header' => Message::getTranslation(49),
            'class' => 'CButtonColumn',
            'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}',
            'viewButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-view.png',
            'updateButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-update.png',
            'deleteButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-delete.png',
	        'buttons'=>array
			(
		       'update' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/language/update", array("language"=>$data->code, "lang" => Yii::app()->getLanguage()))'
		        ),
		       'delete' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/language/delete", array("language"=>$data->code, "lang" => Yii::app()->getLanguage()))'
		        ),
			),
         ),
	),
)); ?>
