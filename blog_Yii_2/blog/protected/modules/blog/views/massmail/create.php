<?php
$this->pageTitle = Message::getTranslation(160);
$this->breadcrumbs=array(
	Message::getTranslation(160),
);
?>
<h1><?= Message::getTranslation(160) ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'language' => isset($language) ? $language : '')); ?>