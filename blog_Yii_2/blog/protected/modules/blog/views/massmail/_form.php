<?php
	$tinymce = '
		tinymce.init({' .
			(file_exists('js/tinymce/langs/' . Yii::app()->getLanguage() . '.js') ? ('language: "' . Yii::app()->getLanguage() . '",') : '') .
		    '
		    content_css: "' . Yii::app()->baseUrl .  '/js/tinymce/content.css",
		    selector: "textarea",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor colorpicker textpattern"
		    ],
		    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pastetext",
		    toolbar2: "forecolor backcolor emoticons | fontselect | fontsizeselect | print preview code pagebreak media",
		    image_advtab: true,
		    templates: [
		        {title: "Test template 1", content: "Test 1"},
		        {title: "Test template 2", content: "Test 2"}
		    ]
		});		
	';
	
	$categoriesscript = "
		disableCategories();
		
		jQuery('body').on('change','#languages',function(){jQuery.ajax({'url':'" . $this->createUrl('/blog/user/categoriesByLanguage') . "','type':'POST','cache':false,'data':jQuery(this).parents('form').serialize(),
		'success':function(html){jQuery('#categories').html(html);disableCategories();}});return false;});

		$('#addbutton').click(function(){
			if($('#categories option:selected').val() != null)
			{
		    	$('#MassMail_categories').append('<option value=\"' + $('#languages').val() + '|' + $('#categories').val() + '\">' 
		    		+ $('#languages option:selected').val() + ': ' + $('#categories option:selected').text() + '</option>');
		    	$('#categories option:selected').attr('disabled','disabled').removeAttr('selected');
		   }
		});
		$('#removebutton').click(function(){
			$('#MassMail_categories option:selected').remove();
			
			$('#categories option').each(function(){
				var value = $('#languages option:selected').val() + '|' + $(this).val();
				var found = false;
				$('#MassMail_categories option').each(function(){
					if($(this).val() == value)
						found = true;
				});
				if(!found)
					$(this).removeAttr('disabled');
			});
		});
		
		$('#submitemail').click(function(){
	       $('#MassMail_categories option').each(function () {
	            $(this).attr('selected', true);
	        });
		});
		
		function disableCategories()
		{
			$('#MassMail_categories option').each(function(){
				var value = $(this).val();
				$('#categories option').each(function(){
					if(($('#languages option:selected').val() + '|' + $(this).val()) == value)
						$(this).attr('disabled','disabled').removeAttr('selected');
				});
			});
		}	
	";

	$autocompletescript = '
		 $(function() {
		     function split(val) {
		         return val.split(/,\s*/);
		     }

		     function extractLast(term) {
		         return split(term).pop();
		     }
		     $("#MassMail_users")
		         // don\'t navigate away from the field on tab when selecting an item
		         .bind("keydown", function(event) {
		             if (event.keyCode === $.ui.keyCode.TAB &&
		                 $(this).autocomplete("instance").menu.active) {
		                 event.preventDefault();
		             }
		         })
		         .autocomplete({
		             source: function(request, response) {
		                 $.getJSON("' . $this->createUrl('/blog/user/suggestUsers') . '", {
		                     term: extractLast(request.term),
		                 }, response);
		             },
		             search: function() {
		                 // custom minLength
		                 var term = extractLast(this.value);
		                 if (term.length < 1) {
		                     return false;
		                 }
		             },
		             focus: function() {
		                 // prevent value inserted on focus
		                 return false;
		             },
		             select: function(event, ui) {
		                 var terms = split(this.value);
		                 // remove the current input
		                 terms.pop();
		                 // add the selected item
		                 terms.push(ui.item.value);
		                 // add placeholder to get the comma-and-space at the end
		                 terms.push("");
		                 this.value = terms.join(", ");
		                 return false;
		             }
		         });
		 });
	 ';
	 
	$baseUrl = Yii::app()->baseUrl; 
	$cs = Yii::app()->getClientScript();
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
	$cs->registerScriptFile($baseUrl.'/js/tinymce/tinymce.min.js',CClientScript::POS_END);
	$cs->registerScript('autocompletescript', $autocompletescript);
	$cs->registerCssFile(Yii::app()->baseUrl . '/css/juiskins/jqueryui-smoothness.css');
	$cs->registerScript('tinymce', $tinymce);
	$cs->registerScript('categoriesscript', $categoriesscript);
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>
	<p class="note"><?= Message::getTranslation(8) ?></p>

	<?php echo CHtml::errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('style'=>'width:100%','maxlength'=>128)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo CHtml::activeTextArea($model,'content',array('style'=>'width:100%','rows'=>15, 'cols'=>70)); ?>
		<?php echo $form->error($model,'content'); ?>
		<p class="hint"><?= Message::getTranslation(170) ?></p>
	</div>
	<?php if(Parameter::getValue(30)): ?>
	<div class="fieldset">
		<h2><span><?= Message::getTranslation(157) ?></span></h2>
		<div class="rememberMe">
			<?php echo $form->radioButtonList($model,'type', Lookup::items('MassMailType'), array('separator'=>'&nbsp;&nbsp;&nbsp;')); ?>
			<p class="hint"><?= Message::getTranslation(161) ?></p>
			<?php echo $form->error($model,'type'); ?>
		</div>
	</div>
	<?php endif; ?>
	<div class="fieldset">
		<h2><span><?= Message::getTranslation(162) ?></span></h2>
			<?php echo $form->textField($model,'users', array('style' => 'width:100%')); ?>
			<p class="hint"><?= Message::getTranslation(71) ?></p>
			<?php echo $form->error($model,'users'); ?>
	</div>
	<?php if(Parameter::getValue(30)): ?>
	<div class="fieldset">
		<h2><span><?= Message::getTranslation(163) ?></span></h2>
		<div>
			<div>
				<?php echo CHtml::label(Message::getTranslation(167), false); ?>
				<?php //echo CHtml::dropDownList('languages', Yii::app()->getLanguage(), Language::getLanguagesOptions()); ?>
				<?php echo CHtml::dropDownList('categories', '', Category::getCategoriesOptions(Yii::app()->getLanguage())); ?>
				<?php echo CHtml::button('', array('class' => 'add', 'id' => 'addbutton')); ?>
			</div>
			<div>
				<?php echo $form->labelEx($model,'categories'); ?>
				<?php echo $form->listBox($model, 'categories', $model->categories, array('multiple' => 'multiple', 'style' => 'width:180px;height:80px;')); ?>
				<?php echo CHtml::button('', array('class' => 'remove', 'id' => 'removebutton')); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="fieldset">
		<h2><span><?= Message::getTranslation(164) ?></span></h2>
			<?php echo $form->listBox($model, 'roles', AuthItem::getRolesOptions(), array('multiple' => 'multiple', 'style' => 'width:140px;height:80px;')); ?>
			<p class="hint"><?= Message::getTranslation(165) ?></p>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Lookup::items('MassMailStatus')); ?>
		<?php echo $form->error($model,'status'); ?>
		<span class="hint"><?= Message::getTranslation(166) ?></span>
	</div>
	<?php if($model->status == MassMail::STATUS_DRAFT): ?>
	<div class="row buttons">
		<?php if($model->isNewRecord): ?>
			<?php echo CHtml::submitButton(Message::getTranslation(50), array('class' => 'bigbutton', 'id' => 'submitemail')); ?>
		<?php elseif(true): ?>
			<?php echo CHtml::submitButton(Message::getTranslation(70), array('class' => 'bigbutton', 'id' => 'submitemail')); ?>		
		<?php endif; ?>
	</div>
	<?php endif; ?>
	
	<?= CHtml::hiddenField('lang', Yii::app()->getLanguage()) ?>
<?php $this->endWidget(); ?>

</div><!-- form -->