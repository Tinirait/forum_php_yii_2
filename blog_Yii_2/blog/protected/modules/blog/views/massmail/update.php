<?php
$this->pageTitle = Message::getTranslation(159);
$this->breadcrumbs=array(
	Message::getTranslation(159),
);
?>

<h1><?= Message::getTranslation(159) ?> <i><?php echo CHtml::encode($model->title); ?></i></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>