<?php
$this->pageTitle = Message::getTranslation(158);
$this->breadcrumbs=array(
	Message::getTranslation(158),
);	
$style ='	@media 
	all and (max-width: 950px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
		td:nth-of-type(1):before { content: "' . Message::getTranslation(27) . '"; }
		td:nth-of-type(2):before { content: "' . Message::getTranslation(28) . '"; }
		td:nth-of-type(3):before { content: "' . Message::getTranslation(157) . '"; }
		td:nth-of-type(4):before { content: "' . Message::getTranslation(33) . '"; }
		td:nth-of-type(5):before { content: "' . Message::getTranslation(31) . '"; }
		td:nth-of-type(6):before { content: "' . Message::getTranslation(32) . '"; }
		td:nth-of-type(7):before { content: "' . Message::getTranslation(30) . '"; }
		td:nth-of-type(8):before { content: "' . Message::getTranslation(139) . '"; }
	}';
Yii::app()->getClientScript()->registerCss('style', $style);
?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php elseif(Yii::app()->user->hasFlash('error')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>
<h1><?= Message::getTranslation(158) ?></h1>
<div style="text-align:center;margin-bottom:20px;">
<?php echo CHtml::link(Message::getTranslation(50),array('massmail/create', 'lang' => Yii::app()->getLanguage()), array('class' => 'btncreate')); ?>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/gridview.css'),
	'cssFile' => Yii::app()->baseUrl . '/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model->with('category'),
	'htmlOptions' => array('class' => 'grid-view rounded'),
	'ajaxUpdate'=>false,
	'columns'=>array(
		array(
			'name'=>'id',
			'type'=>'raw',
			'value'=>'$data->id',
    		'htmlOptions'=>array('style'=>'width:40px'),			
		),
		array(
			'name'=>'title',
			'type'=>'raw',
			'value'=>'CHtml::encode($data->title)'
		),
		array(
			'name'=>'type',
			'value'=>'Lookup::item("MassMailType",$data->type)',
			'filter'=>Lookup::items('MassMailType'),
		),
		array(
			'name'=>'user_id',
			'type'=>'raw',
			'value'=>'is_null($data->user) ? "" : $data->user->username',
    		'htmlOptions'=>array('style'=>'width:40px'),			
		),
		array(
			'name'=>'create_time',
			'type'=>'datetime',
			'filter'=>false,
		),
		array(
			'name'=>'update_time',
			'type'=>'datetime',
			'filter'=>false,
		),
		array(
			'name'=>'status',
			'value'=>'Lookup::item("MassMailStatus",$data->status)',
			'filter'=>Lookup::items('MassMailStatus'),
		),
        array(
            'header' => Message::getTranslation(49),
            'class' => 'CButtonColumn',
            'template' => '{update}&nbsp;&nbsp;&nbsp;{delete}',
            'updateButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-update.png',
            'deleteButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-delete.png',
            'buttons'=>array
    		(
		       'update' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/massmail/update", array("massmail"=>$data->id, "lang" => Yii::app()->getLanguage()))'
		        ),
		       'delete' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/massmail/delete", array("massmail"=>$data->id, "lang" => Yii::app()->getLanguage()))'
		        ),
 			),
         ),
	),
)); ?>




