<?php
	$userscript = "		
		disableCategories();
		
		jQuery('body').on('change','#languages',function(){jQuery.ajax({'url':'" . $this->createUrl('/blog/user/categoriesByLanguage') . "','type':'POST','cache':false,'data':jQuery(this).parents('form').serialize(),
		'success':function(html){jQuery('#categories').html(html);disableCategories();}});return false;});

		$('#rolesinfo').mouseover(function(){
			$('#rolesinfo2').fadeIn();
		});
		$('#rolesinfo').mouseout(function(){
			$('#rolesinfo2').fadeOut();
		});
		$('#addbutton').click(function(){
			if($('#categories option:selected').val() != null)
			{
		    	$('#User_subscribed_categories').append('<option value=\"' + $('#languages').val() + '|' + $('#categories').val() + '\">' 
		    		+ $('#languages option:selected').val() + ': ' + $('#categories option:selected').text() + '</option>');
		    	$('#categories option:selected').attr('disabled','disabled').removeAttr('selected');
		   }
		});
		$('#removebutton').click(function(){
			$('#User_subscribed_categories option:selected').remove();
			
			$('#categories option').each(function(){
				var value = $('#languages option:selected').val() + '|' + $(this).val();
				var found = false;
				$('#User_subscribed_categories option').each(function(){
					if($(this).val() == value)
						found = true;
				});
				if(!found)
					$(this).removeAttr('disabled');
			});
		});
		
		$('#submituser').click(function(){
	       $('#User_subscribed_categories option').each(function () {
	            $(this).attr('selected', true);
	        });
		});
		
		function disableCategories()
		{
			$('#User_subscribed_categories option').each(function(){
				var value = $(this).val();
				$('#categories option').each(function(){
					if(($('#languages option:selected').val() + '|' + $(this).val()) == value)
						$(this).attr('disabled','disabled').removeAttr('selected');
				});
			});
		}
	
		// Password strength meter
	    /*$('#User_repeatPassword, #User_password').pStrength({
	        'changeBackground'          : false,
	        'onPasswordStrengthChanged' : function(passwordStrength, strengthPercentage) {
	            if ($(this).val()) {
	                $.fn.pStrength('changeBackground', this, passwordStrength);
	            } else {
	                $.fn.pStrength('resetStyle', this);
	            }
	            $('#' + $(this).data('display')).html('(" . Message::getTranslation(156) . ": ' + strengthPercentage + '%)');
	        },
	    });*/
	    
	    // Password generator
		$.extend({
		  password: function (length, special) {
		    var iteration = 0;
		    var password = '';
		    var randomNumber;
		    if(special == undefined){
		        var special = false;
		    }
		    while(iteration < length){
		        randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
		        if(!special){
		            if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
		            if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
		            if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
		            if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
		        }
		        iteration++;
		        password += String.fromCharCode(randomNumber);
		    }
		    return password;
		  }
		});
		
		$('#generatepasswordbutton').click(function(){
			var password = $.password(10,true);
			$('#generatepasswordtext').val(password);
			$('#User_password').val(password);
			$('#User_repeatPassword').val(password);
			$('#User_password').keyup();
			$('#User_repeatPassword').keyup();
		});
		
		if($('#User_status').val() == " . User::STATUS_BANNED . ")
		{
			$('#ban_attributes').css('display','block');
		}
			
		// Only show ban options when select status == banned
		$('#User_status').change(function(){
			if($('#User_status').val() == " . User::STATUS_BANNED . ")
			{
				$('#ban_attributes').fadeIn();
			}
			else
			{
				$('#ban_attributes').fadeOut();
			}
		});

		$('#User_ban_forever').change(function(){
			if($('#User_ban_forever').prop('checked') )
			{
				$('#ban_expires_div').slideUp();
			}
			else
			{
				$('#ban_expires_div').slideDown();
			}
		});
		
		if($('#User_ban_forever').prop('checked'))
		{
			$('#ban_expires_div').css('display','none');
		}
		else
		{
			$('#ban_expires_div').css('display','block');
		}

		$('#User_subscribe').change(function(){

			if($('#User_subscribe').prop('checked') )
			{
				$('#subscriptioncategoriesdiv').slideDown();
			}
			else
			{
				$('#subscriptioncategoriesdiv').slideUp();
			}
		});
				
		if($('#User_subscribe').prop('checked'))
		{
			$('#subscriptioncategoriesdiv').css('display','block');
		}
		else
		{
			$('#subscriptioncategoriesdiv').css('display','none');
		}	
	";

	$baseUrl = Yii::app()->baseUrl; 
	$cs = Yii::app()->getClientScript();
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
	$cs->registerScriptFile($baseUrl.'/js/tinymce/tinymce.min.js',CClientScript::POS_END);
	$cs->registerScriptFile($baseUrl.'/js/pStrength.jquery.js',CClientScript::POS_END);
	$cs->registerScript('userscript', $userscript);
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
		'htmlOptions' => array('enctype' => 'multipart/form-data')
	)); ?>
	<p class="note"><?= Message::getTranslation(8) ?></p>

	<?php echo CHtml::errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('style'=>'width:100%', 'autocomplete' => 'off')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'password', array('class' => 'left')); ?>
		<!--<div style="margin-left:10px" class="left" id="pwdisplay"></div><div class="clear"></div>-->
		<?php //echo CHtml::button(Message::getTranslation(155), array('id' => 'generatepasswordbutton')); ?>
		<?php //echo CHtml::textField('','', array('id' => 'generatepasswordtext', 'style' => 'width:120px'))  ?>
		<div class="clear"></div>
		<?php echo $form->passwordField($model,'password',array('style'=>'width:100%', 'autocomplete' => 'off', 'data-display'=>'pwdisplay')); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'repeatPassword', array('class' => 'left')); ?>
                <!--<div style="margin-left:10px" class="left" id="rpwdisplay"></div><div class="clear"></div>-->
		<?php echo $form->passwordField($model,'repeatPassword',array('style'=>'width:100%', 'autocomplete' => 'off', 'data-display'=>'rpwdisplay')); ?>
		<?php echo $form->error($model,'repeatPassword'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('style'=>'width:100%', 'autocomplete' => 'off')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'url'); ?>
		<?php echo $form->textField($model,'url',array('style'=>'width:100%', 'autocomplete' => 'off')); ?>
		<?php echo $form->error($model,'url'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo $form->fileField($model,'image'); ?>
		<?php echo $form->error($model,'image'); ?>
		<?php
			if($model->scenario === 'update')
				echo CHtml::image(Yii::app()->baseUrl . '/' . User::IMAGE_DIRECTORY . $model->getImageFile(), '', array('style' => 'border:1px solid black;'));
		 ?>
	</div>
	<?php 
		if(Yii::app()->user->checkAccess('admin') && $model->scenario !== 'register'): ?>
			<?php
				$disabled = '';
				if($model->scenario === 'update')
				{
					$disabled = $model->authAssignment->itemname == 'admin' ? 'disabled' : '';
				}
			?>
			<div class="row">
				<?php echo $form->labelEx($model,'role'); ?>
				<?php echo $form->dropDownList($model,'role',AuthItem::getRolesOptions(),array('disabled' => $disabled)); ?>
				<?php echo $form->error($model,'role'); ?>
				<?php echo CHtml::link(Message::getTranslation(129), '', array('id' => 'rolesinfo')); ?>
			</div>
			<div id="rolesinfo2" style="border:1px dashed black;padding:5px;display:none">
				<b>Читатель (Reader):</b> <?php echo Message::getTranslation(130); ?><br/>
				<b>Автор (Author):</b> <?php echo Message::getTranslation(131) . ' ' . Message::getTranslation(132) . ' ' . Message::getTranslation(136) ?><br/>
				<b>Редактор (Editor):</b> <?php echo Message::getTranslation(131) . ' ' . Message::getTranslation(133) . ' ' . Message::getTranslation(136) ?><br/>
				<b>Издатель (Publisher):</b> <?php echo Message::getTranslation(131) . ' ' . Message::getTranslation(134) ?><br/>
				<b>Администратор (Admin):</b> <?php echo Message::getTranslation(135); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'status'); ?>
				<?php echo $form->dropDownList($model,'status',Lookup::items('UserStatus'),array('disabled' => $disabled)); ?><?php echo $model->scenario === 'insert' ? '' : '' ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
			<div id="ban_attributes" style="display:none">
				<div class="row">
					<?php echo $form->labelEx($model,'ban_reason'); ?>
					<?php echo $form->textField($model,'ban_reason',array('style'=>'width:100%', 'autocomplete' => 'off')); ?>
					<?php echo $form->error($model,'ban_reason'); ?>
				</div>
				<div class="row rememberMe">
					<?php echo $form->labelEx($model,'ban_forever'); ?>
					<?php echo $form->checkBox($model,'ban_forever'); ?>
					<?php echo $form->error($model,'ban_forever'); ?>
				</div>
				<div id="ban_expires_div" class="row">				
					<?php echo $form->labelEx($model,'ban_expires'); ?>
					<?php
						$this->widget('zii.widgets.jui.CJuiDatePicker',array(
						    'name'=>'datepicker',
						    'flat'=>true,//remove to hide the datepicker
							'model'     => $model,
							'attribute' => 'ban_expires',
							'language'=> Yii::app()->language,
							'themeUrl'=>Yii::app()->baseUrl.'/css/juiskins',
							'theme'=>'sunny',
						    'options'=>array(
						        'dateFormat' => '@',
						        'showAnim'=>'slide',//'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
						        'changeMonth'=>true,
						        'changeYear'=>true,
						        //'yearRange'=>'2000:2099',
						        'minDate' => 'new Date()',      // minimum date
						        //'maxDate' => '5222222222222',      // maximum date
						    ),
						    'htmlOptions'=>array(
						        
						    ),
						));
					?>
					<?php echo $form->error($model,'ban_expires'); ?>
				</div>
			</div>
	<?php
		endif;
	?>
			<div class="row">
				<?php echo $form->labelEx($model,'additional_info'); ?>
				<?php echo $form->textArea($model,'additional_info',array('rows'=>6, 'style' => 'width:100%;')); ?>
				<?php echo $form->error($model,'additional_info'); ?>
			</div>
	<?php	
		if(Parameter::getValue(30)):
	?>		<div class="fieldset">
				<h2><span><?= Message::getTranslation(154)?></span></h2>
				<div style="margin-bottom:10px" class="rememberMe">
					<?php echo $form->labelEx($model,'subscribe'); ?>
					<?php echo $form->checkBox($model,'subscribe'); ?>
					<?php echo $form->error($model,'subcribe'); ?>
				</div>
				<div id="subscriptioncategoriesdiv">
					<div>
						<?php echo CHtml::label(Message::getTranslation(151), false); ?>
						<?php //echo CHtml::dropDownList('languages', Yii::app()->getLanguage(), Language::getLanguagesOptions()); ?>
						<?php echo CHtml::dropDownList('categories', '', Category::getCategoriesOptions(Yii::app()->getLanguage())); ?>
						<?php echo CHtml::button('', array('class' => 'add', 'id' => 'addbutton')); ?>
					</div>
					<div>
						<?php echo $form->labelEx($model,'subscribed_categories'); ?>
						<?php echo $form->listBox($model, 'subscribed_categories', $model->getSubscriptionOptions(), array('multiple' => 'multiple', 'style' => 'width:180px;height:80px;')); ?>
						<?php echo CHtml::button('', array('class' => 'remove', 'id' => 'removebutton')); ?>
					</div>
				</div>
			</div>			
				
	<?php 
		endif;
		if($model->scenario === 'register'):
	?>	
			<?php if(CCaptcha::checkRequirements()): ?>
				<div class="row">
					<?php echo $form->labelEx($model,'verifyCode'); ?>
						<?php $this->widget('CCaptcha'); ?>
						<br/>
						<?php echo $form->textField($model,'verifyCode'); ?>
						<div style="color:black;" class="hint">
							<?= Message::getTranslation(148) ?>
						</div>
					<?php echo $form->error($model,'verifyCode'); ?>
				</div>
			<?php endif; ?>			
	<?php
		endif; 
	?>
	<div class="row buttons">
		<?php
			if($model->scenario === 'insert'):
				echo CHtml::submitButton(Message::getTranslation(50), array('class' => 'bigbutton', 'id' => 'submituser'));
			elseif($model->scenario === 'update'):
				echo CHtml::submitButton(Message::getTranslation(70), array('class' => 'bigbutton', 'id' => 'submituser'));			
			elseif($model->scenario === 'register'):
				echo CHtml::submitButton(Message::getTranslation(106), array('class' => 'bigbutton', 'id' => 'submituser'));
			endif; 
		?>
	</div>
	<?= CHtml::hiddenField('lang', Yii::app()->getLanguage()) ?>

<?php $this->endWidget(); ?>

</div><!-- form -->