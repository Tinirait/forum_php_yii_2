<?php
$this->pageTitle = Message::getTranslation(61);
$this->breadcrumbs=array(
	Message::getTranslation(61),
);
?>
<h1><?= Message::getTranslation(61) ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>