<?php
$this->pageTitle = Message::getTranslation(104);
$this->breadcrumbs=array(
	Message::getTranslation(104),
);
?>

<h1><?= Message::getTranslation(104) ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>