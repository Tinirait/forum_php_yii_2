<?php
	$this->pageTitle = Message::getTranslation(173);
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
?>
<center>
<h1><?= Message::getTranslation(173) ?></h1>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php elseif(Yii::app()->user->hasFlash('error')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
		'htmlOptions' => array('enctype' => 'multipart/form-data')
	)); ?>
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('autocomplete' => 'off')); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<?php if(CCaptcha::checkRequirements()): ?>
		<div class="row">
			<?php echo $form->labelEx($model,'verifyCode'); ?>
				<?php $this->widget('CCaptcha'); ?>
				<br/>
				<?php echo $form->textField($model,'verifyCode'); ?>
				<div style="color:black;" class="hint">
					<?= Message::getTranslation(148) ?>
				</div>
			<?php echo $form->error($model,'verifyCode'); ?>
		</div>
	<?php endif; ?>	
	<div class="row buttons">
		<?php 
			echo CHtml::submitButton("Submit", array('class' => 'bigbutton'));
		?>
	</div>

<?php $this->endWidget(); ?>
</div>
</center>