<?php
$this->pageTitle = Message::getTranslation(55);
$this->breadcrumbs=array(
	Message::getTranslation(55),
);
?>

<h1><?= Message::getTranslation(55) ?> <i><?php echo CHtml::encode($model->username); ?></i></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>