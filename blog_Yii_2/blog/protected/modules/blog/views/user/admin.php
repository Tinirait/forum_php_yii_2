<?php
$this->pageTitle = Message::getTranslation(67);
$this->breadcrumbs=array(
	Message::getTranslation(67),
);
$style ='	@media 
	all and (max-width: 950px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
		td:nth-of-type(1):before { content: "' . Message::getTranslation(43) . '"; }
		td:nth-of-type(2):before { content: "' . Message::getTranslation(11) . '"; }
		td:nth-of-type(3):before { content: "' . Message::getTranslation(12) . '"; }
		td:nth-of-type(4):before { content: "' . Message::getTranslation(137) . '"; }
		td:nth-of-type(5):before { content: "' . '#' . substr(Message::getTranslation(17),0,3) . '"; }
		td:nth-of-type(6):before { content: "' . Message::getTranslation(42) . '"; }
		td:nth-of-type(7):before { content: "' . Message::getTranslation(31) . '"; }
		td:nth-of-type(8):before { content: "' . Message::getTranslation(138) . '"; }
		td:nth-of-type(9):before { content: "' . Message::getTranslation(30) . '"; }
		td:nth-of-type(10):before { content: "' . Message::getTranslation(139) . '"; }
	}';
Yii::app()->getClientScript()->registerCss('style', $style);
?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php elseif(Yii::app()->user->hasFlash('error')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>

<h1><?= Message::getTranslation(67) ?></h1>
<div style="text-align:center;margin-bottom:20px;">
<?php echo CHtml::link(Message::getTranslation(50),array('user/create', 'lang' => Yii::app()->getLanguage()), array('class' => 'btncreate')); ?>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/gridview.css'),
	'cssFile' => Yii::app()->baseUrl . '/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'htmlOptions' => array('class' => 'grid-view rounded'),
	'ajaxUpdate'=>false,
	'columns'=>array(
		array(
			'name'=>'username',
			'type'=>'raw',
			'value'=>'CHtml::link($data->username, array("/blog/user/view", "user" => $data->id, "lang" => Yii::app()->getLanguage()))'
		),
		array(
			'name'=>'email',
			'type'=>'raw',
			'value'=>'$data->email',
		),
		array(
			'name'=>'url',
			'type'=>'raw',
			'value'=>'CHtml::link($data->url, $data->url, array("target" => "_blank"))',
		),
		array(
			'name'=>'role',
			'value'=>'$data->role',
			'filter'=>AuthItem::getRolesOptions()
		),
		array(
			'name'=>'commentCount',
			'type'=>'raw',
			'value'=>'$data->commentCount',
    		'filter'=>false,
		),
		array(
			'name'=>'image',
			'type'=>'raw',
			'value'=>'CHtml::image(Yii::app()->baseUrl . "/" . User::IMAGE_DIRECTORY . $data->getImageFile(), "", array("class" => "avatar"))',
			'filter'=>false,
		),
		array(
			'name'=>'create_time',
			'type'=>'datetime',
			'filter'=>false,
		),
		array(
			'name'=>'last_visit',
			'type'=>'datetime',
			'filter'=>false,
		),
		array(
			'name'=>'status',
			'value'=>'Lookup::item("UserStatus",$data->status)',
			'filter'=>Lookup::items('UserStatus'),
		),
        array(
        	'htmlOptions'=>array('style'=>'width:70px'),
            'header' => Message::getTranslation(49),
            'class' => 'CButtonColumn',
            'template' => '{update}&nbsp;{delete}',
            'viewButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-view.png',
            'updateButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-update.png',
            'deleteButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-delete.png',
	        'buttons'=>array
			(
		       /*'view' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/user/view", array("user"=>$data->id, "lang" => Yii::app()->getLanguage()))'
		        ),*/
		       'update' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/user/update", array("user"=>$data->id, "lang" => Yii::app()->getLanguage()))'
		        ),
		       'delete' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/user/delete", array("user"=>$data->id, "lang" => Yii::app()->getLanguage()))'
		        ),
			),
         ),
	),
)); ?>
