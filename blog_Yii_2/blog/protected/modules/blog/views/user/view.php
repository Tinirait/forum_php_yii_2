<?php
	$this->pageTitle = $model->username;
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
	$userviewstyle = '
		td{
			border:1px solid black;
			word-break: break-all;
			padding-left: 10px;
			padding-right: 10px;
		}
		td:nth-child(1){
			background: gray;
			font-weight: bold;
			color: white;
			width:130px;
		}
		table
		{
			margin-bottom: 20px;
			width: 100%;
			border:1px solid black;
		}
	';
	Yii::app()->clientScript->registerCss('userview', $userviewstyle);
?>
<table>
	<tr>
		<td>
			<?= Message::getTranslation(43) ?>
		</td>
		<td>
			<?= $model->username ?>
		</td>
	</tr>
	<tr>
		<td>
			<?= Message::getTranslation(31) ?>
		</td>
		<td>
			<?php echo date('j ',$model->create_time) . Message::getTranslation(114 + date('n',$model->create_time)) . date(', Y',$model->create_time); ?>
		</td>
	</tr>
	<tr>
		<td>
			<?= Message::getTranslation(12) ?>
		</td>
		<td>
			<?= CHtml::link($model->url, $model->url, array('target' => '_blank')) ?>
		</td>
	</tr>
	<tr>
		<td>
			<?= Message::getTranslation(42) ?>
		</td>
		<td>
			<?= CHtml::image(Yii::app()->baseUrl . '/' . User::IMAGE_DIRECTORY . $model->getImageFile(), '', array('style' => 'max-width:100%;border:1px solid black;')); ?>
		</td>
	</tr>
	<tr>
		<td>
			#<?= Message::getTranslation(17) ?>
		</td>
		<td>
			<?= $model->commentCount ?>
		</td>
	</tr>
	<tr>
		<td>
			<?= Message::getTranslation(153) ?>
		</td>
		<td>
			<?php $this->beginWidget('CMarkdown', array('purifyOutput' => true));
				echo $model->additional_info;
			$this->endWidget();?>
		</td>
	</tr>
</table>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

	<div style="text-align: center;background:url(<?= Yii::app()->baseUrl . '/images/bgmail.jpg' ?>);background-size:100% 100%;border:1px solid black;padding-top:25px;" class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'contact-form',
		'htmlOptions' => array('style' => 'text-align: center;')
	)); ?>
		<span style="font-size:30px"><?= Message::getTranslation(181) ?></span>
		<p style="margin-top:30px;"><?= Message::getTranslation(8) ?></p>
	
		<?php echo $form->errorSummary($contact); ?>
	
		<div class="row">
			<?php echo $form->labelEx($contact,'subject', array('class' => 'shadowtext')); ?>
			<?php echo $form->textField($contact,'subject',array('size'=>40,'maxlength'=>128, 'class' => 'inputtext')); ?>
			<?php echo $form->error($contact,'subject'); ?>
		</div>
	
		<div class="row">
			<?php echo $form->labelEx($contact,'body', array('class' => 'shadowtext')); ?>
			<?php echo $form->textArea($contact,'body',array('rows'=>6, 'cols'=>50, 'class' => 'inputtext')); ?>
			<?php echo $form->error($contact,'body'); ?>
		</div>
	
		<div class="row buttons">
			<?php echo CHtml::submitButton(Message::getTranslation(186), array('class' => 'bigbutton')); ?>
		</div>
	
	<?php $this->endWidget(); ?>
	
	</div><!-- form -->
<?php endif; ?>
