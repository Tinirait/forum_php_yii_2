<?php
$this->pageTitle = Message::getTranslation(53);
$this->breadcrumbs=array(
	Message::getTranslation(53),
);
?>

<h1><?= Message::getTranslation(53) ?> <i><?php echo CHtml::encode($model->name); ?></i></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>