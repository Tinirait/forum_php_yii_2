<?php
if(!is_null($model->id))
{
	$script=
		"$(document).ready(function(){
			$('#Category_category_id option').each(function(){
				if({$model->id} == $(this).val())
					$(this).attr('disabled','disabled');
			});
		});";
	Yii::app()->getClientScript()->registerScript('script', $script);
}
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
?>
	
<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>

	<p class="note"><?= Message::getTranslation(8) ?></p>

	<?php echo CHtml::errorSummary($model); ?>

        <!--<div class="row">
		<?php //echo $form->labelEx($model,'id'); ?>
		<?php //echo $form->textField($model,'id',array('style'=>'width:100%','maxlength'=>3)); ?>
		<?php //echo $form->error($model,'id'); ?>
	</div>-->
	
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('style'=>'width:100%','maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	<!--<div class="row">
		<?php
			//$htmlOptions = array("ajax"=>array("url" => $this->createUrl("/blog/category/categoriesByLanguage"), "type" => "POST", "update" => "#Category_category_id"), "disabled" => $model->scenario === 'update' ? 'disabled' : '');
		?>
		<?php //echo $form->labelEx($model,'language'); ?>
		<?php //echo $form->dropDownList($model,'language', Language::getLanguagesOptions(), $htmlOptions); ?>
		<?php //echo $form->error($model,'language'); ?>
	</div>-->
	
	<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList($model,'category_id', Category::getCategoriesOptions(is_null($model->id) ? $language : $model->language), array('empty'=>Message::getTranslation(194))); ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Lookup::items('CategoryStatus')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Message::getTranslation(50) : Message::getTranslation(70), array('class' => 'bigbutton')); ?>
	</div>
	
	<?= CHtml::hiddenField('lang', Yii::app()->getLanguage()) ?>

<?php $this->endWidget(); ?>

</div><!-- form -->