<?php
$this->pageTitle = Message::getTranslation(59);
$this->breadcrumbs=array(
	Message::getTranslation(59),
);
?>
<h1><?= Message::getTranslation(59) ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'language'=>$language)); ?>