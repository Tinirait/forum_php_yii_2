<?php
$this->pageTitle = Message::getTranslation(65);
$this->breadcrumbs=array(
	Message::getTranslation(65),
);
$style ='	@media 
	all and (max-width: 950px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
		td:nth-of-type(1):before { content: "' . Message::getTranslation(27) . '"; }
		td:nth-of-type(2):before { content: "' . Message::getTranslation(10) . '"; }
		td:nth-of-type(3):before { content: "' . Message::getTranslation(40) . '"; }
		td:nth-of-type(4):before { content: "' . Message::getTranslation(35) . '"; }
		td:nth-of-type(5):before { content: "' . Message::getTranslation(30) . '"; }
		td:nth-of-type(6):before { content: "' . Message::getTranslation(139) . '"; }
	}';
Yii::app()->getClientScript()->registerCss('style', $style);
?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php elseif(Yii::app()->user->hasFlash('error')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>

<h1><?= Message::getTranslation(65) ?></h1>
<div style="text-align:center;margin-bottom:20px;">
<?php echo CHtml::link(Message::getTranslation(50),array('category/create', 'lang' => Yii::app()->getLanguage()), array('class' => 'btncreate')); ?>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/gridview.css'),
	'cssFile' => Yii::app()->baseUrl . '/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'htmlOptions' => array('class' => 'grid-view rounded'),
	'ajaxUpdate'=>false,
	'columns'=>array(
		array(
			'name'=>'id',
			'type'=>'raw',
			'value'=>'$data->id',
    		'htmlOptions'=>array('style'=>'width:40px'),			
		),
		array(
			'name'=>'name',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->name), array("post/index", "category" => $data->id, "lang" => Yii::app()->getLanguage()))'
		),
		array(
			'name'=>'category_id',
			'type'=>'raw',
			'value'=>'is_null($data->category) ? "" : $data->category->name'
		),
		/*array(
			'name'=>'language',
			'value'=>'Language::getNameFromCode($data->language)',
			'filter'=>Language::getLanguagesOptions()
		),*/
		array(
			'name'=>'status',
			'value'=>'Lookup::item("CategoryStatus",$data->status)',
			'filter'=>Lookup::items('CategoryStatus'),
		),
        array(
        	'htmlOptions'=>array('style'=>'width:70px'),
            'header' => Message::getTranslation(49),
            'class' => 'CButtonColumn',
            'template' => '{update}&nbsp;{delete}',
            'viewButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-view.png',
            'updateButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-update.png',
            'deleteButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-delete.png',
           'buttons'=>array
    		(
		       /*'view' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/post/index", array("category"=>$data->id, "lang" => $data->language))'
		        ),*/
		       'update' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/category/update", array("category"=>$data->id, "language" => $data->language, "lang" => Yii::app()->getLanguage()))'
		        ),
		       'delete' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/category/delete", array("category"=>$data->id, "language" => $data->language, "lang" => Yii::app()->getLanguage()))'
		        ),
 			),
         ),
	),
)); ?>
