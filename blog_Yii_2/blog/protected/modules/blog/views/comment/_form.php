<?php
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
?>	
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comment-form',
	'enableClientValidation'=>false,
)); ?>

	<p class="note"><?= Message::getTranslation(8) ?></p>

	<?php if(Yii::app()->user->isGuest || (is_null($model->user_id) && $model->scenario === 'updateguest')): ?>
		<div class="row">
			<?php echo $form->labelEx($model,'author'); ?>
			<?php echo $form->textField($model,'author',array('class'=>'inputtext', 'maxlength'=>128)); ?>
			<?php echo $form->error($model,'author'); ?>
		</div>	
		<div class="row">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('class'=>'inputtext', 'maxlength'=>128)); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>
		<?php if(Parameter::getValue(17) || $model->scenario === 'updateguest'): ?>
		<div class="row">
			<?php echo $form->labelEx($model,'url'); ?>
			<?php echo $form->textField($model,'url',array('class'=>'inputtext', 'size'=>60,'maxlength'=>128)); ?>
			<?php echo $form->error($model,'url'); ?>
		</div>
			<?php if(Yii::app()->user->isGuest): ?>
				<?php if(CCaptcha::checkRequirements()): ?>
					<div class="row">
						<?php echo $form->labelEx($model,'verifyCode'); ?>
							<?php $this->widget('CCaptcha'); ?>
							<br/>
							<?php echo $form->textField($model,'verifyCode'); ?>
							<div style="color:black;" class="hint">
								<?= Message::getTranslation(148) ?>
							</div>
						<?php echo $form->error($model,'verifyCode'); ?>
					</div>
				<?php endif; ?>	
			<?php endif; ?>
		<?php endif; ?>
	<?php  
		endif; 
	?>
	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content',array('class'=>'inputtext', 'rows'=>12, 'style' => 'width:100%', 'maxlength'=>1000)); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>
	<?php if(Yii::app()->user->checkAccess('admin') && ($model->scenario === 'updateguest' || $model->scenario === 'updateuser')): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Lookup::items('CommentStatus')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
	<?php endif; ?>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Message::getTranslation(15) : Message::getTranslation(70), array('class' => 'bigbutton')); ?>
	</div>

	<?php echo $form->hiddenField($model,'comment_id'); ?>
	<?= CHtml::hiddenField('lang', Yii::app()->getLanguage()) ?>

<?php $this->endWidget(); ?>

</div><!-- form -->