<?php
$this->pageTitle = Message::getTranslation(52);
$this->breadcrumbs=array(
	'Comments'=>array('index'),
	Message::getTranslation(52) .' #'.$model->id,
);
?>

<h1><?= Message::getTranslation(52) ?> #<?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>