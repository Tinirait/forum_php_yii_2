<?php
$this->pageTitle = Message::getTranslation(64);
$this->breadcrumbs=array(
	Message::getTranslation(64),
);
$style ='	@media 
	all and (max-width: 950px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
		td:nth-of-type(1):before { content: "' . Message::getTranslation(27) . '"; }
		td:nth-of-type(2):before { content: "' . Message::getTranslation(38) . '"; }
		td:nth-of-type(3):before { content: "' . Message::getTranslation(112) . '"; }
		td:nth-of-type(4):before { content: "' . Message::getTranslation(33) . '"; }
		td:nth-of-type(5):before { content: "' . Message::getTranslation(13) . '"; }
		td:nth-of-type(6):before { content: "' . Message::getTranslation(31) . '"; }
		td:nth-of-type(7):before { content: "' . Message::getTranslation(30) . '"; }
		td:nth-of-type(8):before { content: "' . Message::getTranslation(139) . '"; }
	}';
Yii::app()->getClientScript()->registerCss('style', $style);
if(Yii::app()->user->checkAccess('admin'))
	$template = '{approve}&nbsp;{update}&nbsp;{delete}';
else {
	$template = '{update}';
}
?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php elseif(Yii::app()->user->hasFlash('error')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>
<h1><?= Message::getTranslation(64) ?></h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/gridview.css'),
	'cssFile' => Yii::app()->baseUrl . '/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model->with('category'),
	'htmlOptions' => array('class' => 'grid-view rounded'),
	'ajaxUpdate'=>false,
	'columns'=>array(
		/*array(
			'name'=>'id',
			'type'=>'raw',
			'value'=>'$data->id',
		),*/
		array(
			'name'=>'post_id',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->post->title), $data->getUrl())',
		),
		array(
			'name'=>'user_id',
			'type'=>'raw',
			'value'=>'is_null($data->user) ? "" : CHtml::link($data->user->username, array("/blog/user/view", "user" => $data->user->id, "lang" => Yii::app()->getLanguage()))'
		),
		array(
			'name'=>'author',
			'type'=>'raw',
			'value'=>'CHtml::link($data->author, $data->url, array("target" => "_blank"))'
		),
		array(
			'name'=>'content',
			'type'=>'raw',
			'value'=>'CHtml::encode($data->content)',
		),
		array(
			'name'=>'create_time',
			'type'=>'datetime',
			'filter'=>false,
		),
		array(
			'name'=>'status',
			'value'=>'Lookup::item("CommentStatus",$data->status)',
			'filter'=>Lookup::items('CommentStatus'),
		),
        array(
            'header' => Message::getTranslation(49),
            'class' => 'CButtonColumn',
            'template' => $template,
            'updateButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-update.png',
            'deleteButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-delete.png',
	        'buttons'=>array
			(
               'approve' => array(
                 	'label' => Message::getTranslation(97),
                 	'url'=>'Yii::app()->createUrl("/blog/comment/approve", array("comment"=>$data->id,"post"=>$data->post_id, "category"=>$data->category_id, "language" => $data->language, "lang" => Yii::app()->getLanguage()))',
                    'imageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-approve.png',
                ),
		       'update' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/comment/update", array("comment"=>$data->id, "post"=>$data->post_id, "category"=>$data->category_id, "language" => $data->language, "lang" => Yii::app()->getLanguage()))'
		        ),
		       'delete' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/comment/delete", array("comment"=>$data->id, "post"=>$data->post_id, "category"=>$data->category_id, "language" => $data->language, "lang" => Yii::app()->getLanguage()))'
		        ),
			),
         ),
	),
)); ?>




