<div class="post">
	<div class="title">
		<?php echo CHtml::link(CHtml::encode($data->title), $data -> url); ?>
	</div>
	<div class="author">
		<?php
		if(Parameter::getValue(22)){
			echo Message::getTranslation(7) . ' ' . CHtml::link($data->user->username, $data->user->url, array('target' => '_blank', 'rel' => strpos($data->user->url, 'google') > 0 ? 'author' : null));
		}
		if(Parameter::getValue(23)){  
			echo ' '. date('j ',$data->create_time) . Message::getTranslation(114 + date('n',$data->create_time)) . date(', Y',$data->create_time);
		}
		?>	
	</div>
	<div class="content">
		<?php
			echo $data->getIntroductoryText();
		?>
	</div>
	<div class="nav">
		<?php if(Parameter::getValue(21)){ ?>
			<b><?= Message::getTranslation(4) ?>:</b>
			<?php echo implode(', ', $data->tagLinks); ?>
			<br/>
		<?php } ?>
		<?php 
			if(Parameter::getValue(25)):
				echo ' | ' . CHtml::link(Message::getTranslation(197), $data -> url, array('rel' => 'nofollow'));
			endif;
		?>
		<?php 
			if(Parameter::getValue(20)):
				echo ' | ' . CHtml::link(Message::getTranslation(17) . " ({$data->commentCount})", $data -> url . '#comments', array('rel' => 'nofollow'));
			endif; 
		?>
		<?php
			if(Parameter::getValue(24)):
				echo ' | ' . Message::getTranslation(14) . ' ' . date('j ',$data->update_time) . Message::getTranslation(114 + date('n',$data->update_time)) . date(', Y',$data->update_time);
			endif;
		?>
	</div>
</div>
