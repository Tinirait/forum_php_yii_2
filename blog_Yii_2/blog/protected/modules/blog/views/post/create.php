<?php
$this->pageTitle = Message::getTranslation(58);
$this->breadcrumbs=array(
	Message::getTranslation(58),
);
?>
<h1><?= Message::getTranslation(58) ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'language' => isset($language) ? $language : '')); ?>