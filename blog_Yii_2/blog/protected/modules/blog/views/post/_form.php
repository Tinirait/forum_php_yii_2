<?php
	$tinymce = '
		tinymce.init({' .
			(file_exists('js/tinymce/langs/' . Yii::app()->getLanguage() . '.js') ? ('language: "' . Yii::app()->getLanguage() . '",') : '') .
		    '
		    content_css: "' . Yii::app()->baseUrl .  '/js/tinymce/content.css",
		    selector: "textarea",
		    theme: "modern",
		    plugins: [
		        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
		        "searchreplace wordcount visualblocks visualchars code fullscreen",
		        "insertdatetime media nonbreaking save table contextmenu directionality",
		        "emoticons template paste textcolor colorpicker textpattern"
		    ],
		    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pastetext",
		    toolbar2: "forecolor backcolor emoticons | fontselect | fontsizeselect | print preview code pagebreak media",
		    image_advtab: true,
		    templates: [
		        {title: "Test template 1", content: "Test 1"},
		        {title: "Test template 2", content: "Test 2"}
		    ]
		});';

	$autocompletescript = '
		 $(function() {
		     function split(val) {
		         return val.split(/,\s*/);
		     }

		     function extractLast(term) {
		         return split(term).pop();
		     }
		     $("#Post_tags")
		         // don\'t navigate away from the field on tab when selecting an item
		         .bind("keydown", function(event) {
		             if (event.keyCode === $.ui.keyCode.TAB &&
		                 $(this).autocomplete("instance").menu.active) {
		                 event.preventDefault();
		             }
		         })
		         .autocomplete({
		             source: function(request, response) {
		                 $.getJSON("' . $this->createUrl('/blog/post/suggestTags') . '", {
		                     term: extractLast(request.term),
		                     lang: $("#Post_language").val(),
		                 }, response);
		             },
		             search: function() {
		                 // custom minLength
		                 var term = extractLast(this.value);
		                 if (term.length < 1) {
		                     return false;
		                 }
		             },
		             focus: function() {
		                 // prevent value inserted on focus
		                 return false;
		             },
		             select: function(event, ui) {
		                 var terms = split(this.value);
		                 // remove the current input
		                 terms.pop();
		                 // add the selected item
		                 terms.push(ui.item.value);
		                 // add placeholder to get the comma-and-space at the end
		                 terms.push("");
		                 this.value = terms.join(", ");
		                 return false;
		             }
		         });
		 });
	 ';
	 if($model->scenario === 'insert')
	 {
		 $getMaxId = '
		 	function getMaxId()
		 	{
				$.ajax({
					url: "' . $this->createUrl('/blog/post/getMaxId') . '",
					type: "POST",
					data: { language : $(\'#Post_language\').val(), category : $(\'#Post_category_id\').val() }
				}).success(function(result) {
					$(\'#Post_id\').val(result);
				});
			}
				
		 	$(\'#Post_language\').change(function(){
				getMaxId();
		 	});
			
			getMaxId();
		 ';
		Yii::app()->getClientScript()->registerScript('getmaxid', $getMaxId);
	}
	
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
	Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/tinymce/tinymce.min.js',CClientScript::POS_END);
	Yii::app()->getClientScript()->registerScript('tinymce', $tinymce);
	Yii::app()->getClientScript()->registerScript('autocompletescript', $autocompletescript);
	Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl . '/css/juiskins/jqueryui-smoothness.css');
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>
	<p class="note"><?= Message::getTranslation(8) ?></p>

	<?php echo CHtml::errorSummary($model); ?>
	<!--<div class="row" <?//= Yii::app()->user->checkAccess('author') ? 'style="display:none;"' : '' ?>>
		<?php //echo $form->labelEx($model,'id'); ?>
		<?php //echo $form->textField($model,'id',array('style'=>'width:100%','maxlength'=>10)); ?>
		<?php //echo $form->error($model,'id'); ?>
	</div>-->
	<!--<div class="row">
		<?php
			//$htmlOptions = array("ajax"=>array("url" => $this->createUrl("/blog/post/categoriesByLanguage"), "type" => "POST", "update" => "#Post_category_id"),	"disabled" => $model->scenario === 'update' ? 'disabled' : '');
		?>
		<?php //echo $form->labelEx($model,'language'); ?>
		<?php //echo $form->dropDownList($model,'language', Language::getLanguagesOptions(), $htmlOptions); ?>
		<?php //echo $form->error($model,'language'); ?>
	</div>-->
	
	<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList($model,'category_id', Category::getCategoriesOptions(is_null($model->category) ? $language : $model->category->language)); ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('style'=>'width:100%','maxlength'=>128)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo CHtml::activeTextArea($model,'content',array('style'=>'width:100%','rows'=>15, 'cols'=>70)); ?>
		<?php echo $form->error($model,'content'); ?>
		<p class="hint"><?= Message::getTranslation(170) ?></p>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'tags'); ?>
		<?php echo $form->textField($model,'tags', array('style' => 'width:100%')); ?>
		<p class="hint"><?= Message::getTranslation(71) ?></p>
		<?php echo $form->error($model,'tags'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'meta_keywords'); ?>
		<?php echo $form->textField($model,'meta_keywords',array('style'=>'width:100%')); ?>
		<?php echo $form->error($model,'meta_keywords'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'meta_description'); ?>
		<?php echo $form->textField($model,'meta_description',array('style'=>'width:100%')); ?>
		<?php echo $form->error($model,'meta_description'); ?>
	</div>
	<?php if(Yii::app()->user->checkAccess('admin') || Yii::app()->user->checkAccess('publisher') || (Yii::app()->user->checkAccess('author') && $model->user_id == Yii::app()->user->id && $model->status == Post::STATUS_DRAFT) || (Yii::app()->user->checkAccess('editor') && $model->user_id == Yii::app()->user->id && $model->status == Post::STATUS_DRAFT)): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',$model->getStatusOptions()); ?>
		<?php echo $form->error($model,'status'); ?>
		<?php if(Parameter::getValue(30) && (Yii::app()->user->checkAccess('admin') || Yii::app()->user->checkAccess('publisher'))): ?>
			<span class="hint"><?= Message::getTranslation(169); ?></span>
		<?php endif; ?>
	</div>
	<?php endif; ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Message::getTranslation(50) : Message::getTranslation(70), array('class' => 'bigbutton')); ?>
	</div>
	
	<?= CHtml::hiddenField('lang', Yii::app()->getLanguage()) ?>
<?php $this->endWidget(); ?>

</div><!-- form -->