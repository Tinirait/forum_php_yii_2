<?php
// главная
$this -> pageTitle = Yii::t('messages', 'L2');
$this->canonical = Yii::app()->createAbsoluteUrl('/blog/post/index', array('lang' => Yii::app()->getLanguage()));
?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php elseif(Yii::app()->user->hasFlash('error')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>

<?php if(!empty($_GET['tag'])): ?>
<h1><?= Message::getTranslation(16) ?> <i>"<?php echo CHtml::encode(str_replace('-', ' ', $_GET['tag'])); ?>"</i></h1>
<?php endif; ?>
<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'template'=>"{pager}\n{items}\n{pager}",
	'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/gridview.css'),
)); ?>
