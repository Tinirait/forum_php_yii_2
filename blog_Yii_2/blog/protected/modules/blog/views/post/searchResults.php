<h1>
<?php echo $results->totalItemCount . ' ' . Message::getTranslation(190); ?>
</h1> 
<?php
	$this->pageTitle = $results->totalItemCount . ' ' . Message::getTranslation(190);
	
	Yii::app()->clientScript->registerCss('stylesearch', '
		.items{
			text-align:center;
		}
		
		.list-view .pager
		{
			margin: 5px 0 0 0;
			text-align: center;
		}	
	');

	$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$results,
		'itemView'=>'_searchResult',
		'template'=>"{pager}\n{items}\n{pager}",
	));