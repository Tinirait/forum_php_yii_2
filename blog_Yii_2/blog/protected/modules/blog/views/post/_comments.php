<?php
	$commentReply ="	
			$('.replycomment').click(function(){
				$('#Comment_comment_id').attr('value', $(this).parent().parent().parent().parent().attr('id').substring(1));
			});";
	$jslink = "$('.jslink').click(function(){
				window.open($(this).attr('title'), '_blank');
			});";
	$cs = Yii::app()->getClientScript();
	$cs->registerScript('commentReply', $commentReply);
	$cs->registerScript('jslink', $jslink);

	$this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$comments,
		'itemView'=>'_commentview',
		'viewData' => array('post' => $post), 
		'template'=>"{items}\n{pager}",
	));
?>

<?php 
function printComments($comments, $post)
{
	foreach($comments as $key => $comment): 
?>
	<article style="word-wrap: break-word;border:1px dotted black;<?= is_null($comment->comment_id) ? '' : "margin-left:20px" ?>" class="comment" id="c<?php echo $comment->id; ?>">
		<div style="display:table;width:100%">
			<div style="display:table-cell;width:6%;vertical-align: top;">
				<?php
					echo CHtml::image(Yii::app()->baseUrl . '/' . User::IMAGE_DIRECTORY . (is_null($comment->user_id) ? User::DEFAULT_IMAGE : $comment->user->getImageFile()), '', array('class' => 'avatar'));
				?>				
			</div>
			<div style="display:table-cell;padding-left:10px">
				<header style="padding-bottom:5px;border-bottom: 1px solid silver;">
					<b><?php echo $comment->authorLink; ?>&nbsp;&nbsp;&nbsp;
					<?php echo date('j ',$comment->create_time) . Message::getTranslation(114 + date('n',$comment->create_time)) . date(', Y h:i a',$comment->create_time); ?>&nbsp;&nbsp;&nbsp;</b>
					<?php echo CHtml::link(Message::getTranslation(100), '#comment-form', array('class' => 'replycomment', 'rel' => 'nofollow')); ?>&nbsp;&nbsp;&nbsp;
					<?php echo CHtml::link(Message::getTranslation(197), $comment->getUrl($post), array('title'=>Message::getTranslation(196), 'rel' => 'nofollow')); ?>
				</header>
				<div style="padding-top: 10px;padding-bottom: 10px;">
						<?php echo nl2br($comment->content); ?>
				</div>
			</div>
		</div>
		<?php 
			printComments($comment->getApprovedComments(), $post);
		?>
	</article>
<?php
	endforeach;
}
?>
