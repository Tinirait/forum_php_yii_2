<?php
$this->pageTitle = Message::getTranslation(63);
$this->breadcrumbs=array(
	Message::getTranslation(63),
);	
$style ='	@media 
	all and (max-width: 950px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
		td:nth-of-type(1):before { content: "' . Message::getTranslation(27) . '"; }
		td:nth-of-type(2):before { content: "' . Message::getTranslation(28) . '"; }
		td:nth-of-type(3):before { content: "' . Message::getTranslation(33) . '"; }
		td:nth-of-type(4):before { content: "' . '#' . substr(Message::getTranslation(17),0,3) . '"; }
		td:nth-of-type(5):before { content: "' . Message::getTranslation(34) . '"; }
		td:nth-of-type(6):before { content: "' . Message::getTranslation(35) . '"; }
		td:nth-of-type(7):before { content: "' . Message::getTranslation(31) . '"; }
		td:nth-of-type(8):before { content: "' . Message::getTranslation(32) . '"; }
		td:nth-of-type(9):before { content: "' . Message::getTranslation(30) . '"; }
		td:nth-of-type(10):before { content: "' . Message::getTranslation(139) . '"; }
	}';
Yii::app()->getClientScript()->registerCss('style', $style);
?>
<?php if(Yii::app()->user->hasFlash('success')): ?>
<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php elseif(Yii::app()->user->hasFlash('error')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('error'); ?>
</div>
<?php endif; ?>
<h1><?= Message::getTranslation(63) ?></h1>
<div style="text-align:center;margin-bottom:20px;">
<?php echo CHtml::link(Message::getTranslation(50),array('post/create', 'lang' => Yii::app()->getLanguage()), array('class' => 'btncreate')); ?>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'pager' => array('cssFile' => Yii::app()->baseUrl . '/css/gridview.css'),
	'cssFile' => Yii::app()->baseUrl . '/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model->with('category'),
	'htmlOptions' => array('class' => 'grid-view rounded'),
	'ajaxUpdate'=>false,
    // указываем какие поля показывать
	'columns'=>array(
		array(
			'name'=>'id',
			'type'=>'raw',
			'value'=>'$data->id',
    		'htmlOptions'=>array('style'=>'width:40px'),			
		),
		array(
			'name'=>'title',
			'type'=>'raw',
			'value'=>'CHtml::link(CHtml::encode($data->title), $data->url)',
			'htmlOptions'=>array('style'=>'min-width:80px'),	
		),
		array(
			'name'=>'user_id',
			'type'=>'raw',
			'value'=>'is_null($data->user) ? "" : $data->user->username',
    		'htmlOptions'=>array('style'=>'width:40px'),			
		),
		array(
			'name'=>'commentCount',
			'type'=>'raw',
			'value'=>'$data->commentCount',
    		'filter'=>false,		
		),
		array(
			'name'=>'category_id',
			'type'=>'raw',
			'value'=>'$data->category->name',
    		'htmlOptions'=>array('style'=>'width:80px'),			
		),
		/*array(
			'name'=>'language',
			'value'=>'Language::getNameFromCode($data->language)',
			'filter'=>Language::getLanguagesOptions()
		),*/
		array(
			'name'=>'create_time',
			'type'=>'datetime',
			'filter'=>false,
		),
		array(
			'name'=>'update_time',
			'type'=>'datetime',
			'filter'=>false,
		),
		array(
			'name'=>'status',
			'value'=>'Lookup::item("PostStatus",$data->status)',
			'filter'=>Lookup::items('PostStatus'),
		),
        array(
        	'htmlOptions'=>array('style'=>'width:70px'),
            'header' => Message::getTranslation(49),
            'class' => 'CButtonColumn',
            'template' => '{update}&nbsp;{delete}',
            'viewButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-view.png',
            'updateButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-update.png',
            'deleteButtonImageUrl' => Yii::app()->baseUrl . '/images/gridview/' . 'gr-delete.png',
            'buttons'=>array
    		(
		       /*'view' => array
		        (
		        	'url'=>'$data->url'
		        ),*/
		       'update' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/post/update", array("post"=>$data->id, "category"=>$data->category_id, "language" => $data->language, "lang" => Yii::app()->getLanguage()))'
		        ),
		       'delete' => array
		        (
		        	'url'=>'Yii::app()->createUrl("/blog/post/delete", array("post"=>$data->id, "category"=>$data->category_id, "language" => $data->language, "lang" => Yii::app()->getLanguage()))'
		        ),
 			),
         ),
	),
    // Кнопки редактирования и удаления, которые справа расположены
)); ?>




