<?php
$this->breadcrumbs=array(
	$model->title,
);
$this->pageTitle=$model->title;
$this->metaKeywords = $model->meta_keywords;
$this->metaDescription = $model->meta_description; 
$this->canonical = $model->absoluteUrl;

$script = "
var switchTo5x=true;
stLight.options({publisher: 'ur-abaa83-a142-32c1-1cf9-22038d8811c2', doNotHash: false, doNotCopy: false, hashAddressBar: false});
";

Yii::app()->clientScript->registerScriptFile('http://w.sharethis.com/button/buttons.js',CClientScript::POS_END);
Yii::app()->getClientScript()->registerScript('script', $script);
?>
<article>
	<div class="post">
		<header>
			<div class="title">
				<h1><?php echo CHtml::link(CHtml::encode($model->title), $model -> url); ?></h1>
			</div>
			<div class="author">
				<?php
					if(Parameter::getValue(22)){
						echo Message::getTranslation(7) . ' ' . CHtml::link($model->user->username, $model->user->url, array('target' => '_blank', 'rel' => strpos($model->user->url, 'google') > 0 ? 'author' : null));
					}
					if(Parameter::getValue(23)){  
						echo ' ' . date('j ',$model->create_time) . Message::getTranslation(114 + date('n',$model->create_time)) . date(', Y',$model->create_time);
					}
				?>
			</div>
		</header>
		<div class="content">
			<?php
				$this->beginWidget('CMarkdown', array('purifyOutput'=>false));
				echo $model->content;
				$this->endWidget();
			?>
		</div>
		<?php if(Parameter::getValue(18)): ?>
		<div style="text-align:center;font-size:16px;padding:10px;margin:20px;border:2px solid red;">
			<?= Message::getTranslation(99) ?>:<br /> 
			<?php 
				$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				echo CHtml::link($actual_link, $actual_link); 
			?>
		</div>
		<?php endif; ?>
		<div class="nav">
			<?php if(Parameter::getValue(21)){ ?>
				<b><?= Message::getTranslation(4) ?>:</b>
				<?php echo implode(', ', $model->tagLinks); ?>
				<br/>
			<?php } ?>
			<?php 
				if(Parameter::getValue(25)):
					echo ' | ' . CHtml::link(Message::getTranslation(197), $model -> url, array('rel' => 'nofollow'));
				endif;
			?>
			<?php 
				if(Parameter::getValue(20)):
					echo ' | ' . CHtml::link(Message::getTranslation(17) . " ({$model->commentCount})", $model -> url . '#comments', array('rel' => 'nofollow'));
				endif; 
			?>
			<?php
				if(Parameter::getValue(24)):
					echo ' | ' . Message::getTranslation(14) . ' ' . date('j ',$model->update_time) . Message::getTranslation(114 + date('n',$model->update_time)) . date(', Y',$model->update_time);
				endif;
			?>
		</div>
		<?php if(Parameter::getValue(19)): ?>
		<div style="margin-top:20px">
			<span class='st_facebook_large' displayText='Facebook'></span>
			<span class='st_twitter_large' displayText='Tweet'></span>
			<span class='st_googleplus_large' displayText='Google +'></span>
			<span class='st_blogger_large' displayText='Blogger'></span>
			<span class='st_tumblr_large' displayText='Tumblr'></span>
			<span class='st_pinterest_large' displayText='Pinterest'></span>
			<span class='st_myspace_large' displayText='MySpace'></span>
			<span class='st_reddit_large' displayText='Reddit'></span>
			<span class='st_linkedin_large' displayText='LinkedIn'></span>
			<span class='st_delicious_large' displayText='Delicious'></span>
			<span class='st_meneame_large' displayText='Meneame'></span>
			<span class='st_email_large' displayText='Email'></span>
		</div>
		<?php endif; ?>
	</div>
	<?php if(Parameter::getValue(20)): ?>
	<section id="comments">
		<?php if($model->commentCount>=1): ?>
			<h2>
				<?php echo $model->commentCount>1 ? $model->commentCount . ' ' . Message::getTranslation(171) : Message::getTranslation(172); ?>
			</h2>
	
			<?php 
				$this->renderPartial('_comments',array(
					'post'=>$model,
					'comments'=>$comments,
				)); 
			?>
		<?php endif; ?>
	
		<h2 style="margin-top:30px;"><?= Message::getTranslation(9) ?></h2>
	
		<?php if(Yii::app()->user->hasFlash('commentSubmitted')): ?>
			<div class="flash-success">
				<?php echo Yii::app()->user->getFlash('commentSubmitted'); ?>
			</div>
		<?php else: ?>
			<?php $this->renderPartial('/comment/_form',array(
				'model'=>$comment,
			)); ?>
		<?php endif; ?>
	</section><!-- comments -->
	<?php endif; ?>
</article>
