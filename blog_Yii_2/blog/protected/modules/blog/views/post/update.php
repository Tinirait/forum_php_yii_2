<?php
$this->pageTitle = Message::getTranslation(51);
$this->breadcrumbs=array(
	$model->title=>$model->url,
	Message::getTranslation(51),
);
?>

<h1><?= Message::getTranslation(51) ?> <i><?php echo CHtml::encode($model->title); ?></i></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>