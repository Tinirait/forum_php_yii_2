<article style="word-wrap: break-word;border:1px dotted black;" class="comment" id="c<?php echo $data->id; ?>">
	<div style="display:table;width:100%">
		<div style="display:table-cell;width:6%;vertical-align: top;">
			<?php
				echo CHtml::image(Yii::app()->baseUrl . '/' . User::IMAGE_DIRECTORY . (is_null($data->user_id) ? User::DEFAULT_IMAGE : $data->user->getImageFile()), '', array('class' => 'avatar'));
			?>				
		</div>
		<div style="display:table-cell;padding-left:10px">
			<header style="padding-bottom:5px;border-bottom: 1px solid silver;">
				<b><?php echo $data->authorLink; ?>&nbsp;&nbsp;&nbsp;
				<?php echo date('j ',$data->create_time) . Message::getTranslation(114 + date('n',$data->create_time)) . date(', Y h:i a',$data->create_time); ?>&nbsp;&nbsp;&nbsp;</b>
				<?php echo CHtml::link(Message::getTranslation(100), '#comment-form', array('class' => 'replycomment', 'rel' => 'nofollow')); ?>&nbsp;&nbsp;&nbsp;
				<?php echo CHtml::link(Message::getTranslation(197), $data->getUrl($post), array('title'=>Message::getTranslation(196), 'rel' => 'nofollow')); ?>
			</header>
			<div style="padding-top: 10px;padding-bottom: 10px;">
					<?php echo nl2br($data->content); ?>
			</div>
		</div>
	</div>
	<?php 
		printComments($data->getApprovedComments(), $post);
	?>
</article>