-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 11, 2015 at 04:29 AM
-- Server version: 5.5.34
-- PHP Version: 5.4.38-1+deb.sury.org~precise+2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blog_base`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_auth_assignment`
--

CREATE TABLE IF NOT EXISTS `tbl_auth_assignment` (
  `itemname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `userid` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `bizrule` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_auth_assignment`
--

INSERT INTO `tbl_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '1', NULL, 'N;'),
('reader', '2', NULL, ''),
('reader', '3', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_auth_item`
--

CREATE TABLE IF NOT EXISTS `tbl_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `bizrule` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_auth_item`
--

INSERT INTO `tbl_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, '', NULL, ''),
('author', 2, '', NULL, ''),
('editor', 2, '', NULL, ''),
('publisher', 2, '', NULL, ''),
('reader', 2, '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_auth_item_child`
--

CREATE TABLE IF NOT EXISTS `tbl_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `language` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `name`, `language`, `category_id`, `status`) VALUES
(1, 'Страны', 'ru', NULL, 2),
(2, 'Африка', 'ru', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE IF NOT EXISTS `tbl_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `author` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `language` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`post_id`,`category_id`,`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_comment`
--

INSERT INTO `tbl_comment` (`id`, `content`, `status`, `create_time`, `author`, `email`, `url`, `post_id`, `comment_id`, `user_id`, `language`, `category_id`) VALUES
(1, 'ddfdfdvcvc', 2, 1434015373, 'dddd', 'ddd@mail.ru', 'http://www.code.ru', 3, NULL, NULL, 'r', 2),
(1, 'aaaaa', 2, 1434020628, '', NULL, '', 6, NULL, 1, 'r', 1),
(1, 'sdsdsds', 2, 1434021023, '', NULL, '', 6, NULL, 1, 'ru', 1),
(2, 'вввв', 2, 1434021035, '', NULL, '', 6, 1, 1, 'ru', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_language`
--

CREATE TABLE IF NOT EXISTS `tbl_language` (
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_language`
--

INSERT INTO `tbl_language` (`code`, `name`) VALUES
('ru', 'Русский');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lookup`
--

CREATE TABLE IF NOT EXISTS `tbl_lookup` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `type` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_lookup`
--

INSERT INTO `tbl_lookup` (`id`, `name`, `code`, `type`, `position`) VALUES
(1, 'Корзина', 1, 'PostStatus', 1),
(2, 'Опубликовано', 2, 'PostStatus', 2),
(3, 'Архив', 3, 'PostStatus', 3),
(4, 'Ожидает подтверждения', 1, 'CommentStatus', 1),
(5, 'Подтверждено', 2, 'CommentStatus', 2),
(6, 'Не опубликовано', 1, 'CategoryStatus', 1),
(7, 'Опубликовано', 2, 'CategoryStatus', 2),
(8, 'Подтверждено', 1, 'UserStatus', 1),
(9, 'Бан', 2, 'UserStatus', 2),
(10, 'Активный', 3, 'UserStatus', 3),
(11, 'Корзина', 1, 'MassMailStatus', 1),
(12, 'Отправлено', 2, 'MassMailStatus', 2),
(13, 'Сообщение', 1, 'MassMailType', 1),
(14, 'Новости', 2, 'MassMailType', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE IF NOT EXISTS `tbl_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`,`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=198 ;

--
-- Dumping data for table `tbl_message`
--

INSERT INTO `tbl_message` (`id`, `language`, `translation`) VALUES
(1, 'ru', 'Административная часть'),
(2, 'ru', 'Категории'),
(3, 'ru', 'Архивы'),
(4, 'ru', 'Теги'),
(5, 'ru', 'Последние комментарии'),
(6, 'ru', 'Наиболее комментируемые посты'),
(7, 'ru', 'Пост от'),
(8, 'ru', 'Поля * обязательны'),
(9, 'ru', 'Оставить комментарий'),
(10, 'ru', 'Имя'),
(11, 'ru', 'Email'),
(12, 'ru', 'Web сайт'),
(13, 'ru', 'Комментарий'),
(14, 'ru', 'Последнее обновление'),
(15, 'ru', 'Подтвердить'),
(16, 'ru', 'Пост с тегами'),
(17, 'ru', 'Комментарии'),
(18, 'ru', 'Управление постами'),
(19, 'ru', 'Управление комментариями'),
(20, 'ru', 'Управление категориями'),
(21, 'ru', 'Управление языками'),
(22, 'ru', 'Управление пользователями'),
(23, 'ru', 'Управление параметрами'),
(24, 'ru', 'Выход'),
(25, 'ru', 'Управление сообщениями'),
(26, 'ru', 'Добро пожаловать'),
(27, 'ru', 'ИД'),
(28, 'ru', 'Заголовок'),
(29, 'ru', 'Содержание'),
(30, 'ru', 'Статус'),
(31, 'ru', 'Время создания'),
(32, 'ru', 'Время изменения'),
(33, 'ru', 'Автор'),
(34, 'ru', 'Категория'),
(35, 'ru', 'Язык'),
(36, 'ru', 'Мета ключевые слова'),
(37, 'ru', 'Мета описание'),
(38, 'ru', 'Пост'),
(39, 'ru', 'Родительский комментарий'),
(40, 'ru', 'Родительская категория'),
(41, 'ru', 'Код'),
(42, 'ru', 'Изображение'),
(43, 'ru', 'Имя пользователя'),
(44, 'ru', 'Пароль'),
(45, 'ru', 'Пароль повторно'),
(46, 'ru', 'Перевод'),
(47, 'ru', 'Описание'),
(48, 'ru', 'Значение'),
(49, 'ru', 'Действие'),
(50, 'ru', 'Создание'),
(51, 'ru', 'Изменить пост'),
(52, 'ru', 'Изменить комментарий'),
(53, 'ru', 'Изменить категорию'),
(54, 'ru', 'Изменить язык'),
(55, 'ru', 'Изменить пользователя'),
(56, 'ru', 'Изменить сообщение'),
(57, 'ru', 'Изменить параметр'),
(58, 'ru', 'Создать пост'),
(59, 'ru', 'Создать категорию'),
(60, 'ru', 'Создать язык'),
(61, 'ru', 'Создать пользователя'),
(62, 'ru', 'Создать сообщение'),
(63, 'ru', 'Управление постами'),
(64, 'ru', 'Управление комментариями'),
(65, 'ru', 'Управление категориями'),
(66, 'ru', 'Управление языками'),
(67, 'ru', 'Управление пользователями'),
(68, 'ru', 'Управление сообщениями'),
(69, 'ru', 'Управление параметрами'),
(70, 'ru', 'Сохранить'),
(71, 'ru', 'Пожалуйста разделите элементы запятыми.'),
(72, 'ru', 'Пост создан успешно.'),
(73, 'ru', 'Пост изменен успешно.'),
(74, 'ru', 'Пост удален успешно.'),
(75, 'ru', 'Спасибо за комментарий. После одобрения он будет опубликован.'),
(76, 'ru', 'Коммментарий изменен успешно.'),
(77, 'ru', 'Комментарий удален успешно.'),
(78, 'ru', 'Категория создана успешно.'),
(79, 'ru', 'Категория изменена успешно.'),
(80, 'ru', 'Категория удалена успешно.'),
(81, 'ru', 'Категория не может быть удалена. Удалите все ее посты или подкатегории.'),
(82, 'ru', 'Язык создан успешно.'),
(83, 'ru', 'Язык измене успешно.'),
(84, 'ru', 'Язык удален успешно.'),
(85, 'ru', 'Пользователь создан успешно.'),
(86, 'ru', 'Пользователь изменен успешно.'),
(87, 'ru', 'Пользователь удален успешно.'),
(88, 'ru', 'Пользователь-администратор не может быть удален.'),
(89, 'ru', 'Сообщение создано успешно.'),
(90, 'ru', 'Сообщение изменено успешно.'),
(91, 'ru', 'Сообщение удалено успешно.'),
(92, 'ru', 'Параметр удален успешно.'),
(93, 'ru', 'Комментарий не может быть удален. Удалите все дочерние комментарии сначала.'),
(94, 'ru', 'Запрошенная страница не найдена.'),
(95, 'ru', 'Выберите категорию'),
(96, 'ru', 'Комментарий одобрен успешно.'),
(97, 'ru', 'Одобрить'),
(98, 'ru', 'говорит'),
(99, 'ru', 'Вы можете скопировать пост, но Вы обязаны указать ссылку на оригинал.'),
(100, 'ru', 'Ответить'),
(101, 'ru', 'Теги могут содержать только символы.'),
(102, 'ru', 'Код может содержать только буквы.'),
(103, 'ru', 'Группа пользователей'),
(104, 'ru', 'Регистрация'),
(105, 'ru', 'Вы успешно зарегистрированы. Письмо с данными регистрации отправлены на электронный адрес.'),
(106, 'ru', 'Регистрация'),
(107, 'ru', 'Вы еще не участник?'),
(108, 'ru', 'Спасибо за регистрацию, для активации эккаунта перейдите по ссылке:'),
(109, 'ru', 'Подтверждение по электронной почте'),
(110, 'ru', 'Не правильный токен'),
(111, 'ru', 'Ваш эккаунт подтвержден. Вы можете логироваться.'),
(112, 'ru', 'Пользователь'),
(113, 'ru', 'Вы забанены на этом блоге.'),
(114, 'ru', 'Запомнить меня'),
(115, 'ru', 'Январь'),
(116, 'ru', 'Февраль'),
(117, 'ru', 'Март'),
(118, 'ru', 'Апрель'),
(119, 'ru', 'Май'),
(120, 'ru', 'Июнь'),
(121, 'ru', 'Июль'),
(122, 'ru', 'Август'),
(123, 'ru', 'Сентябрь'),
(124, 'ru', 'Октябрь'),
(125, 'ru', 'Ноябрь'),
(126, 'ru', 'Декабрь'),
(127, 'ru', 'Изменить профиль'),
(128, 'ru', 'Нажмите здесь для получения списка кодов языков.'),
(129, 'ru', 'Узнайте, что означают различные роли.'),
(130, 'ru', 'Можно читать посты и комментировать.'),
(131, 'ru', 'Можно создавать посты и комментировать.'),
(132, 'ru', 'Можно редактировать и удалять собственные посты.'),
(133, 'ru', 'Можно редактировать или удалять любые посты.'),
(134, 'ru', 'Можно редактировать, удалять и публиковать любые посты.'),
(135, 'ru', 'Можно выполнять любые доступные операции. Администратор не может быть удален.'),
(136, 'ru', 'Не можете редактировать или изменять статусы любых постов.'),
(137, 'ru', 'Роли'),
(138, 'ru', 'Последний визит'),
(139, 'ru', 'Действия'),
(140, 'ru', 'Не подписан'),
(141, 'ru', 'Вы отписаны успешно.'),
(142, 'ru', 'Язык не может быть удален. Вы должны сначала удалить связанные категории и сообщения.'),
(143, 'ru', 'Пользователь не может быть удален. Вы должны сначала удалить связанные посты, комментарии и подписавшихся.'),
(144, 'ru', 'Пост не может быть удален. Вы должны удалить сначала связанные комментарии.'),
(145, 'ru', 'Востановление пароля'),
(146, 'ru', 'Вы получили это уведомление, потому что вы запросили новый пароль. Если вы не запрашивали это уведомление, то, пожалуйста, игнорируйте его. Чтобы использовать новый пароль, Вы должны его активировать. Чтобы сделать это, нажмите на ссылку приведеную ниже.\r\n'),
(147, 'ru', 'В случае успеха, вы сможете войти, используя следующий пароль:'),
(148, 'ru', 'Пожалуйста, введите буквы, изображенные на картинке выше. Буквы не чувствительны к регистру.'),
(149, 'ru', 'Проверочный код'),
(150, 'ru', 'Я хочу подписаться. Я буду получать статьи и новости на свою почту.'),
(151, 'ru', 'Выберите категорию по который Вы хотите получать статьи.'),
(152, 'ru', 'Подписанные категории'),
(153, 'ru', 'Дополнительная информация'),
(154, 'ru', 'Подписка'),
(155, 'ru', 'Сгенерировать пароль'),
(156, 'ru', 'Сила пароля'),
(157, 'ru', 'Тип'),
(158, 'ru', 'Управления почтовыми адресами рассылки'),
(159, 'ru', 'Изменение почтового адреса рассылки'),
(160, 'ru', 'Создание электронного адреса рассылки'),
(161, 'ru', 'Новости будут отправлены только подписанным пользователям.'),
(162, 'ru', 'Получатели'),
(163, 'ru', 'Получатели по категориям'),
(164, 'ru', 'Получатели по ролям'),
(165, 'ru', 'Удерживайте управляющую клавишу для отмены выделения элементов.'),
(166, 'ru', 'Это письмо будет отправлено когда Вы изменили статус на `Отправить`.'),
(167, 'ru', 'Выберите категорию'),
(168, 'ru', 'Востановление пароля'),
(169, 'ru', 'Этот пост будет отправлено всем подписанным при первой его публикации.'),
(170, 'ru', 'Внимание: Вставка содержимого из Microsoft Word может быть проблемной, потому что Microsoft Word генерирует дополнительный код.'),
(171, 'ru', 'комментарии'),
(172, 'ru', 'Один комментарий'),
(173, 'ru', 'Востановление пароля'),
(174, 'ru', 'Электронная почта рассылки изменена успешно.'),
(175, 'ru', 'Электронный адрес удален успешно.'),
(176, 'ru', 'Этот ИД, категория и язык уже существуют.'),
(177, 'ru', 'Причина бана'),
(178, 'ru', 'Время бана истекло'),
(179, 'ru', 'Бан без временных ограничений'),
(180, 'ru', 'Никогда'),
(181, 'ru', 'ОТправьте мне одно письмо'),
(182, 'ru', 'Код проверки'),
(183, 'ru', 'Электронный адрес не существует.'),
(184, 'ru', 'Новый пароль отправлен на Ваш почтовый адрес.'),
(185, 'ru', 'Ваш новый пароль должен быть активирован.'),
(186, 'ru', 'Отправить'),
(187, 'ru', 'Сообщение успешно отправлено '),
(188, 'ru', 'ИД языка уже существует.'),
(189, 'ru', 'Поиск'),
(190, 'ru', 'Найденные результаты'),
(191, 'ru', 'Логин'),
(192, 'ru', 'Спасибо за Ваше письмо. Вскоре мы Вам ответим.'),
(193, 'ru', 'Вскоре мы Вам ответим.'),
(194, 'ru', 'Выберите категорию.'),
(195, 'ru', 'Этот ИД уже существует.'),
(196, 'ru', 'Постоянная ссылка на этот комментарий'),
(197, 'ru', 'Постоянная ссылка');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_parameter`
--

CREATE TABLE IF NOT EXISTS `tbl_parameter` (
  `id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_parameter`
--

INSERT INTO `tbl_parameter` (`id`, `description`, `value`) VALUES
(1, 'Количество последних комментариев!', '10'),
(2, 'Количество тегов', '40'),
(3, 'Количество самых комментируемых постов', '4'),
(4, 'Количество постов на странице', '10'),
(5, 'Количество слов в введении поста', '50'),
(6, 'Количество комментариев для одобрения', '1'),
(7, 'Максимальное число предложенных тегов (админка)', '10'),
(8, 'Количество постов на странице (админка)', '20'),
(9, 'Количество комментариев на странице (админка)', '20'),
(10, 'Количество категориев на странице (админка)', '20'),
(11, 'Количество языков на странице (админка)', '20'),
(12, 'Количество пользователей на странице (админка)', '20'),
(13, 'Количество параметров на странице (админка)', '20'),
(14, 'Количество категориев в виджете ', '1'),
(15, 'Количество архивов в виджете', '1'),
(16, 'Количество сообщений на странице (админка)', '20'),
(17, 'Показать поле web сайт в форме комментариев', '1'),
(18, 'Показывать предупреждения при копировании постов', '1'),
(19, 'Показать кнопку Share для поста', '1'),
(20, 'Показать комментарии к посту', '1'),
(21, 'Показать теги к посту', '1'),
(22, 'Показать авторов к посту', '1'),
(23, 'Показать время создания к посту', '1'),
(24, 'Показать время изменения к посту', '1'),
(25, 'Показать ссылку к посту', '1'),
(26, 'Web URL', 'http://blog.local'),
(27, 'Email без ответа', 'noreply@blog.ru'),
(28, 'Название web сайта', 'Блог'),
(29, 'Базовый язык', 'ru'),
(30, 'Включить систему подписки', '0'),
(31, 'Mass mails per page (admin area)', '10'),
(32, 'Nofollow widget links', '0'),
(33, 'Comments per page', '20'),
(34, 'Search results per page', '20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE IF NOT EXISTS `tbl_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `tags` text COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `language` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `meta_keywords` text COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `sent` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`category_id`,`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`id`, `title`, `content`, `tags`, `status`, `create_time`, `update_time`, `user_id`, `language`, `category_id`, `meta_keywords`, `meta_description`, `sent`) VALUES
(1, 'Marruecos', '<p> Marruecos (en árabe: المغرب al-Magrib, ''El país del occidente'' o ''donde el sol se pone''; en bereber: Amrruk o Murakuc) —oficialmente denominado Reino de Marruecos (en árabe: المملكة المغربية al-Mamlaka al-Magribiyya; en bereber: Tageldit n Umeṛṛuk), también conocido como Imperio Jerifiano o Reino Alauí— es un país soberano situado en el Magreb, al norte de África, con costas en el océano Atlántico y el mar Mediterráneo.<br /><br />Se encuentra separado del continente europeo por el estrecho de Gibraltar. Limita con: Argelia al este (la frontera con Argelia se encuentra cerrada desde 1994), por el sur Mauritania y por el norte España, con quien mantiene intensos lazos comerciales y comparte tanto fronteras marítimas como terrestres (Ceuta, Melilla y el Peñón de Vélez de la Gomera). Existen también otros enclaves españoles en la costa del Mediterráneo (Peñón de Alhucemas e Islas Chafarinas). Al territorio marroquí sujeto al Derecho internacional también se añade el territorio del Sahara Occidental, antigua colonia española, ocupado en 1975 por población marroquí, dejando pendiente la administración de facto al no haberse completado los Acuerdos de Madrid.6 n. 1<br /><br />Es el único país africano que actualmente no es miembro de la Unión Africana (UA). En 1984 la asamblea de la Organización para la Unidad Africana (OUA), instancia predecesora de la UA y de la cual Marruecos era miembro fundador, aceptó como miembro a la República Árabe Saharaui Democrática (RASD).7 Como respuesta, Marruecos se retiró de la organización. Es miembro de la Liga Árabe, Unión del Magreb Árabe, la Francofonía, la Organización de la Conferencia Islámica, la Unión por el Mediterráneo, la Unión Europea de Radiodifusión, el Grupo de los 77 y el Centro Norte-Sur. Es también un aliado importante no-OTAN de los Estados Unidos.</p>\r\n<p>Fuente  <a href=`http://es.wikipedia.org/wiki/Marruecos`>http://es.wikipedia.org/wiki/Marruecos</a></p>', 'marruecos,africa', 2, 1421860550, 1421861948, 1, 'ru', 2, NULL, NULL, 1),
(2, 'Morocco', '<p>Morocco (Listeni/məˈrɒkoʊ/; Arabic: المغرب‎ al-Maġrib, Berber: ⵍⵎⴰⵖⵔⵉⴱ[8] Lmaġrib, French: Maroc[Notes 1]), officially the Kingdom of Morocco,[2] is a country in the Maghreb region of North Africa. Geographically, Morocco is characterized by a rugged mountainous interior and large portions of desert. It is one of only three countries (with Spain and France) to have both Atlantic and Mediterranean coastlines. The Arabic name al-Mamlakah al-Maghribiyah (Arabic: المملكة المغربية‎, meaning `The Western Kingdom`) and Al-Maghrib (Arabic: المغرب‎, meaning `The West`) are commonly used as alternate names.<br /><br />Morocco has a population of over 33 million and an area of 446,550 km2 (172,410 sq mi). Its political capital is Rabat, although the largest city is Casablanca; other major cities include Marrakesh, Tangier, Tetouan, Salé, Fes, Agadir, Meknes, Oujda, Kenitra, and Nador. A historically prominent regional power, Morocco has a history of independence not shared by its neighbours. Its distinct culture is a blend of Arab, indigenous Berber, Sub-Saharan African, and European influences.<br /><br />Morocco claims the non-self-governing territory of Western Sahara as its Southern Provinces. Morocco annexed the territory in 1975, leading to a guerrilla war with indigenous forces until a cease-fire in 1991. Peace processes have thus far failed to break the political deadlock.<br /><br />Morocco is a constitutional monarchy with an elected parliament. The King of Morocco holds vast executive and legislative powers, especially over the military, foreign policy and religious affairs. Executive power is exercised by the government, while legislative power is vested in both the government and the two chambers of parliament, the Assembly of Representatives and the Assembly of Councillors. The king can issue decrees called dahirs which have the force of law. He can also dissolve the parliament after consulting the Prime Minister and the president of the Constitutional court.</p>\r\n<p>Source: <a href=`http://en.wikipedia.org/wiki/Morocco`>http://en.wikipedia.org/wiki/Morocco</a></p>', 'africa,morocco', 2, 1421860796, 1425035074, 1, 'ru', 2, NULL, NULL, 1),
(3, 'Egypt', '<p>Egypt (Listeni/ˈiːdʒɪpt/; Arabic: مِصر‎ Miṣr, Egyptian Arabic: مَصر Maṣr) is an Afro-Asiatic transcontinental country spanning the northeast corner of Africa and southwest corner of Asia, via a land bridge formed by the Sinai Peninsula. Most of Egypt''s territory of 1,010,000 square kilometres (390,000 sq mi) lies within the Nile Valley of North Africa, but it is also considered a Mediterranean country as it is bordered by the Mediterranean Sea to the north. It is also bordered by the Gaza Strip and Israel to the northeast, the Gulf of Aqaba to the east, the Red Sea to the east and south, Sudan to the south and Libya to the west.<br /><br />With over 87 million inhabitants, Egypt is the largest country in North Africa and the Arab World, the third-largest in Africa, and the fifteenth-most populous in the world. The great majority of its people live near the banks of the Nile River, an area of about 40,000 square kilometres (15,000 sq mi), where the only arable land is found. The large regions of the Sahara Desert, which constitute most of Egypt''s territory, are sparsely inhabited. About half of Egypt''s residents live in urban areas, with most spread across the densely populated centres of greater Cairo, Alexandria and other major cities in the Nile Delta.<br /><br />Egypt has one of the longest histories of any modern country, arising in the tenth millennium BCE as one of the world''s first nation states.[12] Considered a cradle of civilization, Ancient Egypt experienced some of the earliest developments of writing, agriculture, urbanisation, organised religion and central government in history. Iconic monuments such as the Giza Necropolis and its Great Sphinx, as well the ruins of Memphis, Thebes, Karnak, and the Valley of the Kings, reflect this legacy and remain a significant focus of archaeological study and popular interest worldwide. Egypt''s rich cultural heritage is an integral part of its national identity, having endured and at times assimilated various foreign influences, including Greek, Persian, Roman, Arab, Ottoman, and European.</p>\r\n<p>Source: <a href=`http://en.wikipedia.org/wiki/Egypt`>http://en.wikipedia.org/wiki/Egypt</a></p>', 'africa,egypt', 2, 1421861012, 1425469105, 1, 'ru', 2, NULL, NULL, 1),
(4, 'Egipto', '<p>Egipto, oficialmente la República Árabe de Egipto (en árabe: جمهوريّة مصرالعربيّة Ŷumhūriyyat Miṣr Al-ʿArabiyyah, pronunciado en dialecto egipcio: [Maṣr]), es un país soberano de Oriente Próximo en la parte más occidental del Máshrek. Es un país transcontinental, está ubicado tanto en el extremo noreste de África como en Asia, en la península del Sinaí. Limita con Sudán al sur, con Libia al oeste y con Israel al noreste. Al norte limita con el Mar Mediterráneo y al sureste con el Mar Rojo.<br /><br />La mayor parte de su superficie la integra el desierto del Sahara. El río Nilo cruza el desierto de norte a sur, formando un estrecho valle y un gran delta en su desembocadura en el Mediterráneo. Estas tierras fértiles se hallan densamente pobladas, concentrando la mayor población nacional de África y Oriente Medio. Casi la mitad de los egipcios viven en áreas urbanas, sobre todo en los centros densamente poblados de El Cairo y Alejandría.<br /><br />Egipto es famoso por su civilización antigua y sus monumentos, como las pirámides y la gran esfinge; la ciudad meridional de Lúxor contiene un gran número de restos antiguos, tales como el templo de Karnak y el Valle de los Reyes. Hoy, Egipto es un centro político y cultural importante del Oriente Próximo. Su actual forma de gobierno es la república semipresidencialista bajo gobierno interino, formado tras el golpe de Estado de 2013 que derrocó al primer presidente democrático del país, Mohamed Morsi.</p>\r\n<p>Fuente: <a href=`http://es.wikipedia.org/wiki/Egipto`>http://es.wikipedia.org/wiki/Egipto</a></p>', 'africa,egipto', 2, 1421860929, 1421861937, 1, 'ru', 2, NULL, NULL, 1),
(5, 'qqqq', '<p>qqqqqqq</p>', 'qqqq', 2, 1434020275, 1434020275, 1, 'ru', 2, 'qqqq', 'qqqq', 0),
(6, 'aaa', '<p>aaaa</p>', 'aaa', 2, 1434020599, 1434020609, 1, 'ru', 1, 'aaa', 'aaa', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subscription`
--

CREATE TABLE IF NOT EXISTS `tbl_subscription` (
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `language` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`category_id`,`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tag`
--

CREATE TABLE IF NOT EXISTS `tbl_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `frequency` int(11) DEFAULT '1',
  `language` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

--
-- Dumping data for table `tbl_tag`
--

INSERT INTO `tbl_tag` (`id`, `name`, `frequency`, `language`) VALUES
(36, 'Африка', 2, 'ru'),
(37, 'Морокко', 1, 'ru'),
(39, 'Египет', 1, 'ru'),
(40, 'qqqq', 1, 'en'),
(41, 'aaa', 1, 'ru');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` int(11) NOT NULL DEFAULT '0',
  `last_visit` int(11) NOT NULL DEFAULT '0',
  `url` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscribe` tinyint(1) NOT NULL DEFAULT '0',
  `additional_info` text COLLATE utf8_unicode_ci,
  `ban_expires` int(11) DEFAULT NULL,
  `ban_reason` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_password` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `email`, `create_time`, `last_visit`, `url`, `status`, `token`, `subscribe`, `additional_info`, `ban_expires`, `ban_reason`, `new_password`) VALUES
(1, 'admin', '$2a$13$2kesIoFcSk73VqWbidJyWOFhUNEi2VU4NyauJSwzi8YHLw8WVhen2', 'admin@admin.com', 1400163948, 1434016135, 'http://google.com', 3, 'lvkTZRtKTIVepPje7RUQ', 1, 'blah blah blah', NULL, NULL, NULL),
(2, 'tttttt', '$2a$13$Tt0MpjhaFFBBOkb1EUCxiuUm.SEnIJ.NNBJZ4a8T0VBR5uVWzmGae', 't@mail.ru', 1434014511, 0, 'http://www.ttt.ru', 1, '9we18VH6tbeIWh3sztUD', 0, 'ddddd', NULL, NULL, NULL),
(3, 'qqqqqq', '$2a$13$02rwfVCr2b9uxEJEXvLuAuND9lfxu1.hoz.nbFnYz0jVbaTmHcqcO', 'q@mail.ru', 1434014722, 0, 'http://www.qqtt.ru', 1, 'MDiLBHnMUQpJgWFjDBtf', 0, 'ddddd', NULL, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
