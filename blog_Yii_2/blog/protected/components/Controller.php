<?php
/**
 * Контроллер настраиваем базовый класс контроллера.
  * Все классы контроллер для этого приложения должен расширять этот базовый класс.
 */
class Controller extends CController
{
	/**
	 * var строка макет по умолчанию для view контроллера. По умолчанию '// layout/ column1',
     * Означает, используя макет одного столбца. См protected / views / layout / column1.php ".
	 */
	public $layout='//layouts/column1';
	/**
	 * var массив элементы контекстного меню. Это свойство будет присвоен {link CMenu :: items}.
	 */
	public $menu=array();
	/**
	 * * var массив костяк текущей страницы.Значение этого свойства будет
     * Быть назначены {link CBreadcrumbs :: links}
    // CBreadcrumbs-вы сделаете удобство для посетителя, а поисковые роботы увидят,
    //что пользователю у вас нравится и по этой причине поднимет позицию статьи или несколько (все зависит от поведенческих факторов)
	 */
	public $breadcrumbs=array();
	public $languages;
	public $paramKeys;
	public $paramValues;
	
	// Переменные, используемые в <HEAD>
	public $metaKeywords;
	public $metaDescription;
	public $metaRobots;
	public $canonical;
	public $pageTitle;

	// Блог переменные
	public $posts;
	public $postCount;
	public $loginForm;
	
	public function init()
	{		
        // зарегистрируйтесь, класс для расширения CAPTCHA, наследуется
        Yii::$classMap = array_merge( Yii::$classMap, array(
            'CaptchaExtendedAction' => Yii::getPathOfAlias('ext.captchaExtended').DIRECTORY_SEPARATOR.'CaptchaExtendedAction.php',
            'CaptchaExtendedValidator' => Yii::getPathOfAlias('ext.captchaExtended').DIRECTORY_SEPARATOR.'CaptchaExtendedValidator.php'
        ));
	}
	
	public function beforeAction($action)
	{
		Yii::import('application.modules.blog.models.Language');

		// Настройка языка.....................
		if(isset($_GET['lang']) && Language::model()->checkLanguage($_GET['lang']))
		{
			Yii::app()->setLanguage($_GET['lang']);
		}
		elseif(!isset($_GET['lang']))
		{
			$languageused = Language::getNavigatorLanguage();

			if(!Language::checkLanguage($languageused))
				//$languageused = Parameter::getValue(29);
                                $languageused = 'ru';
			Yii::app()->setLanguage($languageused);
		}
		elseif($action->id != 'error')
		{
			throw new CHttpException(404,'Page not found');	
		}
	
		$this->languages = Language::model()->findAll(array('order' => 'code ASC'));	
		$this->paramKeys = array_keys($_GET);
		$this->paramValues = array_values($_GET);
		
		// не удалять
		if(Yii::app()->user->isGuest)
			;
		
		return true;
	}
}