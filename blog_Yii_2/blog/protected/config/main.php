<?php
//Файл настройки проекта
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Мой Блог',
	'language' => 'ru',
    //язык, который будет использоваться по умолчанию для сообщений ошибок
	// предзагрузка 'log' component
	'preload'=>array('log'),
    //предзагрузка логов,чтобы вести логирование проекта

	// автозагрузка model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
		),
        //gii для генерации блоков кода автоматически
        'blog'=>array(
			/*
			 * Добавить значения параметров блога здесь.  можем получить доступ к их помощи Yii::app()->controller->module->myBlogParameter
			 */
			'myParameter' => 'value',
        ),
	),
    //блог который мы подключаем, который разработан.
	
	'components'=>array(
        'assetManager' => array(
            'linkAssets' => true,
        ),
		
        
		'request'=>array(
            'enableCookieValidation'=>true,
        ),
        //встроенные компоненты , которые нужны для работы системы,
        //asset-обрабатывает папку asset, и занимается её заполнением.
        
		'clientScript'=>array(
		  'packages'=>array(
		    'jquery'=>array(
		      'baseUrl'=>'js',
		      'js'=>array('jquery-2.1.3.min.js'),
		      'coreScriptPosition'=>CClientScript::POS_END
		    ),
		  ),
		),

		
		'user'=>array(
			// включить аутентификацию на основе cookie
			'allowAutoLogin'=>true,
			'loginUrl'=>array('/site/error'),
		),
        // Какие скрипты будут подключаться автоматически
        // в данном случает jquery

        'authManager'=>array(
            'class'=>'CDbAuthManager',
            'connectionID'=>'db',
	        'itemTable'=>'{{auth_item}}',
	        'itemChildTable'=>'{{auth_item_child}}',
	        'assignmentTable'=>'{{auth_assignment}}',
        ),
        //Отвечает за авторизацию проекта и права доступа(редактор, читатель, издатель)
        //Cдесь определяется 3-и таблицы(1ая-название ролей,2-ая подроли,3-ая права у каждой роли)
        //Всё это assignmentTable'=>'{{auth_assignment  r-bag - система доступа

		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>true,
            'rules'=>array(

                '<lang:\w+>/blog/<category:\d+>/<post:\d+>/<title:.*?>'=>'blog/post/view',
				'<lang:\w+>/blog/tag/<tag:.*?>'=>'blog/post/index',
				'<lang:\w+>/blog/<category:\d+>'=> 'blog/post/index',
				'<lang:\w+>/blog'=> 'blog/post/index',
				'<lang:\w+>/blog/register'=> 'blog/user/register',
				'<lang:\w+>/blog/passwordrecovery'=> 'blog/user/passwordrecovery',
				'<lang:\w+>/blog/user/<user:\d+>'=> 'blog/user/view',
				'<lang:\w+>/blog/<controller:\w+>/<action:\w+>'=>'blog/<controller>/<action>',
                				                                                        
                '<lang:\w+>'=>'site/index',
                '<lang:\w+>/<action:\w+>'=> 'site/<action>',
                // как формируются адреса, которые мы видим в браузере
                //cначала указываем язык,указываем тэг, потом параметр,
                // шаблон и куда он ведёт 'blog/post/index'(перед этим модель шаблона)
                //blog-название модуля,post-название контроллера,index-название действия.

            ),
		),

		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=blog_base',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
                        'tablePrefix' => 'tbl_',
		),
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
        // 'errorAction'=>'site/error' Если мы вызываем прирывание сообщений :контроллер site/действие error
        //Если указан контроллер и действие запускаем из папки protected, если модуль
        //например blog-запускается из modules/blog
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
                // log-ведение логов.
				
			),
		),
	),

	// Параметры на уровне приложения, которые могут быть доступны
	// использование Yii::app()->params['paramName']
	'params'=>array(
		'emailHost' => 'myhost',
		'emailPort' => '25', 
		'emailUsername' => 'myusername',
		'emailPassword' => 'mypassword',		
		'emailAltMessage' => 'To view the message, please use an HTML compatible email viewer.',
		'adminEmail'=>'admin@admin.com',
	),
);
//Cписок параметров для почтовой рассылки,скрыта,(на сайте проводится)