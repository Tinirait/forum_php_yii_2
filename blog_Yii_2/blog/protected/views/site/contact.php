<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::t('messages', 'L3');
$this->breadcrumbs=array(
	Yii::t('messages', 'L11'),
);
	$this->canonical = Yii::app()->createAbsoluteUrl("site/contact", array('lang' => 'en'));

Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/form.css');
?>

<h1><?= Yii::t('messages', 'L3') ?></h1>

<div style="padding-left:25px;padding-bottom:55px;">
<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p>
<?= Yii::t('messages', 'C1') ?>
</p>

<div style="text-align: left;padding-top:25px;" class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	/*'enableClientValidation'=>false,*/
        'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		//'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note"><?= Yii::t('messages', 'C2') ?></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('class' => 'inputtext','maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('class' => 'inputtext','maxlength'=>128)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subject'); ?>
		<?php echo $form->textField($model,'subject',array('size'=>40,'maxlength'=>128, 'class' => 'inputtext')); ?>
		<?php echo $form->error($model,'subject'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50, 'class' => 'inputtext','maxlength'=>2000)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
		<div class="row">
			<?php echo $form->labelEx($model,'verifyCode'); ?>
				<?php $this->widget('CCaptcha'); ?>
				<br/>
				<?php echo $form->textField($model,'verifyCode'); ?>
				<div style="color:black;" class="hint">
					<?= Yii::t('messages', 'C8') ?>
				</div>
			<?php echo $form->error($model,'verifyCode'); ?>
		</div>
	<?php endif; ?>

	<div class="row buttons">
		<?= CHtml::submitButton(Yii::t('messages', 'C9'), array('class'=>'bigbutton')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>

</div>