<?php
/* @var $this SiteController */
$this -> pageTitle = Yii::t('messages','L1');
$this -> metaKeywords = Yii::t('messages', 'L0');
$this -> metaDescription = $this -> metaKeywords;
?>
<div>
	<?= Yii::t('messages', 'H1') ?>
</div><br>
<div>Для блогов характерны недлинные записи вре́менной значимости, 
    упорядоченные в обратном хронологическом порядке (последняя запись сверху). 
    Отличия блога от традиционного дневника обусловливаются средой: 
    блоги обычно публичны и предполагают сторонних читателей, которые могут вступить 
    в публичную полемику с автором (в комментарии к блогозаписи или своих блогах).
</div><br>
<div>Людей, ведущих блог, называют блогерами. Совокупность всех блогов Сети принято <br>
    называть блогосферой.</div>
<div>Для блогов характерна возможность публикации отзывов (комментариев) посетителями. 
    Она делает блоги средой сетевого общения, имеющей ряд преимуществ перед электронной 
    почтой, группами новостей, веб-форумами и чатами.</div><br>
<div>Под блогами также понимаются персональные сайты, которые состоят в основном из 
    личных записей владельца блога и комментариев пользователей к этим записям.
</div>