<?php
	Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/css/blog.css');
?>
<?php $this->beginContent('//layouts/main'); ?>
<div>
	<div id="blogcontent" style="float:left;width:70%">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
	<div id="blogsidebar" style="padding-top:20px;float:left;width:28%">
			<div style="margin-bottom:5px">
				<?php 
					if(Yii::app()->user->isGuest)
					 	$this->widget('Login' , array('title' => Message::getTranslation(191)));
					elseif(!Yii::app()->user->isGuest)
					{
						echo "<div style='margin-bottom:20px;'><span style='font-size:20px'>" . Message::getTranslation(26) . ' ' . Yii::app()->user->name . "!</span></div>";
						$this->widget('UserMenu' , array('title' => Message::getTranslation(1)));
					}
				?>
			</div>		

			<?php $this->widget('Search', array('title' => Message::getTranslation(189))); ?>
	
			<?php $this->widget('Categories', array('title' => Message::getTranslation(2))); ?>

			<?php $this->widget('Archives', array('title' => Message::getTranslation(3))); ?>

			<?php $this->widget('TagCloud', array(
				'maxTags'=>Parameter::getValue(2), 'title' => Message::getTranslation(4)
			)); ?>

			<?php /*$this->widget('RecentComments', array(
				'maxComments'=>Parameter::getValue(1),
				'title' => Message::getTranslation(5)
			));*/ ?>
			
			<?php /*$this->widget('MostCommentedPosts', array(
				'mostCommentedPostsCount'=>Parameter::getValue(3),
				'title' => Message::getTranslation(6)
			));*/ ?>
	</div>
</div>
<div class="clear"></div>
<?php $this->endContent(); ?>