<?php
	$lang = Yii::app()->getLanguage();
	Yii::app()->clientScript->coreScriptPosition=CClientScript::POS_END;
	Yii::app()->clientScript->registerCoreScript('jquery');
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/main.css');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= $lang ?>" lang="<?= $lang ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?= Yii::app()->name ?> - <?= $this->pageTitle ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<?php if(isset($this->metaDescription)) : ?>
<meta name="description" content="<?= $this->metaDescription ?>" />
<?php endif; ?>
<?php if(isset($this->metaKeywords)) : ?>
<meta name="keywords" content="<?= $this->metaKeywords ?>" />
<?php endif; ?>
<?php if(isset($this->metaRobots)) : ?>
<meta name="robots" content="<?= $this->metaRobots ?>" />
<?php endif; ?>
<?php if(isset($this->canonical)) : ?>
<link rel="canonical" href="<?= $this->canonical ?>" />
<?php endif; ?> 
</head>
<body>
	<div id="main-page">
		<header id="main-header">
			<div>
				<div id = "main-logo">
					Мой Блог
				</div>
				<div id="main-flagsdiv">
					<span id="insertHere"></span>
					<?php?>
				</div>				

				<div class="clear"></div>
			</div>
		</header>

		<nav>
			<?php
            //подключение верхнего меню(главная страница, посты, кантакты).
			$this->widget('ext.multilevelhorizontalmenu.MultilevelHorizontalMenu',
			array(
			'menu'=>array(
				  	array('url'=>array('route'=>'site/index',
				  					  'params'=>array('lang'=>$lang)),
									  'label'=>Yii::t('messages', 'L1')),
				  	array('url'=>array('route'=>'blog/post/index',
				  					  'params'=>array('lang'=>$lang)),
									  'label'=>Yii::t('messages', 'L2')),
				  	array('url'=>array('route'=>'site/contact',
				  					  'params'=>array('lang'=>$lang)),
									  'label'=>Yii::t('messages', 'L3')),									  
       				  	array('url'=>array('route'=>'blog/rss/index'),
                                                                          'label'=>Yii::t('messages', 'L4')),
                                                                              //Yii::t чтобы на будующее делать
                                                                               //многоязычность сайта,ищет язык
                                                                               // в системе) в папке messages-возвращает ru


			          ),
			)
			);
			?>
		</nav>
		
		<?php echo $content; ?>
		
		<footer id="main-footer">
			<div>

				<?php echo 'Янчевский Владислав'; ?>
			</div>
		</footer>
	</div>
</body>
</html>

<?php
	function printFlags($lang, $paramKeys, $paramValues, $languages, $posts, $postCount)
	{
		if(!is_null($postCount))
		{
			foreach ($postCount as $post)
			{
				$arrayUrl = array();
				$arrayUrl[0] = Yii::app()->controller->id . '/' . Yii::app()->controller->action->id;
				$arrayUrl['lang'] = $post->language;	
				echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/images/flags/' . $post->language . '.gif',$post->language								
					,array('class' => ($lang === $post->language ? 'flag selectedflag' : 'flag')))
					,$arrayUrl);
			}			
		}
		elseif(!is_null($posts))
		{
			foreach ($posts as $post)
			{
				echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/images/flags/' . $post->language . '.gif',$post->language								
					,array('class' => ($lang === $post->language ? 'flag selectedflag' : 'flag')))
					,$post->url);
			}
		}
		else
		{
			$offset = 0;		
			foreach($languages as $key => $language)
			{
				$arrayUrl = array();					

				$arrayUrl[0] = Yii::app()->controller->id . '/' . Yii::app()->controller->action->id;
			 	for($i = 0; $i < count($paramKeys); $i++)
					if($paramKeys[$i] !== 'lang')
			 			$arrayUrl[$paramKeys[$i]] = $paramValues[$i];
				$arrayUrl['lang'] = $language->code;
				
				echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/images/flags/' . $language->code . '.gif', $language->code								
					,array('class' => ($lang === $language->code ? 'flag selectedflag' : 'flag')))
					,$arrayUrl);

				$offset -= 18;
			}
		}	
		
	}
?>
